/*
 * Copyright (C) 2009-2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define([
	"sap/ui/core/util/MockServer"
], function(MockServer) {
	"use strict";
	var oMockServer,
		_sAppModulePath = "retail/store/orderproducts1/",
		_sJsonFilesModulePath = _sAppModulePath + "localService/mockdata";

	return {

		/**
		 * Initializes the mock server.
		 * You can configure the delay with the URL parameter "serverDelay".
		 * The local mock data in this folder is returned instead of the real data for testing.
		 * @public
		 */
		init: function() {
			var oUriParameters = jQuery.sap.getUriParameters(),
				sJsonFilesUrl = jQuery.sap.getModulePath(_sJsonFilesModulePath),
				sManifestUrl = jQuery.sap.getModulePath(_sAppModulePath + "manifest", ".json"),
				sEntity = "StoreProducts",
				sErrorParam = oUriParameters.get("errorType"),
				iErrorCode = sErrorParam === "badRequest" ? 400 : 500,
				oManifest = jQuery.sap.syncGetJSON(sManifestUrl).data,
				oMainDataSource = oManifest["sap.app"].dataSources.mainService,
				sMetadataUrl = jQuery.sap.getModulePath(_sAppModulePath + oMainDataSource.settings.localUri.replace(".xml", ""), ".xml"),
				// ensure there is a trailing slash
				sMockServerUrl = /.*\/$/.test(oMainDataSource.uri) ? oMainDataSource.uri : oMainDataSource.uri + "/";

			oMockServer = new MockServer({
				rootUri: sMockServerUrl
			});

			// configure mock server with a delay of 1s
			MockServer.config({
				autoRespond: true,
				autoRespondAfter: (oUriParameters.get("serverDelay") || 1000)
			});

			// load local mock data
			oMockServer.simulate(sMetadataUrl, {
				sMockdataBaseUrl: sJsonFilesUrl,
				bGenerateMissingMockData: true
			});

			var aRequests = oMockServer.getRequests();
			var fnResponse = function(iErrCode, sMessage, aRequest) {
				aRequest.response = function(oXhr) {
					oXhr.respond(iErrCode, {
						"Content-Type": "text/plain;charset=utf-8"
					}, sMessage);
				};
			};

			// handling the metadata error test
			if (oUriParameters.get("metadataError")) {
				aRequests.forEach(function(aEntry) {
					if (aEntry.path.toString().indexOf("$metadata") > -1) {
						fnResponse(500, "metadata Error", aEntry);
					}
				});
			}

			// Handling request errors
			if (sErrorParam) {
				aRequests.forEach(function(aEntry) {
					if (aEntry.path.toString().indexOf(sEntity) > -1) {
						fnResponse(iErrorCode, sErrorParam, aEntry);
					}
				});
			}

			// Add request handling for initial store requests with empty key (map to default store R110)
			var sDefaultStoreID = "R110";
			aRequests.push({
				method: "GET",
				path: new RegExp("(Stores)\\(''\\)(.*)?"),
				response: function(oXhr) {
					jQuery.sap.log.debug("Initial store request with empty key -> map to default StoreID");
					var sUrl = oXhr.url.replace("('')", "('" + sDefaultStoreID + "')");
					var oResponse = jQuery.sap.sjax({
						url: sUrl
					});
					oXhr.respondJSON(200, {}, JSON.stringify(oResponse.data));
					return true;
				}
			});

			// Add request handler for printing of new handling unit (function import)
			aRequests.push({
				method: "POST",
				path: new RegExp("(DeleteUserDefaultFilterValues|UpdateUserDefaultFilterValue)(.*)"),
				response: function(oXhr) {
					// Do nothing, just fake the request to be successful
					oXhr.respondJSON(200, {}, JSON.stringify({
						d: {}
					}));
					return true;
				}
			});

			// Add request handler of special 'ProductsOfSamePage' request
			aRequests.push({
				method: "GET",
				path: new RegExp("StoreProducts\\(StoreID='(.*)',ProductID='(.*)'\\)\\/ProductsOfSamePage.*\\$top=(\d*).*"),
				response: function(oXhr) {
					var aParts = /StoreProducts\(StoreID='(.*)',ProductID='(.*)'\)\/ProductsOfSamePage.*\$top=(\d*).*/.exec(oXhr.url);
					if (aParts && aParts.length === 4) {
						var sStoreID = aParts[1];
						var sProductID = aParts[2];
						var iTop = Number(aParts[3]);
						var sUrl = oXhr.url.replace("StoreProducts(StoreID='" + sStoreID + "',ProductID='" + sProductID + "')/ProductsOfSamePage",
							"Stores('" + sStoreID + "')/Products").replace("$top=" + iTop + "&", "");
						var oResponse = jQuery.sap.sjax({
							url: sUrl
						});
						var aResults = oResponse.data.d.results;
						var iIndex = -1;
						for (var i = 0, iLength = aResults && aResults.length || 0; i < iLength; i++) {
							if (aResults[i].ProductID === sProductID) {
								iIndex = i;
								break;
							}
						}
						var iStartIndex = Math.floor(iIndex / iTop) * iTop;
						var aFinalResults = [];
						for (i = iStartIndex; i < iStartIndex + iTop; i++) {
							aFinalResults.push(aResults[i]);
						}
						oResponse.data.d.results = aFinalResults;
						oXhr.respondJSON(200, {}, JSON.stringify(oResponse.data));
						return true;
					}
				}
			});

			// Do not return any user default filter values for the initial store request
			oMockServer.attachAfter("GET", this.afterGetStores.bind(this), "Stores");

			oMockServer.setRequests(aRequests);

			oMockServer.start();

			jQuery.sap.log.info("Order Products: Running the app with mock data");
		},

		afterGetStores: function(oEvent) {
			// For the initial store request make sure that no user default filter values are returned (simulating a fresh entry into the app without automatically setting filters)
			var oXhr = oEvent.getParameter("oXhr");
			var oEntry = oEvent.getParameter("oEntry");
			var oFilteredData = oEvent.getParameter("oFilteredData");
			for (var i = 0, iLength = oEntry && oEntry.Filters && oEntry.Filters.results && oEntry.Filters.results.length || 0; i < iLength; i++) {
				if (oEntry.Filters.results[i].UserDefaultFilterValues && oEntry.Filters.results[i].UserDefaultFilterValues.results) {
					oEntry.Filters.results[i].UserDefaultFilterValues.results = [];
				}
			}

			// Take care of the entity set index
			var oRegExp = /.*\/Products.*\$skip=(\d*).*/;
			var aParts = oRegExp.exec(oXhr.url);
			var iIndex = 1;
			if (aParts && aParts[1] > 0) {
				iIndex = iIndex + Number(aParts[1]);
			}
			for (i = 0, iLength = oFilteredData && oFilteredData.results && oFilteredData.results.length || 0; i < iLength; i++) {
				oFilteredData.results[i].EntitySetIndex = iIndex + i;
			}
		},

		/**
		 * @public returns the mockserver of the app, should be used in integration tests
		 * @returns {sap.ui.core.util.MockServer} the mockserver instance
		 */
		getMockServer: function() {
			return oMockServer;
		}
	};

});