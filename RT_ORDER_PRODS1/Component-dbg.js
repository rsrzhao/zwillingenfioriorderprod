/*
 * Copyright (C) 2009-2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define([
	"sap/ui/core/routing/HashChanger",
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"sap/m/MessageBox",
	"retail/store/orderproducts1/model/models",
	"sap/retail/store/lib/reuses1/util/BarcodeScanHandler",
	"retail/store/orderproducts1/utils/CalendarUtil",
	"retail/store/orderproducts1/utils/DataChangeUtil",
	"retail/store/orderproducts1/utils/ScanUtil",
	"retail/store/orderproducts1/model/DataManager",
	"retail/store/orderproducts1/model/formatter"
], function(HashChanger, UIComponent, Device, MessageBox, models, BarcodeScanHandler, CalendarUtil, DataChangeUtil, ScanUtil,
	DataManager, Formatter) {
	"use strict";

	return UIComponent.extend("retail.store.orderproducts1.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * In this function, the FLP and device models are set and the router is initialized.
		 * @public
		 * @override
		 */
		init: function() {
			this.sSelectedProductID = "";
			this.oLastScannedStoreProduct = null;
			this.oStoreWeekDisplayData = null;
			this.oStoreSettings = null;
			this.dCurrentDate = null;

			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// set the device model
			this.setModel(models.createDeviceModel(), "device");

			// attach the Error Handler for metadatafailed error
			this.getModel().attachMetadataFailed(function(oEvent) {
				var oParams = oEvent.getParameters();
				this._showMetadataError(oParams.response);
			}, this);

			// create the views based on the url/hash
			this.getRouter().initialize();

			if (this.addCustomCSS) {
				this.addCustomCSS();
			}

			// Initialize the Utility Classes
			this.initUtilities();

			// Set current date						
			this.setCurrentDate(new Date());
		},

		initUtilities: function() {
			var oResourceBundle = this.getModel("i18n").getResourceBundle();

			// Initialize the barcode scan handler
			this.initBarcodeScanHandler();

			this.oDataManager = new DataManager(this, this.getModel(), oResourceBundle, this.oBarcodeScanHandler);

			// Initialize the DataChangeUtil
			DataChangeUtil.init(this, this.oDataManager, oResourceBundle);

			// Initialize the CalendarUtil
			CalendarUtil.init(this, oResourceBundle);

			// Initialize the Formatter
			Formatter.init(this, oResourceBundle);

			// Initialize the ScanUtil
			ScanUtil.init(oResourceBundle);
		},

		initBarcodeScanHandler: function() {
			// Create an adjust an instance of the barcode scan handler
			this.oBarcodeScanHandler = new BarcodeScanHandler();
			/**
			 * @ComponentHook [Utility hook to adjust the barcode scan handler instance] The hook can be used to adjust the instance of the
			 *                barcode scan handler for this app. In particular, the necessary hook functions for initializing and resetting
			 *                the connection to an external barcode scan handler can be created.
			 * @callback retail.store.receiveproducts1.Component~utilityExtHookAdjustBarcodeScanHandler
			 * @param {object} oBarcodeScanHandler - the instance of the barcode scan handler that is used within the current application.
			 * @return {void}
			 */
			if (this.utilityExtHookAdjustBarcodeScanHandler) {
				this.utilityExtHookAdjustBarcodeScanHandler(this.oBarcodeScanHandler);
			}

			// Initialize the barcode scan handler
			this.oBarcodeScanHandler.init();
		},

		/**
		 * Setter and Getter Methods for the global variables and objects
		 */
		setCurrentDate: function(dDate) {
			this.dCurrentDate = dDate;
		},

		getCurrentDate: function() {
			return this.dCurrentDate;
		},

		setSelectedStoreProduct: function(sProductID) {
			this.sSelectedProductID = sProductID;
		},

		getSelectedStoreProduct: function() {
			return this.sSelectedProductID;
		},

		setLastScannedStoreProduct: function(oLastScannedStoreProduct) {
			this.oLastScannedStoreProduct = oLastScannedStoreProduct;
		},

		getLastScannedStoreProduct: function() {
			return this.oLastScannedStoreProduct;
		},

		setStoreWeekDisplayData: function(oStoreWeekDisplayData) {
			this.oStoreWeekDisplayData = oStoreWeekDisplayData;
		},

		getStoreWeekDisplayData: function() {
			return this.oStoreWeekDisplayData;
		},

		setStoreSettings: function(oStoreSettings) {
			this.oStoreSettings = oStoreSettings;
		},

		getStoreSettings: function() {
			return this.oStoreSettings;
		},

		getDataManager: function() {
			return this.oDataManager;
		},

		getBarcodeScanHandler: function() {
			return this.oBarcodeScanHandler;
		},

		/**
		 * Shows a {@link sap.m.MessageBox} when the metadata call has failed.
		 * The user can try to refresh the metadata.
		 * @param {string} sDetails a technical error to be displayed on request
		 * @private
		 */
		_showMetadataError: function(sDetails) {
			MessageBox.error(
				this.getModel("i18n").getResourceBundle().getText("errorText"), {
					id: "metadataErrorMessageBox",
					details: sDetails,
					styleClass: this.getContentDensityClass(),
					actions: [MessageBox.Action.RETRY, MessageBox.Action.CLOSE],
					onClose: function(sAction) {
						if (sAction === MessageBox.Action.RETRY) {
							this.getModel().refreshMetadata();
						}
					}.bind(this)
				}
			);
		},

		createContent: function() {
			var oComponentData = this.getComponentData();

			if (oComponentData && oComponentData.startupParameters && oComponentData.startupParameters.StoreID) {
				// The app is started with startup parameters: navigate to the appropriate views
				var sUrl = "";
				var oHashChanger = HashChanger.getInstance();
				var oRouter = this.getRouter();

				if (oComponentData.startupParameters.ProductID) {
					// The app is started with a given store and article -> navigate directly to detail view
					sUrl = oRouter.getURL("toProductDetails", {
						store: encodeURIComponent(oComponentData.startupParameters.StoreID[0]),
						product: encodeURIComponent(oComponentData.startupParameters.ProductID[0])
					});
				} else {
					// The app is started with a given store -> navigate directly to to the product list of this store
					sUrl = oRouter.getURL("toProductList", {
						store: encodeURIComponent(oComponentData.startupParameters.StoreID[0])
					});
				}

				if (sUrl) {
					oHashChanger.replaceHash(sUrl);
				}
			}

			return UIComponent.prototype.createContent.apply(this, arguments);
		},

		/**
		 * The component is destroyed by UI5 automatically.
		 * In this method, the DataManager is destroyed.
		 * @public
		 * @override
		 */
		destroy: function() {
			// Reset the global objects
			this.oBarcodeScanHandler.reset();
			this.oBarcodeScanHandler.destroy();

			this.oDataManager.destroy();

			// call the base component's destroy function
			UIComponent.prototype.destroy.apply(this, arguments);
		},

		/**
		 * This method can be called to determine whether the sapUiSizeCompact or sapUiSizeCozy
		 * design mode class should be set, which influences the size appearance of some controls.
		 * @public
		 * @return {string} css class, either 'sapUiSizeCompact' or 'sapUiSizeCozy' - or an empty string if no css class should be set
		 */
		getContentDensityClass: function() {
			if (this._sContentDensityClass === undefined) {
				// check whether FLP has already set the content density class; do nothing in this case
				if (jQuery(document.body).hasClass("sapUiSizeCozy") || jQuery(document.body).hasClass("sapUiSizeCompact")) {
					this._sContentDensityClass = "";
				} else if (!Device.support.touch) { // apply "compact" mode if touch is not supported
					this._sContentDensityClass = "sapUiSizeCompact";
				} else {
					// "cozy" in case of touch support; default for most sap.m controls, but needed for desktop-first controls like sap.ui.table.Table
					this._sContentDensityClass = "sapUiSizeCozy";
				}
			}
			return this._sContentDensityClass;
		}

	});

});