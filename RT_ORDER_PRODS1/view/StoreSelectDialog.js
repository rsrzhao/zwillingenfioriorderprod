/*
 * Copyright (C) 2009-2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define(["sap/m/MessageBox","sap/retail/store/lib/reuses1/customControls/SiteSelectDialog"],function(M,S){"use strict";return S.extend("retail.store.orderproducts1.view.StoreSelectDialog",{metadata:{properties:{"dataManager":{type:"object",defaultValue:null}}},_showDataProcessing:function(){return;},_finishDataProcessing:function(){return;},searchSites:function(s,f,e,t,i){this.getProperty("dataManager").readStores({skip:i,top:t,search:s},function(d){f(d.results);},function(E){if(E&&E.message){M.error(E.message);}e(E);});}});});
