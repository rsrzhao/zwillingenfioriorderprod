/*
 * Copyright (C) 2009-2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define([
	"sap/m/MessageBox",
	"sap/retail/store/lib/reuses1/customControls/SiteSelectDialog"
], function(MessageBox, SiteSelectDialog) {
	"use strict";

	return SiteSelectDialog.extend("retail.store.orderproducts1.view.StoreSelectDialog", {
	    
		metadata: {
			properties: {
				"dataManager": {
					type: "object",
					defaultValue: null
				}
			}
		},
		
		// Disable Busy Dialog Handling of the SiteSelectDialog
		_showDataProcessing: function() {
			return;
		},
		
		_finishDataProcessing: function() {
			return;
		}, 

		searchSites: function(sSearchText, fnSuccess, fnError, iTop, iSkip) {
			this.getProperty("dataManager").readStores({
				// Additional Parameters
				skip: iSkip,
				top: iTop,
				search: sSearchText
			}, function(oData) {
				fnSuccess(oData.results);
			}, function(oError){
				if (oError && oError.message){
					MessageBox.error(oError.message);
				}
				fnError(oError);
			});
		}
	});

});