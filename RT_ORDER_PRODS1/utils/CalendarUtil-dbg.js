/*
 * Copyright (C) 2009-2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define([
	"sap/ui/core/format/DateFormat"
], function(DateFormat) {
	"use strict";

	var oCalendarUtil = {

		oStoreWeekDisplayDataDefault: {
			StoreID: "",
			DayOne: {
				IsWeekend: false,
				WeekDayID: 1
			},
			DayTwo: {
				IsWeekend: false,
				WeekDayID: 2
			},
			DayThree: {
				IsWeekend: false,
				WeekDayID: 3
			},
			DayFour: {
				IsWeekend: false,
				WeekDayID: 4
			},
			DayFive: {
				IsWeekend: false,
				WeekDayID: 5
			},
			DaySix: {
				IsWeekend: true,
				WeekDayID: 6
			},
			DaySeven: {
				IsWeekend: true,
				WeekDayID: 7
			}
		},

		init: function(oComponent, oResourceBundle) {
			this._oComponent = oComponent;
			this._oResourceBundle = oResourceBundle;
		},

		getTextKeyForWeekDayID: function(iWeekDayID) {

			var sTextKey = "";
			switch (iWeekDayID) {
				case 1:
					sTextKey = "DETAIL_CALENDAR_MONDAY";
					break;
				case 2:
					sTextKey = "DETAIL_CALENDAR_TUESDAY";
					break;
				case 3:
					sTextKey = "DETAIL_CALENDAR_WEDNESDAY";
					break;
				case 4:
					sTextKey = "DETAIL_CALENDAR_THURSDAY";
					break;
				case 5:
					sTextKey = "DETAIL_CALENDAR_FRIDAY";
					break;
				case 6:
					sTextKey = "DETAIL_CALENDAR_SATURDAY";
					break;
				case 7:
					sTextKey = "DETAIL_CALENDAR_SUNDAY";
					break;
			}

			return sTextKey;
		},

		getCalendarHeaderText: function(oProduct) {

			var sText = "";

			var aWeeks = oProduct.Weeks.results;
			if (aWeeks && aWeeks.length > 0) {
				// Get earliest and latest date in order to adjust the header text of the calendar (one month, two months)
				var sEarliestDate = aWeeks[0].DayOne.Date;
				var sLatestDate = aWeeks[aWeeks.length - 1].DaySeven.Date;

				var oDateFormatYear = DateFormat.getDateInstance({
					pattern: "YYYY",
					UTC: true // Pure dates must always be treated as UTC timestamps with time 00:00
				});
				var oDateFormatMonth = DateFormat.getDateInstance({
					pattern: "MMMM",
					UTC: true // Pure dates must always be treated as UTC timestamps with time 00:00
				});

				var sEarliestMonth = oDateFormatMonth.format(sEarliestDate);
				var sLatestMonth = oDateFormatMonth.format(sLatestDate);

				var sEarliestYear = oDateFormatYear.format(sEarliestDate);
				var sLatestYear = oDateFormatYear.format(sLatestDate);

				if (sEarliestMonth === sLatestMonth) {
					sText = this._oResourceBundle.getText("DETAIL_CALENDAR_MONTH", [sEarliestMonth, sEarliestYear]);
				} else if (sEarliestYear === sLatestYear) {
					sText = this._oResourceBundle.getText("DETAIL_CALENDAR_MONTH_CHANGE", [sEarliestMonth, sLatestMonth,
						sEarliestYear
					]);
				} else {
					sText = this._oResourceBundle.getText("DETAIL_CALENDAR_MONTH_YEAR_CHANGE", [sEarliestMonth, sEarliestYear,
						sLatestMonth, sLatestYear
					]);
				}
			}

			return sText;
		},

		adjustColumnHeader: function(oLabel, sWeekday, oStoreWeekDisplayData) {
			// For every weekday get the respective text key for the column header
			// (e.g. "DETAIL_CALENDAR_MONDAY" for WeekDayID=1)
			if (oLabel && oStoreWeekDisplayData) {
				// Get current weekday ID
				var dCurrentDate = this._oComponent.getCurrentDate();
				var iCurrentWeekDayID = dCurrentDate.getDay();
				if (iCurrentWeekDayID === 0) {
					iCurrentWeekDayID = 7;
				}

				// Get column header text for given weekday
				var sTextKey = oCalendarUtil.getTextKeyForWeekDayID(oStoreWeekDisplayData[sWeekday].WeekDayID);
				oLabel.bindProperty("text", {
					path: "i18n>" + sTextKey
				});

				// If the given weekday corresponds to today's weekday, add a style class to bold the text
				if (oStoreWeekDisplayData[sWeekday].WeekDayID === iCurrentWeekDayID) {
					oLabel.addStyleClass("sapRTSTOrdProdLabelColumnHeaderToday");
				}
			}
		},

		addCustomDataForWeekday: function(oCellControl, sWeekday, oStoreWeekDisplayData) {
			// Set weekend property to cells
			oCellControl.setProperty("isWeekend", oStoreWeekDisplayData[sWeekday].IsWeekend);

			// Add additional custom data (customer hook method)
			oCalendarUtil.addAdditionalCustomDataForWeekday(oCellControl, sWeekday, oStoreWeekDisplayData);
		},

		addAdditionalCustomDataForWeekday: function(oCellControl, sWeekday, oStoreWeekDisplayData) {
			/**
			 * @UtilityHook [Add additional data to the calendar cell] 
			 *           With this hook it is possible to add custom data to the calendar cell
			 * @callback retail.store.orderproducts1.utils.CalendarUtil~extHookAddAdditionalCustomDataForWeekday
			 * @param {object}
			 *          oCellControl The cell control
			 * @param {string}
			 *          sWeekday the weekday of the cell
			 * @param {object}
			 *          oStoreWeekDisplayData the data of the storeweek
			 * @return {void}
			 */
			if (this.extHookAddAdditionalCustomDataForWeekday) {
				this.extHookAddAdditionalCustomDataForWeekday(oCellControl, sWeekday, oStoreWeekDisplayData);
			}
			return;
		}

	};

	return oCalendarUtil;

});