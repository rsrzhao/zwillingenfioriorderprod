/*
 * Copyright (C) 2009-2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define([

], function() {
	"use strict";

	var oFacetFilterUtil = {

		getActiveFacetFilterLists: function(oFacetFilter) {
			// Build an array of facet filter lists, which are active and have at least one selected item
			var mFacetFilterLists = oFacetFilter.getLists().filter(function(oList) { // "filter" is a callback function
				return oList.getActive() && (oList.getSelectedItems().length > 0);
			});
			return mFacetFilterLists;
		},

		resetFacetFilter: function(oFacetFilter) {

			var i = 0;

			// Get all facet filter lists
			var aFacetFilterLists = oFacetFilter.getLists();
			for (i = 0; i < aFacetFilterLists.length; i++) {
				// For every facet filter list: remove all selected keys
				aFacetFilterLists[i].setSelectedKeys();
			}
		},

		buildFilterObjectForSingleList: function(oList) {

			var oFilter = null;
			var sAttribute = "";

			if (oList.getKey() === "Attributes") {
				// The attributes of the facet filter list (normally) correspond to boolean fields in the StoreProduct entity
				oFilter = new sap.ui.model.Filter(oList.getSelectedItems().map(function(oItem) { // "map" is a callback function
					sAttribute = oItem.getKey();
					if (sAttribute === "OrderQuantity") {
						// Special handling for OrderQuantity: "Zero Order Quantity" means value 0
						return new sap.ui.model.Filter(oItem.getKey(), "EQ", 0);
					} else if (sAttribute === "IsOrderQuantityNotProcessed") {
						return new sap.ui.model.Filter("IsOrderQuantityProcessed", "EQ", false);
					} else if (sAttribute === "IsChangedOrderQuantityPosted") {
						return new sap.ui.model.Filter("IsChangedOrderQuantityPosted", "EQ", false);
					} else if (sAttribute === "MustBeReviewed") {
						return new sap.ui.model.Filter("OrderQuantityReviewUrgency", "EQ", "1");
					} else if (sAttribute === "ShouldBeReviewed") {
						return new sap.ui.model.Filter("OrderQuantityReviewUrgency", "EQ", "2");
					} else {
						return new sap.ui.model.Filter(oItem.getKey(), "EQ", true);
					}
				}), false);
			} else if (oList.getKey()) {
				// The key of the facet filter list defines a specific attribute of the StoreProduct entity
				var sPath = oList.getKey();

				// The keys of the facet filter list items correspond to field values of the given attribute.
				// A filter is created where all these values are ANDed
				oFilter = new sap.ui.model.Filter(oList.getSelectedItems().map(function(oItem) { // "map" is a callback function
					return new sap.ui.model.Filter(sPath, "EQ", oItem.getKey());
				}), false);
			}

			return oFilter;
		},

		mergeFilterValuesToUserDefaultValues: function(oStoreData, oData) {
			var i, j;

			var aUserDefaultFilters = oStoreData.Filters.results;
			oStoreData.Filters = oData;

			var iLengthUDF = aUserDefaultFilters.length;
			var iLengthF = oStoreData.Filters.results.length;
			for (i = 0; i < iLengthUDF; i++) {
				for (j = 0; j < iLengthF; j++) {
					if (oStoreData.Filters.results[j].OrderProductFilterID === aUserDefaultFilters[i].OrderProductFilterID) {
						oStoreData.Filters.results[j].UserDefaultFilterValues = aUserDefaultFilters[i].UserDefaultFilterValues;
					}
				}
			}
		},

		isPagingForbiddenForFilter: function(oFacetFilter) {
			var bForbidden = false;
			var aLists = oFacetFilter.getLists();
			for (var i = 0; i < aLists.length; i++) {
				// Check if for any facet filter list the paging is forbidden
				if (this.isPagingForbiddenForFilterList(aLists[i])) {
					bForbidden = true;
					break;
				}
			}
			return bForbidden;
		},

		isPagingForbiddenForFilterList: function(oList) {
			var bForbidden = false;
			var sKey = "";
			if (oList.getKey() === "Attributes" && oList.getActive()) {
				// Check if any of the 'critical' attribute filters is selected:
				// If the user performs changes in the app (setting product to processed or changing the quantity), these changes may
				// change the data which is selected by the current filter. In this case it is not possible to use backend paging since
				// it may lead to missing products on the UI after paging. As a solution, the paging must be disabled in these cases.
				var aItems = oList.getSelectedItems();
				for (var i = 0; i < aItems.length; i++) {
					sKey = aItems[i].getKey();
					if (sKey === "IsOrderQuantityProcessed" || sKey === "IsOrderQuantityNotProcessed" ||
						sKey === "IsChangedOrderQuantityPosted" || sKey === "OrderQuantity") {
						bForbidden = true;
						break;
					}
				}
			}
			return bForbidden;
		},

		buildFilterObject: function(oFacetFilter) {

			var oFilter = null;

			if (oFacetFilter) {
				// Get the active facet filter lists
				var mFacetFilterLists = oFacetFilterUtil.getActiveFacetFilterLists(oFacetFilter);

				if (mFacetFilterLists.length > 0) {
					// For every facet filter list build a filter object and combine them all to a single filter object with
					// AND-link
					oFilter = new sap.ui.model.Filter(mFacetFilterLists.map(function(oList) {
						return oFacetFilterUtil.buildFilterObjectForSingleList(oList);
					}), true);
				}
			}

			return oFilter;
		},

		buildFilterObjectWithPersOData: function(oFacetFilter) {
			if (!oFacetFilter) {
				return null;
			}

			// Select the user default filter values in the facet filter
			oFacetFilterUtil.selectUserDefaultValuesInFilter(oFacetFilter);

			// Build the facet filter object and return it
			var oFilter = oFacetFilterUtil.buildFilterObject(oFacetFilter);
			return oFilter;
		},

		selectUserDefaultValuesInFilter: function(oFacetFilter) {
			var i, j, k;
			var iLengthLists, iLengthItems, iLengthItemsPers;
			var aItemsAvailable = [];
			var aUserDefaultFilterValues = [];

			var aFacetFilterLists = oFacetFilter.getLists();
			iLengthLists = aFacetFilterLists.length;
			for (i = 0; i < iLengthLists; i++) {
				// Check if filter personalization is actually enabled for the current filter ID
				if (!oFacetFilterUtil.getPersEnablementForFilterID(aFacetFilterLists[i].getKey())) {
					continue;
				}

				// Get all available items for this facet filter list
				aItemsAvailable = aFacetFilterLists[i].getItems();
				iLengthItems = aItemsAvailable.length;

				// Get personalized filter values
				aUserDefaultFilterValues = aFacetFilterLists[i].getBindingContext().getProperty("UserDefaultFilterValues/results");
				iLengthItemsPers = aUserDefaultFilterValues.length;

				for (k = 0; k < iLengthItems; k++) {
					// First de-select all items to make sure that no 'keep-selection' logic interferes
					aItemsAvailable[k].setSelected(false);
					for (j = 0; j < iLengthItemsPers; j++) {
						if (aItemsAvailable[k].getKey() === aUserDefaultFilterValues[j].OrderProductFilterValueID) {
							aItemsAvailable[k].setSelected(true);
							break;
						}
					}
				}
			}
		},

		getSelectedFilterValues: function(oFacetFilter) {
			var i, j;
			var aItems = [];
			var oFilters = {};
			var sFilterID = "";

			// Get the active facet filter lists
			var mFacetFilterLists = oFacetFilterUtil.getActiveFacetFilterLists(oFacetFilter);

			for (i = 0; i < mFacetFilterLists.length; i++) {
				// Get the current filter ID
				sFilterID = mFacetFilterLists[i].getKey();

				// Check if the values of the current filter ID should be personalized:
				// - "Attributes" filter values are never personalized
				// - For every other filter ID, call a specific function which can be enhanced by customers
				if (sFilterID !== "Attributes" && oFacetFilterUtil.getPersEnablementForFilterID(sFilterID)) {
					oFilters[sFilterID] = {};

					aItems = mFacetFilterLists[i].getSelectedItems();
					for (j = 0; j < aItems.length; j++) {
						// Set the filter value to selected
						oFilters[sFilterID][aItems[j].getKey()] = true;
					}
				}
			}
			return oFilters;
		},

		addUserDefaultValuesToFilterValues: function(oData) {
			if (!oData || !oData.Filters || !oData.Filters.results) {
				return;
			}

			var iLength = oData.Filters.results.length;
			for (var i = 0; i < iLength; i++) {
				oData.Filters.results[i].FilterValues = oData.Filters.results[i].UserDefaultFilterValues;
			}
		},

		getPersEnablementForFilterID: function(sOrderProductFilterID) {
			var bGetPersEnablementForFilterID = true; //Default

			/**
			 * @UtilityHook 
			 * With this hook it is possible to determine whether a several filter should be presonalized or not
			 * By default all filters are personalized (except for "Attributes").
			 * @callback retail.store.orderproducts1.utils.FacetFilterUtil~extHookGetPersEnablementForFilterID
			 * @param {string}
			 *      sOrderProductFilterID the id of the filter
			 * @return {boolean} Whether the given filter should be personalized or not
			 */
			if (this.extHookGetPersEnablementForFilterID) {
				var bGetPersEnablementForFilterIDHook = this.extHookGetPersEnablementForFilterID(sOrderProductFilterID);

				// Check if a boolean is returned
				if (typeof bRepdigitCheckEnabledHook === "boolean") {
					bGetPersEnablementForFilterID = bGetPersEnablementForFilterIDHook;
				}
			}
			return bGetPersEnablementForFilterID;
		}

	};

	return oFacetFilterUtil;

});