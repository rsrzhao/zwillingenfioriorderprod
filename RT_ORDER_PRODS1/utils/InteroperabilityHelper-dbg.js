/*
 * Copyright (C) 2009-2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define([], function() {
	"use strict";

	return {

		init: function(oModel) {
			this._oEntitySchemaVersion = {};
			this._mInteropProperties = {};
			
			if (oModel && !this._oMetadata) {
				// Save the service metadata
				this._oMetadata = oModel.getServiceMetadata();

				// Create interoperability properties based on metadata
				this.createInteroperabilityProperties();
			}
		},

		getEntitySchemaVersion: function(sEntityType) {
			var iVersion;

			if (this._oEntitySchemaVersion[sEntityType]) {
				iVersion = this._oEntitySchemaVersion[sEntityType];
			} else {
				// Get the version from the entity type annotation
				var sVersion = this._getEntityTypeAnnotation(this._oMetadata, "service-schema-version", sEntityType);
				if (sVersion) {
					// Convert the version to integer
					iVersion = parseInt(sVersion);
				} else {
					// Default version: 1
					iVersion = 1;
				}
				this._oEntitySchemaVersion[sEntityType] = iVersion;
			}
			return iVersion;
		},

		_getEntityTypeAnnotation: function(oMetadata, sAnnotationName, sEntityType) {
			var i = 0;
			var j = 0;

			// Check for proper metadata structure
			if (oMetadata && oMetadata.dataServices && oMetadata.dataServices.schema && oMetadata.dataServices.schema.length > 0 && oMetadata.dataServices
				.schema[0].entityType) {
				// Get the entity types
				var aEntityTypes = oMetadata.dataServices.schema[0].entityType;

				// Loop the entity types
				for (i = 0; i < aEntityTypes.length; i++) {
					if ((sEntityType === undefined || sEntityType === aEntityTypes[i].name) && aEntityTypes[i].extensions) {
						// Loop the annotations of the entity type
						for (j = 0; j < aEntityTypes[i].extensions.length; j++) {
							if (aEntityTypes[i].extensions[j].name === sAnnotationName) {
								return aEntityTypes[i].extensions[j].value;
							}
						}
					}
				}
			}
			return null;
		},

		createInteroperabilityProperties: function() {
		},

		setInteropProperties: function(mInteropProperties) {
			this._mInteropProperties = mInteropProperties;
		},

		getInteropProperties: function() {
			return this._mInteropProperties;
		},
		
		cleanup: function() {
			this._oMetadata = null;
		}

	};

});