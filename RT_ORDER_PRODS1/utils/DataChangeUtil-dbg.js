/*
 * Copyright (C) 2009-2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define([
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"sap/retail/store/lib/reuses1/util/Formatter",
	"sap/retail/store/lib/reuses1/util/TextUtil"
], function(MessageBox, MessageToast, Reuses1Formatter, Reuses1TextUtil) {
	"use strict";

	var oDataChangeUtil = {

		init: function(oComponent, oDataManager, oResourceBundle) {
			this._sCurrentTimeOutID = "";
			this._oComponent = oComponent;
			this._oDataManager = oDataManager;
			this._oResourceBundle = oResourceBundle;
		},

		submitProductChanges: function(sStoreID, sProductID, fnSuccess, fnError) {
			// Check if there is a timeout for delayed submit and cancel it
			if (this._sCurrentTimeOutID) {
				jQuery.sap.clearDelayedCall(this._sCurrentTimeOutID);
				this._sCurrentTimeOutID = "";
			}

			// Get current (maybe changed) order quantity before it is sent to the backend
			var oProduct = this._oDataManager.getSingleProduct(null, sProductID);
			var fOrderQuantityOld = oProduct && Number(oProduct.OrderQuantity);
			var bOrderQuantityChanged = oProduct && oProduct._bOrderQuantityChanged;

			var sErrorMessage = this._oResourceBundle.getText("ERROR_MSG_SUBMIT_FAILED");
			this._oDataManager.submitProductChanges(sStoreID, sProductID, jQuery.proxy(function(oResponseUpdate,
				oResponseRead) {
				// Batch request was successful or no update was necessary at all
				if (oResponseUpdate) {
					// Check if the change response was successful
					var oChangeResponse = oResponseUpdate.__changeResponses && oResponseUpdate.__changeResponses[0];
					if (oChangeResponse && oChangeResponse.statusCode === "204") {
						// Display a message toast in case the order quantity was changed again (maybe rounded) by the backend
						if (oResponseRead && oResponseRead.data && typeof oResponseRead.data.OrderQuantity !== "undefined" && bOrderQuantityChanged &&
							Number(oResponseRead.data.OrderQuantity) !== fOrderQuantityOld) {
							var sMessage = this._oResourceBundle.getText("INFO_MSG_QUANTITY_ROUNDED", [oResponseRead.data.OrderQuantity,
								oResponseRead.data.OrderQuantityUnitCode
							]);
							MessageToast.show(sMessage);
						}

						// Call the success callback if given
						if (typeof fnSuccess === "function") {
							fnSuccess(oResponseUpdate, oResponseRead);
						}
					} else {
						// Update was not successful
						this.showErrorMessageForResponse(sErrorMessage, oResponseUpdate, fnError);
					}
				} else {
					// Call the success callback if given
					if (typeof fnSuccess === "function") {
						fnSuccess(oResponseUpdate, oResponseRead);
					}
				}
			}, this), jQuery.proxy(function(oError) {
				// Error during submit
				this.showErrorMessageForResponse(sErrorMessage, oError, fnError);
			}, this));
		},

		showErrorMessageForResponse: function(sMessage, oResponse, fnCallback) {
			var fnClose = function() {
				// If a callback function is given, execute it and pass the response
				if (typeof fnCallback === "function") {
					fnCallback(oResponse);
				}
			};

			// Show an error popup with the detailed error message; suppress the generic standard error message if a detailed message is available
			var sErrorDetail = Reuses1TextUtil.getMessageForErrorResponses([oResponse]);
			if (!sErrorDetail) {
				sErrorDetail = sMessage;
			}
			MessageBox.error(sErrorDetail, {
				onClose: fnClose
			});
		},

		createDelayedSubmit: function(oProduct) {
			// Check if there is already a timeout for delayed submit
			if (this._sCurrentTimeOutID) {
				jQuery.sap.clearDelayedCall(this._sCurrentTimeOutID);
			}

			// Create a new delayed submit
			this._sCurrentTimeOutID = jQuery.sap.delayedCall(5000, null, jQuery.proxy(function() {
				// Check if after 5 seconds the same store product is still selected
				var sSelectedStoreProduct = this._oComponent.getSelectedStoreProduct();
				if (sSelectedStoreProduct && oProduct && sSelectedStoreProduct === oProduct.ProductID) {
					// Same store product still selected --> set it to processed, refresh the UI and submit the change
					this._oDataManager.setProductProcessed(sSelectedStoreProduct);
					this._oDataManager.triggerProductRefresh([sSelectedStoreProduct]);
					oDataChangeUtil.submitProductChanges(oProduct.StoreID, sSelectedStoreProduct);
				}
				this._sCurrentTimeOutID = "";
			}, this)); // 5sec
		},

		increaseOrderQuantity: function(oProduct) {
			var sMessage = "";
			var fChangedOrderQuantity = null;
			var fOldOrderQuantity = oProduct.OrderQuantity * 1; // convert from string to number
			var fMaxOrderQuantity = oDataChangeUtil.getMaxOrderQuantity(oProduct);

			if (fOldOrderQuantity < fMaxOrderQuantity) {
				var fNewOrderQuantity = fOldOrderQuantity + 1;
				if (fNewOrderQuantity > fMaxOrderQuantity) {
					// Set it to the maximum
					fNewOrderQuantity = fMaxOrderQuantity;
				}

				// Execute a "hard error" check which prevents increasing the order quantity
				var oHardErrorCheck = oDataChangeUtil.orderQuantityHardErrorCheck(fNewOrderQuantity, oProduct);
				if (oHardErrorCheck && oHardErrorCheck.error) {
					// Per default set a generic error message in the popup
					sMessage = this._oResourceBundle.getText("ERROR_MSG_ORDER_QUANTITY_GENERIC");
					if (oHardErrorCheck.message) {
						// Take the error message provided by the custom check
						sMessage = oHardErrorCheck.message;
					}

					// Display error popup
					MessageBox.error(sMessage);
				} else {
					// Set the new order quantity
					this._oDataManager.setProductNewOrderQuantity(oProduct.ProductID, fNewOrderQuantity);
					fChangedOrderQuantity = fNewOrderQuantity;

					// Create a delayed submit, so that after a certain inactivity time the data is sent automatically
					// to the backend for update
					oDataChangeUtil.createDelayedSubmit(oProduct);
				}
			}
			return fChangedOrderQuantity;
		},

		decreaseOrderQuantity: function(oProduct) {
			var sMessage = "";
			var fChangedOrderQuantity = null;
			var fOldOrderQuantity = oProduct.OrderQuantity * 1; // convert from string to number

			if (fOldOrderQuantity > 0) {
				var fNewOrderQuantity = fOldOrderQuantity - 1;
				if (fNewOrderQuantity < 0) {
					// Set it to zero
					fNewOrderQuantity = 0;
				}

				// Execute a "hard error" check which prevents decreasing the order quantity
				var oHardErrorCheck = oDataChangeUtil.orderQuantityHardErrorCheck(fNewOrderQuantity, oProduct);
				if (oHardErrorCheck && oHardErrorCheck.error) {
					// Per default set a generic error message in the popup
					sMessage = this._oResourceBundle.getText("ERROR_MSG_ORDER_QUANTITY_GENERIC");
					if (oHardErrorCheck.message) {
						// Take the error message provided by the custom check
						sMessage = oHardErrorCheck.message;
					}

					// Display error popup
					MessageBox.error(sMessage);
				} else {
					// Set the new order quantity
					this._oDataManager.setProductNewOrderQuantity(oProduct.ProductID, fNewOrderQuantity);
					fChangedOrderQuantity = fNewOrderQuantity;

					// Create a delayed submit, so that after a certain inactivity time the data is sent automatically
					// to the backend for update
					oDataChangeUtil.createDelayedSubmit(oProduct);
				}
			}
			return fChangedOrderQuantity;
		},

		// changeOrderQuantity returns "true" if error/warning popup is displayed
		changeOrderQuantity: function(oProduct, fNewOrderQuantity, oInput, oOtherInput, fnBeforeUpdate, fnOnPopupClose, fnSuccess, fnError) {
			var sMessage = "";

			var fnRefreshInput = function() {
				// Reset the value of the input field to the last valid state (also for input field from other layout)
				oInput.getBinding("value").refresh(true);
				if (oOtherInput) {
					oOtherInput.getBinding("value").refresh(true);
				}
			};

			if (fNewOrderQuantity === "" || isNaN(fNewOrderQuantity) || fNewOrderQuantity < 0) {
				// No positive numeric number was entered -> display an error popup
				sMessage = "";

				if (fNewOrderQuantity === "" || isNaN(fNewOrderQuantity)) {
					sMessage = this._oResourceBundle.getText("ERROR_MSG_NOT_NUMERIC");
				} else if (fNewOrderQuantity < 0) {
					sMessage = this._oResourceBundle.getText("ERROR_MSG_NEG_NUMBER");
				}

				MessageBox.error(sMessage, {
					onClose: fnOnPopupClose
				});

				// Reset the value of the input fields to the last valid state
				fnRefreshInput();
				return true;
			}

			var fMaxOrderQuantity = oDataChangeUtil.getMaxOrderQuantity(oProduct);

			if (fNewOrderQuantity > fMaxOrderQuantity) {
				// If maximal order quantity is exceeded -> display an error popup
				var sOrderUoM = oProduct.OrderQuantityUnitName;
				var sFormattedQuantity = Reuses1Formatter.formatQuantityMaxOneDigitWithUnit(fMaxOrderQuantity, sOrderUoM);

				MessageBox.error(this._oResourceBundle.getText("ERROR_MSG_MAX_QTY_EXCEEDED", [sFormattedQuantity]), {
					onClose: fnOnPopupClose
				});

				// Reset the value of the input fields to the last valid state
				fnRefreshInput();
				return true;
			}

			// Execute a "hard error" check which leads to an error message and a reset of the order quantity
			var oHardErrorCheck = oDataChangeUtil.orderQuantityHardErrorCheck(fNewOrderQuantity, oProduct);
			if (oHardErrorCheck && oHardErrorCheck.error) {
				// Per default set a generic error message in the popup
				sMessage = this._oResourceBundle.getText("ERROR_MSG_ORDER_QUANTITY_GENERIC");
				if (oHardErrorCheck.message) {
					// Take the error message provided by the custom check
					sMessage = oHardErrorCheck.message;
				}

				// Display error popup
				MessageBox.error(sMessage, {
					onClose: fnOnPopupClose
				});

				// Reset the value of the input fields to the last valid state
				fnRefreshInput();
				return true;
			}

			// Execute a "soft" check which leads to a warning message where the user can confirm or reset the entered quantity
			var oSoftCheck = oDataChangeUtil.orderQuantitySoftCheck(fNewOrderQuantity, oProduct);

			var oDialog = new sap.m.Dialog({
				title: this._oResourceBundle.getText("QUANTITY_CONFIRMATION_MSG_TITLE"),
				type: "Message",
				beginButton: new sap.m.Button({
					text: this._oResourceBundle.getText("QUANTITY_CONFIRMATION_MSG_BTN_LABEL"),
					press: function(oEvent) {
						oDataChangeUtil.updateNewOrderQuantity(oProduct, fNewOrderQuantity, fnBeforeUpdate, fnSuccess,
							fnError);
						oEvent.getSource().getParent().close();
					}
				}),
				endButton: new sap.m.Button({
					text: this._oResourceBundle.getText("QUANTITY_CONFIRMATION_MSG_CANCEL_BTN_LABEL"),
					press: function(oEvent) {
						fnRefreshInput();
						oEvent.getSource().getParent().close();
					}
				}),
				afterClose: function(oEvent) {
					oEvent.getSource().destroyContent();
				}
			});

			if (oSoftCheck && oSoftCheck.requestConfirmation) {
				// Per default set a generic confirmation message in the popup
				var sQuestion = this._oResourceBundle.getText("QUANTITY_CONFIRMATION_MSG_QUESTION_GENERIC");
				if (oSoftCheck.message) {
					// Take the confirmation message provided by the custom check
					sQuestion = oSoftCheck.message;
				}

				// Add Content to the Dialog
				oDialog.addContent(new sap.m.Text({
					text: sQuestion
				}));
				// Open the confirmation dialog
				oDialog.open();

				return true;

			} else if ((oSoftCheck === null || oSoftCheck === undefined) && oDataChangeUtil.repdigitCheckEnabled() &&
				oDataChangeUtil.containsRepdigit(fNewOrderQuantity)) { // Check for repdigit
				// (e.g. 44, 333, 355)

				// Add Content to the Dialog
				oDialog.addContent(new sap.m.Text({
					text: this._oResourceBundle.getText("QUANTITY_CONFIRMATION_MSG_QUESTION")
				}));
				// Open the confirmation dialog
				oDialog.open();

				return true;

			} else {
				// Send update of order quantity if no customer specific soft check has been implemented or the customer
				// specific soft check doesn't return an issue and our repdigit test was successful or suppressed
				oDataChangeUtil.updateNewOrderQuantity(oProduct, fNewOrderQuantity, fnBeforeUpdate, fnSuccess, fnError);
			}
		},

		containsRepdigit: function(fQuantity) {
			var i = 0;
			var sQuantity = "";

			if (typeof fQuantity !== "string") {
				sQuantity = fQuantity.toString();
			} else {
				sQuantity = fQuantity;
			}

			for (i = 0; i < sQuantity.length; i++) {
				if (i > 0 && (sQuantity.charAt(i) === sQuantity.charAt(i - 1))) {
					return true;
				}
			}

			return false;
		},

		updateNewOrderQuantity: function(oProduct, fNewOrderQuantity, fnBeforeUpdate, fnSuccess, fnError) {
			// Execute the callback function before doing the actual update
			if (typeof fnBeforeUpdate === "function") {
				fnBeforeUpdate();
			}

			// Set the new order quantity in the data manager and set the product to processed
			this._oDataManager.setProductNewOrderQuantity(oProduct.ProductID, fNewOrderQuantity, true);

			// Submit the changes to the backend
			oDataChangeUtil.submitProductChanges(oProduct.StoreID, oProduct.ProductID, fnSuccess, fnError);
		},

		repdigitCheckEnabled: function() {
			var bRepdigitCheckEnabled = false; //Default

			/**
			 * @UtilityHook 
			 * When entering a repdigit as a new order quantity there is a possibility to ask for a confirmation if this amount is right
			 * By default this check is disabled.
			 * @callback retail.store.orderproducts1.utils.DataChangeUtil~extHookGetRepDigitCheckEnabled
			 * @return {boolean} Whether the repdigit-check should be enabled or not
			 */
			if (this.extHookGetRepDigitCheckEnabled) {
				var bRepdigitCheckEnabledHook = this.extHookGetRepDigitCheckEnabled();

				// Check if a boolean is returned
				if (typeof bRepdigitCheckEnabledHook === "boolean") {
					bRepdigitCheckEnabled = bRepdigitCheckEnabledHook;
				}
			}
			return bRepdigitCheckEnabled;
		},

		orderQuantityHardErrorCheck: function(fNewOrderQuantity, oProduct) {
			var oOrderQuantityHardErrorCheck = null; //Default

			/**
			 * @UtilityHook [Provide an object for a hard error check]
			 * When changing the order quantity, you can do a hard error check for the changed product and the entered quantity.
			 * By default this check is disabled.
			 * @callback retail.store.orderproducts1.utils.DataChangeUtil~extHookGetOrderQuantityHardErrorCheck
			 * @param {float} fNewOrderQuantity The entered order quantity
			 * @param {object} oProduct The changed product
			 * @return {object} The object which contains information for the possible error handling. 
			 *                  To display an error the error attribute of the object must be filled
			 *                  To display a custom error message the message attribute of the object must be filled,
			 *                  otherwise a generic message will be displayed
			 */
			if (this.extHookGetOrderQuantityHardErrorCheck) {
				oOrderQuantityHardErrorCheck = this.extHookGetOrderQuantityHardErrorCheck(fNewOrderQuantity, oProduct);
			}
			return oOrderQuantityHardErrorCheck;
		},

		orderQuantitySoftCheck: function(fNewOrderQuantity, oProduct) {
			var oOrderQuantitySoftCheck = null; //Default

			/**
			 * @UtilityHook [Provide an object for a soft check]
			 * When changing the order quantity, you can do a soft check for the changed product and the entered quantity.
			 * By default this check is disabled.
			 * @callback retail.store.orderproducts1.utils.DataChangeUtil~extHookGetOrderQuantitySoftCheck
			 * @param {float} fNewOrderQuantity The entered order quantity
			 * @param {object} oProduct The changed product
			 * @return {object} The object which contains information for the possible confirmation dialog. 
			 *                  To display an confirmation dialog the requestConfirmation attribute of the object must be filled
			 *                  To display a custom message the message attribute of the object must be filled,
			 *                  otherwise a generic message will be displayed
			 */
			if (this.extHookGetOrderQuantitySoftCheck) {
				oOrderQuantitySoftCheck = this.extHookGetOrderQuantitySoftCheck(fNewOrderQuantity, oProduct);
			}
			return oOrderQuantitySoftCheck;
		},

		getMaxOrderQuantity: function(oProduct) {
			var iMaxOrderQuantity = 9999; //Default

			/**
			 * @UtilityHook [Provide max. Order Quantity for one Product]
			 * When changing the order quantity, the entered amount is not allowed to exceed the defined maximum amount.
			 * By default this is 9999.
			 * @callback retail.store.orderproducts1.utils.DataChangeUtil~extHookGetMaxOrderQuantity
			 * @param {object} oProduct The changed product
			 * @return {number} The max. Order Quantity for one Product
			 */
			if (this.extHookGetMaxOrderQuantity) {
				var iMaxOrderQuantityHook = this.extHookGetMaxOrderQuantity(oProduct);

				// Check if a positive number is returned and then take the new value
				if (typeof iMaxOrderQuantityHook === "number" && iMaxOrderQuantityHook > 0) {
					iMaxOrderQuantity = iMaxOrderQuantityHook;
				}
			}
			return iMaxOrderQuantity;
		}

	};

	return oDataChangeUtil;

});