/*
 * Copyright (C) 2009-2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define([
	"retail/store/orderproducts1/utils/FacetFilterUtil"
], function(FacetFilterUtil) {
	"use strict";

	var oScanUtil = {

		_oRegExpLeadingZero: /^0*([0-9]*)$/,
		
		init: function(oResourceBundle){
			this._sCurrentTimeOutID = "";
			this._oResourceBundle = oResourceBundle;  
		},

		handleInputLiveChange: function(sGTIN, fnCallback) {
			// The automatic processing should only work on smartphone/tablet
			// Additionally it first has to be activated by customers
			if (sap.ui.Device.system.desktop || oScanUtil.isAutomaticGTINProcessingEnabled() === false) {
				return;
			}

			// If there is already a timeout, cancel it now
			if (this._sCurrentTimeOutID) {
				clearTimeout(this._sCurrentTimeOutID);
			}

			if (sGTIN) {
				// Create a new timeout for processing the entered GTIN
				this._sCurrentTimeOutID = setTimeout(function() {
					// After 300 ms automatically call the callback function with the current GTIN
					if (fnCallback) {
						fnCallback(sGTIN);
					}
					this._sCurrentTimeOutID = "";
				}, 300); // 300 ms
			}
		},

		isBarcodeNumeric: function(sBarcode) {
			var bNumeric = true;

			// Only integer numbers are valid
			if (sBarcode % 1 !== 0) {
				bNumeric = false;
			}

			if (bNumeric && typeof sBarcode === "string" && sBarcode.indexOf(".") > -1) {
				bNumeric = false;
			}

			return bNumeric;
		},

		checkBarcodeMatchForProduct: function(sBarcode, oProduct) {
			var bMatch = false;

			// Check if main GTIN matches to the barcode
			bMatch = this.checkBarcodeMatchForGTIN(sBarcode, oProduct.GlobalTradeItemNumber);

			if (!bMatch && oProduct.AssignedGTINs) {
				// Get all available GTINs for the product
				var aGTINs = oProduct.AssignedGTINs.split(",");
				var iLength = aGTINs.length;

				// Check if any of the given GTINs matches to the barcode
				for (var i = 0; i < iLength; i++) {
					if (this.checkBarcodeMatchForGTIN(sBarcode, aGTINs[i])) {
						bMatch = true;
						break;
					}
				}
			}

			// Call utility hook function where addtional barcode checks can be performed
			if (!bMatch && this.utilityExtHookCheckBarcodeMatchForProduct) {
				bMatch = this.utilityExtHookCheckBarcodeMatchForProduct(sBarcode, oProduct);
			}

			return bMatch;
		},

		checkBarcodeMatchForGTIN: function(sBarcode, sGTIN) {
			var bMatch = false;
			var sBarcodeWOZeros = this.removeLeadingZeros(sBarcode);

			// Check if barcode matches the GTIN
			if (sBarcode == sGTIN || sBarcodeWOZeros == sGTIN) {
				bMatch = true;
			}

			if (!bMatch && sBarcodeWOZeros.length === 13 && sGTIN.length >= 12) {
				// Check EAN 21-29: compare only the first 12 digits with 5 zeros at the end
				var sTestGTIN = sBarcode.substr(0, 7) + "00000";
				var sGTIN12 = sGTIN.substr(0, 12);
				var rEAN = new RegExp('^2[1-9]');
				if (sTestGTIN === sGTIN12 && rEAN.test(sGTIN12)) {
					bMatch = true;
				}
			}

			if (!bMatch && this.utilityExtHookCheckBarcodeMatchForGTIN) {
				bMatch = this.utilityExtHookCheckBarcodeMatchForGTIN(sBarcode, sGTIN);
			}

			return bMatch;
		},

		removeLeadingZeros: function(sValue) {
			var sResult = "";
			var aPartsLeadingZero = this._oRegExpLeadingZero.exec(sValue);
			if (aPartsLeadingZero && aPartsLeadingZero[1]) {
				sResult = aPartsLeadingZero[1];
			}
			return sResult;
		},

		buildFilterObjectWithStoreProduct: function(oFacetFilter, oStoreProduct, sGTIN) {
			if (!oFacetFilter || !oStoreProduct) {
				return null;
			}

			if (oScanUtil.isMerchandiseCategorySelectionEnabled(oStoreProduct, sGTIN)) {
				// Get merchandise category of scanned product
				var sMerchCatScanned = oStoreProduct.MerchandiseCategory;

				// Select the merchandise category of the scanned store product
				oScanUtil.selectMerchandiseCategoryFilterItem(oFacetFilter, sMerchCatScanned);
			}

			// Select additional filter items based on the scanned store product by customer implementation
			oScanUtil.selectAdditionalFilterItems(oFacetFilter, oStoreProduct, sGTIN);

			// Build and return the oData filter object
			var oFilter = FacetFilterUtil.buildFilterObject(oFacetFilter);
			return oFilter;
		},

		selectMerchandiseCategoryFilterItem: function(oFacetFilter, sMerchandiseCategory) {
			// Get merchandise category facet filter list
			var oMerchCatList = oFacetFilter.getLists().filter(function(oList) {
				return (oList.getKey() === "MerchandiseCategory");
			})[0];

			if (oMerchCatList) {
				// Get the corresponding item of the facet filter list (for given merchandise category)
				var oMerchCatItem = oMerchCatList.getItems().filter(function(oItem) {
					return (oItem.getKey() === sMerchandiseCategory);
				})[0];

				if (oMerchCatItem) {
					// Select the scanned merchandise category
					oMerchCatItem.setSelected(true);
				}
			}
		},

		isMerchandiseCategorySelectionEnabled: function(oStoreProduct, sGTIN) {
			var bIsMerchandiseCategorySelectionEnabled = true; //Default
		    
		    /**
			 * @UtilityHook 
			 * With this hook it is possible to determine whether the selection of the merchandise category of the 
			 * scanned product should be enabled or not.
			 * By default this selection is enabled
			 * @callback retail.store.orderproducts1.utils.ScanUtil~extHookisMerchandiseCategorySelectionEnabled
			 * @param {object}
			 *      oStoreProduct the scanned product
			 * @param {string}
			 *      sGTIN the GTIN of the scanned product
			 * @return {boolean} Whether the selection of the merchandise category of the scanned product should be enabled
			 */
			if (this.extHookisMerchandiseCategorySelectionEnabled) {
				var bIsMerchandiseCategorySelectionEnabledHook = this.extHookisMerchandiseCategorySelectionEnabled(oStoreProduct, sGTIN);

				// Check if a boolean is returned
				if (typeof bIsMerchandiseCategorySelectionEnabledHook === "boolean") {
					bIsMerchandiseCategorySelectionEnabled = bIsMerchandiseCategorySelectionEnabledHook;
				}
			}
			return bIsMerchandiseCategorySelectionEnabled;
		},

		isAutomaticGTINProcessingEnabled: function() {
			// Per default the automatic GTIN processing for the live change event of the input
			// field in the scan popup is disabled. Return "true" if the currently entered GTIN
			// should automatically be processed after 300 ms (for smartphone/tablet)
			var bIsAutomaticGTINProcessingEnabled = false; //Default
		    
		    /**
			 * @UtilityHook 
			 * Per default the automatic GTIN processing for the live change event of the inout field
			 * in the scan popup is disabled. With this hook it is possible to change this setting.
			 * Then the currently entered GTIN should be processed after 300ms automatically.
			 * (For smartphone/tablet)
			 * @callback retail.store.orderproducts1.utils.ScanUtil~extHookIsAutomaticGTINProcessingEnabledHook
			 * @return {boolean} Whether the automatic GTIN processing should be enabled
			 */
			if (this.extHookIsAutomaticGTINProcessingEnabledHook) {
				var bIsAutomaticGTINProcessingEnabledHook = this.extHookIsAutomaticGTINProcessingEnabledHook();

				// Check if a boolean is returned
				if (typeof bIsAutomaticGTINProcessingEnabledHook === "boolean") {
					bIsAutomaticGTINProcessingEnabled = bIsAutomaticGTINProcessingEnabledHook;
				}
			}
			return bIsAutomaticGTINProcessingEnabled;
		},

		selectAdditionalFilterItems: function(oFacetFilter, oStoreProduct, sGTIN) {
			// Additional filter items based on the scanned store product/GTIN
			// can be set in the facet filter control by customer specific implementation.
			// An example can be found for the merchandise category in function "selectMerchandiseCategoryFilterItem".
			
			/**
			 * @UtilityHook [Select additional filter items] 
			 *           With this hook additional filter items based on the scanned store product/GTIN
			 *           can be set in the facet filter control
			 * @callback retail.store.orderproducts1.utils.ScanUtil~extHookSetAdditionalFilterItems
			 * @param {object}
			 *          oFacetFilter the FacetFilter control
			 * @param {object}
			 *          oStoreProduct the scanned product
			 * @param {string}
			 *          sGTIN the scanned GTIN
			 * @return {void}
			 */
			if (this.extHookSetAdditionalFilterItems) {
				this.extHookSetAdditionalFilterItems(oFacetFilter, oStoreProduct, sGTIN);
			}
			return;
		}

	};

	return oScanUtil;
});