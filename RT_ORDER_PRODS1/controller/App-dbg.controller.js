/*
 * Copyright (C) 2009-2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"retail/store/orderproducts1/controller/BaseController",
	"retail/store/orderproducts1/utils/InteroperabilityHelper"
], function(JSONModel, BaseController, InteroperabilityHelper) {
	"use strict";

	return BaseController.extend("retail.store.orderproducts1.controller.App", {
	    
		onInit: function() {
			var oViewModel,
				fnSetAppNotBusy,
				iOriginalBusyDelay = this.getView().getBusyIndicatorDelay();

			oViewModel = new JSONModel({
				busy: true,
				delay: 0
			});
			this.setModel(oViewModel, "appView");

			fnSetAppNotBusy = function() {
				oViewModel.setProperty("/busy", false);
				oViewModel.setProperty("/delay", iOriginalBusyDelay);
			};

			this.getOwnerComponent().getModel().metadataLoaded().
			then(fnSetAppNotBusy);

			// apply content density mode to root view
			this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());
		},
		
		onExit: function() {
			// Cleanup some utilities
			InteroperabilityHelper.cleanup();
		}
	});

});