/*
 * Copyright (C) 2009-2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
/*global location*/
sap.ui.define([
	"sap/m/MessageBox",
	"sap/ui/core/IconPool",
	"sap/ui/core/routing/History",
	"sap/ui/model/json/JSONModel",
	"retail/store/orderproducts1/controller/BaseController",
	"sap/retail/store/lib/reuses1/util/Formatter",
	"sap/retail/store/lib/reuses1/util/TextUtil",
	"retail/store/orderproducts1/utils/CalendarUtil",
	"retail/store/orderproducts1/utils/DataChangeUtil",
	"retail/store/orderproducts1/utils/FacetFilterUtil",
	"retail/store/orderproducts1/utils/ScanUtil",
	"retail/store/orderproducts1/model/formatter"
], function(MessageBox, IconPool, History, JSONModel, BaseController, Reuses1Formatter, Reuses1TextUtil,
	CalendarUtil,
	DataChangeUtil,
	FacetFilterUtil, ScanUtil, formatter) {
	"use strict";

	return BaseController.extend("retail.store.orderproducts1.controller.ProductDetails", {

		formatter: formatter,
		_aWeekdays: ["DayOne", "DayTwo", "DayThree", "DayFour", "DayFive", "DaySix", "DaySeven"], // put it in the prototype because it's used in qunit tests

		onInit: function() {
			// Initialize the class variables for this controller
			this._oTemplate = null;
			this._oComponent = null;
			this._oCrossAppNav = null;
			this._aPricingDetailsFormContent = [];
			this._aCalendarCellFragments = [];
			this._oStoreWeekDisplayData = null;
			this._oActionSheet = null;
			this._oBookmarkButton = null;
			this._oJamShareButton = null;
			this._sProductLiveChange = "";
			this._bInputFocussed = false;
			this._bErrorWarningPopupOpen = false;
			this._bDetailViewActive = false;
			this._sCurrentlyScannedGTIN = "";
			this._oStoreWeekDisplayDeferred = null; // deferred object to check if the store week display data is available
			this._oStoreSettingsDeferred = null; // deferred object to check if the store settings data is available

			// Init handling for this view
			this.init();
		},

		init: function() {
			// Get UI5 component
			this._oComponent = this.getOwnerComponent();

			// Perform some device specific handling (orientation change, resize, auto-selection, ...)
			this.handleDeviceSpecificFeatures();

			// Initialize the data manager utility
			this.getDataManager().registerProductRefresh(jQuery.proxy(this.refreshProductData, this));

			// Set routing handler
			this.getRouter().attachRoutePatternMatched(null, this.onRoutePatternMatched, this);

			// Add custom icon for promotion indicator to the pool
			IconPool.addIcon("coins", "AppSymbols", "BusinessSuiteInAppSymbols", "e00d", true);

			// Get Cross Application Navigation service by unified shell
			this._oCrossAppNav = sap.ushell && sap.ushell.Container && sap.ushell.Container.getService("CrossApplicationNavigation");

			// Create binding for view and calendar table
			this.setBindingForView();

			// Save the initial content of the "Price Details" form
			var oForm = this.getView().byId("orderProductsFormPricingDetails");
			this._aPricingDetailsFormContent = oForm.getContent().slice(0);

			// Create the action sheet for the "action" button (down right)
			this.createActionSheet();
		},

		onExit: function() {
			// Destroy the calendar cell fragments manually
			for (var i = 0; i < this._aCalendarCellFragments.length; i++) {
				if (this._aCalendarCellFragments[i]) {
					this._aCalendarCellFragments[i].destroy();
				}
			}
		},

		onRoutePatternMatched: function(oEvent) {
			if (oEvent.getParameter("name") === "toProductDetails") {
				// Set the store product in this view
				this.handleProductDetailsRoute(decodeURIComponent(oEvent.getParameter("arguments").store), decodeURIComponent(oEvent.getParameter("arguments").product));
			}
		},

		handleProductDetailsRoute: function(sStore, sProduct) {
			// Register the barcode scan handler
			this.getBarcodeScanHandler().registerScanHandling(jQuery.proxy(this.onGTINScanComplete, this));

			// Remember that the detail view is currently active; this will be evaluated and reset in the product list later
			this._bDetailViewActive = true;

			// Read missing store configuration data if necessary
			this.readStoreConfigData(sStore);

			// Set the store product binding
			this.setStoreProductData(sStore, sProduct);

			// Remember the currently selected store product
			this.getOwnerComponent().setSelectedStoreProduct(sProduct);

			// Disable the autofocus of the NavContainer
			this.getView().getParent().setAutoFocus(false);
		},

		readStoreConfigData: function(sStoreID) {
			if (!this._oStoreWeekDisplayData || this._oStoreWeekDisplayData.StoreID !== sStoreID) {
				// Create a jQuery deferred object which will be resolved when the store week display data is available
				this._oStoreWeekDisplayDeferred = jQuery.Deferred();

				// Get the store week display configuration from data buffer
				var oStoreWeekDisplayData = this.getOwnerComponent().getStoreWeekDisplayData();
				if (oStoreWeekDisplayData && oStoreWeekDisplayData.StoreID === sStoreID) {
					// The store in the configuration from data buffer matches --> set it in this controller
					this.onAfterWeekdayConfComplete(oStoreWeekDisplayData);
				} else {
					// Read and save the weekday configuration of the given store (weekend, 1st day of week)
					this.getDataManager().readStoreWeekDisplayData(sStoreID, jQuery.proxy(function(oData) {
						// Store week display data read successful
						this.onAfterWeekdayConfComplete(oData);
					}, this), jQuery.proxy(function() {
						// Store week display data read failed -> use default data
						oStoreWeekDisplayData = CalendarUtil.oStoreWeekDisplayDataDefault;
						oStoreWeekDisplayData.StoreID = sStoreID;
						this.onAfterWeekdayConfComplete(oStoreWeekDisplayData);

						// Write to log that we are using default data
						jQuery.sap.log.error("Reading of Store Week Display Data failed. Defaults are used.");
					}, this));
				}
			}

			var oStoreSettings = this.getView().getModel("Settings").getData();
			if (!oStoreSettings || oStoreSettings.StoreID !== sStoreID) {
				// Create a jQuery deferred object which will be resolved when the store settings data is available
				this._oStoreSettingsDeferred = jQuery.Deferred();

				// Get the store week display configuration from data buffer
				oStoreSettings = this.getOwnerComponent().getStoreSettings();
				if (oStoreSettings && oStoreSettings.StoreID === sStoreID) {
					// The store in the configuration from data buffer matches --> set it in this controller
					this.onAfterStoreSettingsComplete(sStoreID, oStoreSettings);
				} else {
					// Read and save the settings of the given store
					this.getDataManager().readStoreSettings(sStoreID, jQuery.proxy(function(oData) {
						// Store settings read successful
						this.onAfterStoreSettingsComplete(sStoreID, oData);
					}, this), jQuery.proxy(function() {
						// Store settings read failed
						this.onAfterStoreSettingsComplete(sStoreID, {});
					}, this));
				}
			}
		},

		onAfterWeekdayConfComplete: function(oStoreWeekDisplayData) {
			// Save the store week display configuration
			this._oStoreWeekDisplayData = oStoreWeekDisplayData;

			// Build the calendar table using the calendar cell fragment
			this.buildCalendarTableTemplate();

			// Adjust the column headers in the calendar table
			this.adjustColumnHeaders();

			// Add custom data to table cells for coloring and formatting
			this.addCustomData();

			// The store week display data is available now
			this._oStoreWeekDisplayDeferred.resolve();
		},

		onAfterStoreSettingsComplete: function(sStoreID, oStoreSettings) {
			// Save the store settings
			var oSettings = {
				StoreID: sStoreID,
				ExceptionsEnabled: !!oStoreSettings.ExceptionsEnabled,
				RangeOfCoverageEnabled: !!oStoreSettings.RangeOfCoverageEnabled
			};

			this.getView().getModel("Settings").setData(oSettings);

			// The store settings are available now
			this._oStoreSettingsDeferred.resolve();
		},

		setStoreProductData: function(sStoreID, sProductID, bRecursive) {
			var fnSetStoreProductData = jQuery.proxy(function(oProduct) {
				// Set the product data only after the store week display data is available
				this._oStoreWeekDisplayDeferred.done(jQuery.proxy(function() {
					// Adjust the product data before it is set to the view model (if necessary)
					this.adjustProductDataForDetailView(oProduct);

					// Set the product data to the model of the view
					var oModelJSON = this.getView().getModel("storeProduct");
					oModelJSON.setData(oProduct);

					// Refresh the binding for the input field to make sure that no value from another product is displayed
					var oInput = this.byId("orderProductsInputOrderQuantity");
					oInput.getBinding("value").refresh(true);

					// Set OData model binding for generic UI adaptation (new fields); it won't trigger another request
					this.getView().bindElement({
						path: "/StoreProducts(StoreID='" + encodeURIComponent(sStoreID) + "',ProductID='" + encodeURIComponent(sProductID) + "')"
					});

					// Adjust some data on the UI for the current product
					this.adjustUIDataForProduct(oProduct);

					// Enable/disable the up/down buttons
					this.enableDisableUpDown();

					// Check if the product details for the previous or next product should already be read in background
					this.readAdjacentProductDetailsIfNecessary();
				}, this));
			}, this);

			var fnError = jQuery.proxy(function(oError) {
				// Error callback: show an error popup with the detailed error message
				var sErrorDetail = Reuses1TextUtil.getMessageForErrorResponses([oError]);
				MessageBox.error(sErrorDetail);
			}, this);

			// Get product from buffer
			var oDataManager = this.getDataManager();
			var oProduct = oDataManager.getSingleProduct(null, sProductID);

			if (oProduct && oProduct.RetailPromotions && oProduct.RetailPromotions.results !== undefined && oProduct.RetailPromotions.results !==
				null && oProduct.Weeks &&
				oProduct.Weeks.results !== undefined && oProduct.Weeks.results !== null) {
				// All product data is already available
				fnSetStoreProductData(oProduct);
			} else if (oProduct) {
				// The product was already read, but not the promotions and weeks and exceptions: check if there is a pending background request
				var oPromise = oDataManager.getProductDetailBackgroundPromise(sProductID);
				if (!bRecursive && oPromise) {
					// There is a pending background request for reading the details of the given product -> wait until this request is finished and then try again to set the product (do it only once).
					// We also have to make sure that the background request is 'converted' to a foreground request (busy dialog, ...) by calling onRequestSent/onRequestCompleted
					oDataManager.onRequestSent();
					oPromise.done(jQuery.proxy(function() {
						oDataManager.onRequestCompleted();
						this.setStoreProductData(sStoreID, sProductID, true);
					}, this));
				} else {
					oDataManager.readProductDetails(sStoreID, sProductID, false, function() {
						// Get the product again (which now contains promotions and weeks and exceptions) and set the data to this view
						oProduct = oDataManager.getSingleProduct(null, sProductID);
						fnSetStoreProductData(oProduct);
					}, function() {
						// At least set the data to the UI which currently available
						fnSetStoreProductData(oProduct);
					});
				}
				if (!bRecursive) {
					this.readStockProjection(sStoreID, sProductID);
				}
			} else {
				// Read the whole product with expand on promotions and weeks and exceptions
				var oProductReadDeferred = jQuery.Deferred();
				oDataManager.readSingleProduct(sStoreID, sProductID, function(oData) {
					if (oData && oData.ProductID) {
						// The product was not in the buffer before -> set it now to enable later update
						oDataManager.resetProducts();
						oDataManager.appendNewProducts([jQuery.extend({}, oData)]);

						// Set the product data to the detail view
						fnSetStoreProductData(oData);

						// Product data for detail screen is available now
						oProductReadDeferred.resolve();
					}
				}, fnError);
				this.readStockProjection(sStoreID, sProductID, jQuery.proxy(function(oStockProjection) {
					if (oProductReadDeferred.state() !== "resolved") {
						// The request for stock projection was finished before the main request for the product data
						oProductReadDeferred.done(jQuery.proxy(function() {
							// As soon as the product data request is finished, merge the stock projection data into the buffer and set it to this view
							oDataManager.mergeProductStockProjection(sProductID, oStockProjection);
							var oModelData = this.getView().getModel("storeProduct").getData();
							oModelData.StockProjection = oStockProjection;
							this.setRangeOfCoverageToModel(oModelData);
						}, this));
					}
				}, this));
			}
		},

		adjustProductDataForDetailView: function(oProduct) {
			var bOrderDateOneFound = false;
			var iLengthWeeks = (oProduct.Weeks && oProduct.Weeks.results && oProduct.Weeks.results.length) || 0;
			var iLengthWeekdays = this._aWeekdays.length;
			for (var i = 0; i < iLengthWeeks; i++) {
				for (var j = 0; j < iLengthWeekdays; j++) {
					if (oProduct.Weeks.results[i][this._aWeekdays[j]].IsOrderDateOne) {
						bOrderDateOneFound = true;
						break;
					}
				}
			}

			if (!bOrderDateOneFound) {
				// Compatibility mode: set "today" to first order day and "IsDeliveryDateForOrderToday" to first delivery day
				for (i = 0; i < iLengthWeeks; i++) {
					for (j = 0; j < iLengthWeekdays; j++) {
						if (formatter.isPastTodayFuture(oProduct.Weeks.results[i][this._aWeekdays[j]].Date) === "T") {
							oProduct.Weeks.results[i][this._aWeekdays[j]].IsOrderDateOne = true;
						}
						if (oProduct.Weeks.results[i][this._aWeekdays[j]].IsDeliveryDateForOrderToday) {
							oProduct.Weeks.results[i][this._aWeekdays[j]].IsDeliveryDateOne = true;
						}
					}
				}
			}
		},

		adjustUIDataForProduct: function(oProduct) {
			// Build the promotions in the "Price Details" form dynamically
			this.buildPriceDetailsContent(oProduct);

			// Update the calendar title according to the displayed months
			this.updateCalendarHeader(oProduct);

			// The product may already (or still) contain the range of coverage information -> set it to the view
			this.setRangeOfCoverageToModel(oProduct);

			// Update the share buttons
			this.updateActionSheetButtons(oProduct);

			if (this._bInputFocussed) {
				// The input field is still in focus (maybe due to paging via swipe) -> select the text again to be consistent
				this.selectTextInInput(this.byId("orderProductsInputOrderQuantity"));
			}

			/**
			 * @ControllerHook [Adjust data for a given product] // EXC_LINTER_014 The hook is called when the app
			 *                 displays a new product in the details view. 
			 * @callback retail.store.orderproducts1.controller.ProductDetails~extHookAdjustDataForProduct
			 * @param {object}
			 *          oProduct The new product
			 * @return {void}
			 */
			if (this.extHookAdjustDataForProduct) {
				this.extHookAdjustDataForProduct(oProduct);
			}
		},

		adjustUIDataForProductAfterRefresh: function(oProduct) {
			// The product may already (or still) contain the range of coverage information -> set it to the view
			this.setRangeOfCoverageToModel(oProduct);
		},

		readStockProjection: function(sStoreID, sProductID, fnSuccess) {
			// Make sure that the store settings are already available
			this._oStoreSettingsDeferred.done(jQuery.proxy(function() {
				// Check if the range of coverage is enabled (or available) at all
				if (this.getView().getModel("Settings").getProperty("/RangeOfCoverageEnabled")) {
					this.getDataManager().readStockProjection(sStoreID, sProductID, jQuery.proxy(function(oStockProjection) {
						var oData = this.getView().getModel("storeProduct").getData();
						if (oData.StoreID === sStoreID && oData.ProductID === sProductID) {
							// The same product is still selected -> set the range of coverage to the view
							oData.StockProjection = oStockProjection;
							this.setRangeOfCoverageToModel(oData);
						}
						if (typeof fnSuccess === "function") {
							fnSuccess(oStockProjection);
						}
					}, this));
				}
			}, this));
		},

		setRangeOfCoverageToModel: function(oProduct) {
			// Check if the range of coverage feature is enabled at all
			if (!this.getView().getModel("Settings").getProperty("/RangeOfCoverageEnabled")) {
				return;
			}

			var oData = {
				RangeOfCoverageInDays: null,
				EndOfCoverageDateTime: null
			};
			if (oProduct.StockProjection && oProduct.StockProjection.results && oProduct.Weeks && oProduct.Weeks.results) {
				var oRcov = this.calculateRcovFromStockProjection(oProduct);
				oData.RangeOfCoverageInDays = oRcov.RangeOfCoverageInDays;
				oData.EndOfCoverageDateTime = oRcov.EndOfCoverageDateTime;
			}
			this.getView().getModel("rcov").setData(oData);
		},

		calculateRcovFromStockProjection: function(oProduct) {
			var oRcov = {};
			var fStock, iCounter = 0;
			var aStockProjection = oProduct.StockProjection && oProduct.StockProjection.results || [];

			// Get next availability date from calendar data (weeks data)
			var dAvailabilityDate = this.getAvailabilityDate(oProduct);

			// Convert order and original order quantity to base unit of measure (stock projection is always in base UoM)
			var fOrderQuantityBase = Number(oProduct.OrderQuantity * oProduct.OrderToBaseQuantityNmrtr / oProduct.OrderToBaseQuantityDnmntr);
			var fOriginalOrderQuantityBase = Number(oProduct.OriginalOrderQuantity * oProduct.OrderToBaseQuantityNmrtr / oProduct.OrderToBaseQuantityDnmntr);

			var iLength = aStockProjection.length;
			for (var i = 0; i < iLength; i++) {
				// Get the current date within the iteration and compare it with the availability date
				var dCurrentDate = formatter.getJSDate(aStockProjection[i].Date);
				var sCompare = formatter.dateIsEarlierEqualLater(dCurrentDate, dAvailabilityDate, true, true);

				// Stock projection is only relevant for the time after the availability date
				if (sCompare === "Equal" || sCompare === "Later") {
					// Get the forecasted stock for the current date based on the entered order quantity
					fStock = Number(aStockProjection[i].ForecastedStock) - fOriginalOrderQuantityBase + fOrderQuantityBase;

					if (fStock < 0 && iCounter > 0) {
						// End of coverage is reached and there is at least one day (after the availability date) with a positive forecasted stock
						oRcov.EndOfCoverageDateTime = new Date(dCurrentDate.getTime() - 24 * 60 * 60 * 1000); // Take the day before as end of coverage
						oRcov.RangeOfCoverageInDays = this.calculateRcovInDays(oRcov.EndOfCoverageDateTime, dAvailabilityDate);
						break;
					} else if (fStock < 0) {
						// Even for the availability date the projected stock is negative
						oRcov.EndOfCoverageDateTime = null;
						oRcov.RangeOfCoverageInDays = 0;
						break;
					} else {
						// The stock is positive, so range of coverage is at least one day
						iCounter++;
					}
				}
			}
			if (fStock >= 0 && iCounter > 0) {
				// Even at the end of the given stock projection the estimated stock is positive
				oRcov.EndOfCoverageDateTime = formatter.getJSDate(aStockProjection[iLength - 1].Date);
				oRcov.RangeOfCoverageInDays = this.calculateRcovInDays(oRcov.EndOfCoverageDateTime, dAvailabilityDate);
			}
			return oRcov;
		},

		getAvailabilityDate: function(oProduct) {
			// Fallback: set delivery date as availability date
			var dDate = formatter.getJSDate(oProduct.PlannedOrderDeliveryDateTime);

			// Search for the calendar cell where the flag IsAvailabilityDateOne is set
			var iLengthWeeks = oProduct.Weeks && oProduct.Weeks.results && oProduct.Weeks.results.length || 0;
			var iLengthWeekdays = this._aWeekdays.length;
			for (var i = 0; i < iLengthWeeks; i++) {
				for (var j = 0; j < iLengthWeekdays; j++) {
					if (oProduct.Weeks.results[i][this._aWeekdays[j]].IsAvailabilityDateOne) {
						dDate = oProduct.Weeks.results[i][this._aWeekdays[j]].Date;
						break;
					}
				}
			}
			return dDate;
		},

		calculateRcovInDays: function(dEocDate, dAvailabilityDate) {
			var iMillisecAvailability = Date.UTC(dAvailabilityDate.getUTCFullYear(), dAvailabilityDate.getUTCMonth(), dAvailabilityDate.getUTCDate());
			var iMillisecEoc = Date.UTC(dEocDate.getUTCFullYear(), dEocDate.getUTCMonth(), dEocDate.getUTCDate());
			var iRcovInDays = Math.ceil(Math.abs(iMillisecEoc - iMillisecAvailability) / (1000 * 60 * 60 * 24)) + 1;
			return iRcovInDays;
		},

		setBindingForView: function() {
			// Get calendar table and save the item template
			var oTable = this.byId("orderProductsTableCalendar");
			this._oTemplate = oTable.getItems()[0].clone();

			// // Set the binding context of the view in order to stay compatible with previous version (direct oData binding)
			// oView.setModel(this.getOwnerComponent().getModel());

			// Create new JSON model for displaying data in detail view
			var oView = this.getView();
			var oModel = new JSONModel();
			oModel.setDefaultBindingMode(sap.ui.model.BindingMode.OneWay);
			oView.setModel(oModel, "storeProduct");

			// Bind item aggregation of calendar table
			oTable.bindAggregation("items", {
				path: "storeProduct>/Weeks/results",
				template: this._oTemplate
			});

			// Create JSON Model for store settings
			oModel = new JSONModel({
				ExceptionsEnabled: false,
				RangeOfCoverageEnabled: false
			});
			oModel.setDefaultBindingMode(sap.ui.model.BindingMode.OneWay);
			this.getView().setModel(oModel, "Settings");

			// Create JSON model for range of coverage related data
			oModel = new JSONModel();
			oModel.setDefaultBindingMode(sap.ui.model.BindingMode.OneWay);
			oView.setModel(oModel, "rcov");

			// Get exception list and save the item template
			var oList = this.byId("orderProductsListExceptions");
			var oExceptionTemplate = oList.getItems()[0].clone();
			oList.bindAggregation("items", {
				path: "storeProduct>/Exceptions/results",
				template: oExceptionTemplate
			});
		},

		onUpPress: function() {
			// Check if the order quantity is changed in the same user action and cancel the processing if necessary
			if (this.checkOrderQuantityChange()) {
				return;
			}

			var sPrevProductID = this.getPrevProductID();
			if (sPrevProductID) {
				this.setNewSelectedProduct(sPrevProductID);

				// Check if the next/prev package is already available and if not: read it in background
				this.readAdjacentPackagesIfNecessary(sPrevProductID);
			}

		},

		onDownPress: function() {
			// Check if the order quantity is changed in the same user action and cancel the processing if necessary
			if (this.checkOrderQuantityChange()) {
				return;
			}

			var sNextProductID = this.getNextProductID();
			if (sNextProductID) {
				this.setNewSelectedProduct(sNextProductID);

				// Check if the next/prev package is already available and if not: read it in background
				this.readAdjacentPackagesIfNecessary(sNextProductID);
			}

		},

		readAdjacentProductDetailsIfNecessary: function() {
			// Get previous and next product
			var oProduct = this.getView().getModel("storeProduct").getData();
			var oDataManager = this.getDataManager();
			var iIndex = oDataManager.getIndexOfProduct(oProduct.ProductID);
			var oPrevProduct = oDataManager.getSingleProduct(iIndex - 1);
			var oNextProduct = oDataManager.getSingleProduct(iIndex + 1);

			// If product detail data is missing, read it in background (but only if there is not already a background request)
			if (oPrevProduct && !(oPrevProduct.RetailPromotions && oPrevProduct.RetailPromotions.results && oPrevProduct.Weeks &&
					oPrevProduct.Weeks.results) && !oDataManager.getProductDetailBackgroundPromise(oPrevProduct.ProductID)) {
				oDataManager.readProductDetails(oPrevProduct.StoreID, oPrevProduct.ProductID, true, null, null);
				this.readStockProjection(oPrevProduct.StoreID, oPrevProduct.ProductID);
			}
			if (oNextProduct && !(oNextProduct.RetailPromotions && oNextProduct.RetailPromotions.results && oNextProduct.Weeks &&
					oNextProduct.Weeks.results) && !oDataManager.getProductDetailBackgroundPromise(oNextProduct.ProductID)) {
				oDataManager.readProductDetails(oNextProduct.StoreID, oNextProduct.ProductID, true, null, null);
				this.readStockProjection(oNextProduct.StoreID, oNextProduct.ProductID);
			}
		},

		readAdjacentPackagesIfNecessary: function(sProductID) {
			var oController = this.getProductListController();
			if (!oController) {
				// Nothing to do
				return;
			}

			var iIndex = this.getDataManager().getIndexOfProduct(sProductID);
			var iCheckIndex = iIndex + oController.getGrowingThreshold();
			if (!this.getDataManager().getSingleProduct(iCheckIndex)) {
				// Read next package in background
				this.getDataManager().readProductsNextPackage(true, jQuery.proxy(this.enableDisableUpDown, this));
			}

			iCheckIndex = iIndex - oController.getGrowingThreshold();
			if (iCheckIndex > -1 && !this.getDataManager().getSingleProduct(iCheckIndex)) {
				// Read previosu package in background
				this.getDataManager().readProductsPrevPackage(true, jQuery.proxy(this.enableDisableUpDown, this));
			}
		},

		checkOrderQuantityChange: function() {
			var bCancelProcessing = false;

			// Check if the order quantity is changed in the same user action
			var oInput = this.byId("orderProductsInputOrderQuantity");
			var fQuantity = this.getView().getModel("storeProduct").getProperty("/OrderQuantity");
			if (this._sProductLiveChange && Number(oInput.getValue()) !== Reuses1Formatter.roundQuantityMaxTwoDigits(
					fQuantity)) {
				// Simulate the "order quantity changed" event
				this._sProductLiveChange = "";

				var oProduct = this.getView().getModel("storeProduct").getData();
				fQuantity = oInput.getValue();
				if (DataChangeUtil.changeOrderQuantity(oProduct, fQuantity, oInput, null, jQuery.proxy(function() {
						// Callback function before update: set the changed order quantity on the UI
						this.setChangedOrderQuantity(fQuantity, oProduct.ProductID);
					}, this), null, null, null)) {
					// An error/warning popup is displayed -> Cancel current processing
					bCancelProcessing = true;
				}
			}

			if (this._bErrorWarningPopupOpen) {
				// In case a warning/error popup is displayed (maybe order quantity changed in the same step) --> Cancel current
				// processing
				bCancelProcessing = true;
			}

			return bCancelProcessing;
		},

		getPrevProductID: function() {
			var iPrevIndex = 0;
			var sPrevProductID = "";
			var oPrevProduct = null;
			var bPrevIndexFound = false;

			var oProduct = this.getView().getModel("storeProduct").getData();
			if (oProduct) {
				var iIndex = this.getDataManager().getIndexOfProduct(oProduct.ProductID);
				if (iIndex > 0) {
					// Get the previous index which points to a valid product (not hidden in the product list by search)
					iPrevIndex = iIndex;
					while (!bPrevIndexFound && iPrevIndex > 0) {
						iPrevIndex--;
						oPrevProduct = this.getDataManager().getSingleProduct(iPrevIndex);
						if (oPrevProduct) {
							// Check if this product is valid in the list
							bPrevIndexFound = this.isValidProductInList(oPrevProduct);
						} else {
							// End the loop
							break;
						}
					}

					if (bPrevIndexFound && oPrevProduct) {
						sPrevProductID = oPrevProduct.ProductID;
					}
				}
			}

			return sPrevProductID;
		},

		getNextProductID: function() {
			var iNextIndex = 0;
			var sNextProductID = "";
			var oNextProduct = null;
			var bNextIndexFound = false;

			var oProduct = this.getView().getModel("storeProduct").getData();
			if (oProduct) {
				var iIndex = this.getDataManager().getIndexOfProduct(oProduct.ProductID);
				if (iIndex > -1) {
					// Get the next index which points to a valid product (not hidden in the product list by search)
					iNextIndex = iIndex;
					while (!bNextIndexFound) {
						iNextIndex++;
						oNextProduct = this.getDataManager().getSingleProduct(iNextIndex);
						if (oNextProduct) {
							// Check if this product is valid in the list
							bNextIndexFound = this.isValidProductInList(oNextProduct);
						} else {
							// End the loop
							break;
						}
					}

					if (bNextIndexFound && oNextProduct) {
						sNextProductID = oNextProduct.ProductID;
					}
				}
			}

			return sNextProductID;
		},

		getProductListController: function() {
			var oController = null;

			var aPages = this.getView().getParent().getPages();
			var oListView = jQuery.grep(aPages, function(oItem) {
				if (oItem.getViewName && oItem.getViewName() === "retail.store.orderproducts1.view.ProductList") {
					return true;
				} else {
					return false;
				}
			})[0];

			if (oListView) {
				oController = oListView.getController();
			}

			return oController;
		},

		isValidProductInList: function(oProduct) {
			var bIsValid = true;

			var oController = this.getProductListController();
			if (oController) {
				var oItem = oController.getItemByProductID(oProduct.ProductID);
				if (oItem) {
					// Check if the item is visible
					if (!oItem.getVisible()) {
						bIsValid = false;
					}

					/**
					 * @ControllerHook [Check if a product list item is valid] // EXC_LINTER_014 The hook is called when the up/down
					 *                 buttons are pressed or enabled/disabled. It can be used to indicate that specific list items
					 *                 do not contain valid store products (e.g. for artificial grouping lines).
					 * @callback retail.store.orderproducts1.controller.ProductDetails~extHookIsValidStoreProductListItem
					 * @param {object}
					 *          oItem The list item of the product table
					 * @return {boolean} The indicator if the item is a valid store product
					 */
					if (bIsValid && this.extHookIsValidStoreProductListItem) {
						bIsValid = this.extHookIsValidStoreProductListItem(oItem);
					}
				}
			}

			return bIsValid;
		},

		setNewSelectedProduct: function(sProductID) {
			var oJSONModel = this.getView().getModel("storeProduct");
			var sStoreID = oJSONModel.getProperty("/StoreID");
			var sCurrentProductID = oJSONModel.getProperty("/ProductID");

			// Set the previously selected item to processed and submit it to the backend if necessary
			this.setProductProcessed(sCurrentProductID, true);

			// Set this product in the detail screen
			this.getRouter().navTo("toProductDetails", {
				store: encodeURIComponent(sStoreID),
				product: encodeURIComponent(sProductID)
			}, true);
		},

		setProductProcessed: function(sProductID, bAsync) {
			var sStoreID = this.getView().getModel("storeProduct").getProperty("/StoreID");

			var fnSetProductProcessed = jQuery.proxy(function() {
				// Set product to processed in data buffer and submit it to the backend
				this.getDataManager().setProductProcessed(sProductID);
				DataChangeUtil.submitProductChanges(sStoreID, sProductID);

				var oController = this.getProductListController();
				if (oController) {
					// Set the old store product processed in the table (do it manually for performance reasons)
					var oItem = oController.getItemByProductID(sProductID);
					if (oItem) {
						oController.setItemProcessed(oItem);
					}
				}
			}, this);

			if (bAsync) {
				jQuery.sap.delayedCall(0, null, fnSetProductProcessed);
			} else {
				fnSetProductProcessed();
			}
		},

		enableDisableUpDown: function() {
			if (this.getPrevProductID()) {
				this.adjustUpDownEnablement("Up", true);
			} else {
				this.adjustUpDownEnablement("Up", false);
			}

			if (this.getNextProductID()) {
				this.adjustUpDownEnablement("Down", true);
			} else {
				this.adjustUpDownEnablement("Down", false);
			}
		},

		adjustUpDownEnablement: function(sBtn, bEnabled) {

			if (sBtn === "Up") {
				// Enable or disable the Up button
				var oButtonUp = this.getView().byId("orderProductsButtonUp");
				oButtonUp.setEnabled(bEnabled);
			} else if (sBtn === "Down") {
				// Enable or disable the Down button
				var oButtonDown = this.getView().byId("orderProductsButtonDown");
				oButtonDown.setEnabled(bEnabled);
			}
		},

		refreshProductData: function(aProductIDs) {
			var oModelJSON = this.getView().getModel("storeProduct");
			var oProduct = oModelJSON.getData();
			if (oProduct && aProductIDs.indexOf(oProduct.ProductID) > -1) {
				var oProductNew = this.getDataManager().getSingleProduct(null, oProduct.ProductID);
				var oData = this.getRefreshedProduct(oProduct, oProductNew);
				oModelJSON.setData(oData);

				// Refresh dependent data for this product (e.g. range of coverage)
				this.adjustUIDataForProductAfterRefresh(oData);
			}
		},

		getRefreshedProduct: function(oProductOld, oProductNew) {
			var oProduct = oProductNew;

			// Adjust the product data before it is set to the view model (if necessary)
			this.adjustProductDataForDetailView(oProduct);

			return oProduct;
		},

		onOrderQuantityLiveChange: function(oEvent) {
			var oInput = oEvent.getSource();
			var oModel = this.getView().getModel("storeProduct");
			var fQuantityModel = oModel.getProperty("/OrderQuantity");
			if (Number(oInput.getValue()) !== Reuses1Formatter.roundQuantityMaxTwoDigits(fQuantityModel)) {
				this._sProductLiveChange = oModel.getProperty("/ProductID");
			} else {
				this._sProductLiveChange = "";
			}
		},

		onOrderQuantityChange: function(oEvent) {
			var oInput = oEvent.getSource();

			// Check if there is still a pending live change; it might be, that a pending change was already submitted by checkOrderQuantityChange
			if (this._sProductLiveChange) {
				this._sProductLiveChange = "";
			} else {
				oInput.getBinding("value").refresh(true);
				return;
			}

			var oProduct = this.getView().getModel("storeProduct").getData();
			var fQuantity = oEvent.getParameter("newValue");
			if (DataChangeUtil.changeOrderQuantity(oProduct, fQuantity, oInput, null, jQuery.proxy(function() {
					// Callback function before update: set the changed order quantity on the UI
					this.setChangedOrderQuantity(fQuantity, oProduct.ProductID);
				}, this), jQuery.proxy(function() {
					// Callback function for popup close
					this._bErrorWarningPopupOpen = false;
				}, this), null, null)) {
				// A warning/error popup is opened --> up/down and back function should not be executed now
				this._bErrorWarningPopupOpen = true;
			}
		},

		onAddButtonPress: function() {
			var oJSONModel = this.getView().getModel("storeProduct");
			var oProduct = oJSONModel.getData();
			var fChangedOrderQuantity = DataChangeUtil.increaseOrderQuantity(oProduct);
			if (fChangedOrderQuantity !== null) {
				this.setChangedOrderQuantity(fChangedOrderQuantity, oProduct.ProductID);
			}
		},

		onLessButtonPress: function() {
			var oJSONModel = this.getView().getModel("storeProduct");
			var oProduct = oJSONModel.getData();
			var fChangedOrderQuantity = DataChangeUtil.decreaseOrderQuantity(oProduct);
			if (fChangedOrderQuantity !== null) {
				this.setChangedOrderQuantity(fChangedOrderQuantity, oProduct.ProductID);
			}
		},

		setChangedOrderQuantity: function(fChangedOrderQuantity, sProductID) {
			var oJSONModel = this.getView().getModel("storeProduct");
			oJSONModel.setProperty("/OrderQuantity", fChangedOrderQuantity);
			if (oJSONModel.getProperty("/IsChangedOrderQuantityPosted") !== null) {
				oJSONModel.setProperty("/IsChangedOrderQuantityPosted", null);
			}

			// Directly update the (maybe changed) range of coverage for the current product
			var oProduct = oJSONModel.getData();
			this.setRangeOfCoverageToModel(oProduct);

			var oController = this.getProductListController();
			if (oController) {
				// Update new order quantity in the table (do it manually and asynchronously for performance reasons)
				jQuery.sap.delayedCall(0, null, function() {
					var oItem = oController.getItemByProductID(sProductID);
					if (oItem) {
						oController.setChangedOrderQuantity(oItem, fChangedOrderQuantity);
					}
				});
			}
		},

		onScanButtonPress: function(oEvent) {
			/**
			 * @ControllerHook [React on scan button] // EXC_LINTER_014 The hook is called when the scan button is pressed in
			 *                 the app. A laser scanner can be triggered here. When the GTIN is determined, the function
			 *                 onGTINScanComplete of this controller should be called with the scanned GTIN.
			 * @callback retail.store.orderproducts1.controller.ProductDetails~extHookOnScanButtonPress
			 * @param {object}
			 *          oEvent The event object of the button press event
			 * @return {void}
			 */
			if (this.extHookOnScanButtonPress) {
				this.extHookOnScanButtonPress(oEvent);
			} else {
				// Call the standard barcode scanner which either uses the camera scanner or shows a popup
				this.standardScanButtonHandling();
			}
		},

		standardScanButtonHandling: function() {
			sap.ndc.BarcodeScanner.scan(jQuery.proxy(function(oResult) {
				// Barcode scan successful
				if (oResult.text && !oResult.cancelled) {
					this.getBarcodeScanHandler().handleBarcodeScan(oResult.text);
				}
			}, this), null, jQuery.proxy(function(oParams) {
				// Live change event of input field
				ScanUtil.handleInputLiveChange(oParams.newValue, jQuery.proxy(function(sGTIN) {
					// Continue if automatic GTIN processing is enabled (smartphone/tablet)
					this.getBarcodeScanHandler().handleBarcodeScan(sGTIN);
				}, this));
			}, this));
		},

		onGTINScanComplete: function(sGTIN) {
			// Check if the order quantity is changed in the same user action and cancel the processing if necessary
			if (this.checkOrderQuantityChange()) {
				return;
			}

			// Check if the given barcode is numerical
			if (!ScanUtil.isBarcodeNumeric(sGTIN)) {
				MessageBox.error(this.getResourceBundle.getText("ERROR_MSG_NOT_NUMERIC"));
				return;
			}

			// Get the current store id
			var oProduct = this.getView().getModel("storeProduct").getData();
			if (!oProduct || !sGTIN) {
				return;
			}
			var sStoreID = oProduct.StoreID;

			var oScannedProduct = this.getDataManager().findProductByBarcode(sGTIN);
			if (oScannedProduct) {
				// Product is already in buffer: just set it as new product in the details view
				this.setNewSelectedProduct(oScannedProduct.ProductID);

				// Check if the next/prev package is already available and if not: read it in background
				this.readAdjacentPackagesIfNecessary(oScannedProduct.ProductID);
			} else {
				// Product is not yet in buffer: read it synchronously via GTIN
				this.getDataManager().readSingleProductByBarcode(sStoreID, sGTIN, jQuery.proxy(function(oData) {
					// Success callback
					oScannedProduct = oData;

					// Set currently selected product to processed and submit
					var sCurrentProductID = this.getView().getModel("storeProduct").getProperty("/ProductID");
					this.setProductProcessed(sCurrentProductID, false);

					// Now reset the products in the data manager and add just the scanned product
					this.getDataManager().resetProducts();
					this.getDataManager().appendNewProducts([jQuery.extend({}, oScannedProduct)]);

					// Display the scanned product in the detail screen
					this.getRouter().navTo("toProductDetails", {
						store: encodeURIComponent(sStoreID),
						product: encodeURIComponent(oScannedProduct.ProductID)
					}, true);

					// Read the stock projection for the scanned product
					this.readStockProjection(sStoreID, oScannedProduct.ProductID);

					// Read the list for the scanned product in background
					this.readListForScannedProduct(oScannedProduct, sGTIN);
				}, this), function() {
					// No valid GTIN was scanned --> display an error popup
					MessageBox.error(this.getResourceBundle.getText("ERROR_MSG_SCAN_FAILED"));
				});
			}
		},

		readListForScannedProduct: function(oScannedProduct, sGTIN) {
			// Get the product list controller
			var oController = this.getProductListController();
			if (!oController) {
				return;
			}

			var oProductTable = oController.getProductTable();
			var oFacetFilter = oController.getFacetFilter();
			var oSearchField = oController.getSearchField();

			// Reset the facet filter control and the search field and remove the data from the list
			FacetFilterUtil.resetFacetFilter(oFacetFilter);
			oSearchField.setValue("");
			oProductTable.getModel().setData({
				Products: null
			});

			// Build the filter according to the scanned store product
			var oFilter = ScanUtil.buildFilterObjectWithStoreProduct(oFacetFilter, oScannedProduct, sGTIN);

			var mOptions = {};
			mOptions.ProductDataExpand = oController._mSettings.EnableProductExpand;
			mOptions.DisablePaging = FacetFilterUtil.isPagingForbiddenForFilter(oFacetFilter);
			mOptions.SuppressBufferUpdate = true;
			mOptions.InBackground = true;

			this.getDataManager().readProductsInitial(oScannedProduct.StoreID, oScannedProduct.ProductID, oFilter, "",
				mOptions, jQuery.proxy(function(oData) {
					// Get scanned product from data buffer again (might have been updated already)
					var oProduct = this.getDataManager().getSingleProduct(null, oScannedProduct.ProductID);

					// Set the new products manually in the buffer
					this.getDataManager().resetProducts();
					var aResults = jQuery.map(oData.results, function(oItem) {
						return jQuery.extend({}, oItem);
					});
					this.getDataManager().appendNewProducts(aResults);

					// Set the (maybe changed) data of the scanned product in the buffer again
					this.getDataManager().refreshSingleProduct(oProduct);

					// Enable/disable the up down buttons in detail view
					this.enableDisableUpDown();

					// Read next/prev package in background
					var fnAfterNextPrevPackage = jQuery.proxy(function() {
						this.enableDisableUpDown();
						this.readAdjacentProductDetailsIfNecessary();
					}, this);
					this.getDataManager().readProductsNextPackage(true, fnAfterNextPrevPackage);
					this.getDataManager().readProductsPrevPackage(true, fnAfterNextPrevPackage);

					// Check if the product details for the previous or next product should already be read in background
					this.readAdjacentProductDetailsIfNecessary();

					// Save the last scanned store product which is later used by the product list
					this.getOwnerComponent().setLastScannedStoreProduct(oScannedProduct);
					this._sCurrentlyScannedGTIN = sGTIN;
				}, this),
				function(oError) {
					// No error handling in this case because the products were read in background
					return;
				});
		},

		onActionButtonPress: function(oEvent) {
			if (!this._oActionSheet.isOpen()) {
				this._oActionSheet.openBy(oEvent.getSource());
			} else {
				this._oActionSheet.close();
			}
		},

		createActionSheet: function() {
			// Create an action sheet which will be opened to the top
			this._oActionSheet = new sap.m.ActionSheet({
				placement: "Top"
			});

			// Add bookmark button for creating tiles
			this._oBookmarkButton = new sap.ushell.ui.footerbar.AddBookmarkButton();
			this._oActionSheet.addButton(this._oBookmarkButton);

			// Add a Jam Share button
			this._oJamShareButton = new sap.ushell.ui.footerbar.JamShareButton();
			this._oActionSheet.addButton(this._oJamShareButton);

			// Add a navigation button to the product lookup app
			var oNavButton = new sap.m.Button({
				text: this.getResourceBundle().getText("DETAIL_BUTTON_LOOKUP_PRODUCT"),
				icon: "sap-icon://Fiori3/F0535",
				press: jQuery.proxy(this.onNavToProductLookup, this)
			});
			this._oActionSheet.addButton(oNavButton);

			// Add a navigation button to the adjust stock app
			oNavButton = new sap.m.Button({
				text: this.getResourceBundle().getText("DETAIL_BUTTON_ADJUST_STOCK"),
				icon: "sap-icon://Fiori4/F0586",
				press: jQuery.proxy(this.onNavToAdjustStock, this)
			});
			this._oActionSheet.addButton(oNavButton);

			/**
			 * @ControllerHook [Add buttons to the action sheet] // EXC_LINTER_014 The hook is called once when the app is
			 *                 started.
			 * @callback retail.store.orderproducts1.controller.ProductDetails~extHookAddActionSheetButtons
			 * @param {object}
			 *          oActionSheet The action sheet control (sap.m.ActionSheet), which can be enhanced with new buttons.
			 * @return {void}
			 */
			if (this.extHookAddActionSheetButtons) {
				this.extHookAddActionSheetButtons(this._oActionSheet);
			}
		},

		updateActionSheetButtons: function(oProduct) {

			var sProductName = oProduct.ProductName;
			var sProductId = oProduct.ProductID;

			// Update the bookmark button
			var oAppData = (this._oBookmarkButton.getAppData() || {});
			jQuery.extend(oAppData, {
				title: this.getResourceBundle().getText("FULLSCREEN_TITLE"),
				subtitle: sProductName + " (" + sProductId + ")"
			});

			this._oBookmarkButton.setAppData(oAppData);

			// Update the Jam Share Button
			var oDisplay = new sap.m.ObjectListItem({
				title: sProductName,
				attributes: [new sap.m.ObjectAttribute({
					text: sProductId
				})]
			});

			var oJamShareSettings = (this._oJamShareButton.getJamData() || {});
			jQuery.extend(oJamShareSettings, {
				object: {
					id: document.URL,
					display: oDisplay,
					share: sProductName + " (" + sProductId + ")"
				}
			});

			this._oJamShareButton.setJamData(oJamShareSettings);
		},

		onNavToProductLookup: function() {
			var oModel = this.getModel("storeProduct");
			if (oModel && oModel.getProperty("/ProductID") && this._oCrossAppNav) {
				// Navigate directly to the detail screen of the product lookup app
				this._oCrossAppNav.toExternal({
					target: {
						semanticObject: "Article",
						action: "lookup"
					},
					params: {
						SiteID: oModel.getProperty("/StoreID"),
						ProductID: oModel.getProperty("/ProductID")
					}
				}, this._oComponent);
			}
		},

		onNavToAdjustStock: function() {
			var oModel = this.getModel("storeProduct");
			if (oModel && oModel.getProperty("/ProductID") && this._oCrossAppNav) {
				// Navigate directly to the detail screen of the adjust stock app
				this._oCrossAppNav.toExternal({
					target: {
						semanticObject: "Article",
						action: "correctStock"
					},
					params: {
						ProductID: oModel.getProperty("/ProductID")
					}
				}, this._oComponent);
			}
		},

		buildPriceDetailsContent: function(oProduct) {
			var oForm = this.getView().byId("orderProductsFormPricingDetails");

			// Remove the current promotion specific content in the price details form if there is any
			var aFormContent = oForm.getContent().slice(0);
			for (var i = this._aPricingDetailsFormContent.length; i < aFormContent.length; i++) {
				aFormContent[i].destroy();
			}

			if (oProduct && oProduct.RetailPromotions && oProduct.RetailPromotions.results && oProduct.RetailPromotions.results.length > 0) {
				// Build the content for the given promotions and set it to the form
				this.buildPromotionContent(oForm, oProduct.RetailPromotions.results);
			}
		},

		buildPromotionContent: function(oForm, aPromotions) {

			var oLabel = null;
			var oText = null;
			var sPath = "";

			// Add 4 new fields for each promotion
			for (var j = 0; j < aPromotions.length; j++) {
				// Add label for promotion name and ID
				oLabel = new sap.m.Label({
					text: "{i18n>DETAIL_PROMOTION}",
					design: "Bold"
				});
				oLabel.addStyleClass("sapRTSTOrdProdLabelPromotionName");
				oForm.addContent(oLabel);

				// Add text field for promotion title
				oText = new sap.m.Text({
					text: formatter.formatPromotionNameWithID(aPromotions[j].RetailPromotionID, aPromotions[j].RetailPromotionName, "medium")
				});
				oText.addStyleClass("sapRTSTOrdProdLinkPromotionName");
				oForm.addContent(oText);

				// Add label for promotion start date
				oLabel = new sap.m.Label({
					text: "{i18n>DETAIL_PROMOTION_START}"
				});
				oForm.addContent(oLabel);

				// Add text field for promotion start date
				oText = new sap.m.Text({
					text: formatter.formatDate(aPromotions[j].StartDate, "medium")
				});
				oForm.addContent(oText);

				// Add label for promotion end date
				oLabel = new sap.m.Label({
					text: "{i18n>DETAIL_PROMOTION_END}"
				});
				oForm.addContent(oLabel);

				// Add text field for promotion end date
				oText = new sap.m.Text({
					text: formatter.formatDate(aPromotions[j].EndDate, "medium")
				});
				oForm.addContent(oText);

				// Add label for promotion price
				oLabel = new sap.m.Label({
					text: "{i18n>DETAIL_PROMOTION_PRICE}"
				});
				oForm.addContent(oLabel);

				// Add text field for promotion price
				oText = new sap.m.Text({
					text: formatter.formatPriceWithCurrencyForPromotion(aPromotions[j].SalesPrice, aPromotions[j].SalesPriceCurrency,
						aPromotions[j].SalesPriceQuantity,
						aPromotions[j].SalesQuantityUnitCode)
				});
				oForm.addContent(oText);

				sPath = "/RetailPromotions/results/" + j;
				/**
				 * @ControllerHook [Provide additional promotion fields] // EXC_LINTER_014 The hook is called every time the
				 *                 promotion fields in the details view are created (once for every promotion). The promotion data
				 *                 in oPromotion can be used to create new controls. These controls can then be added to oForm
				 *                 using oForm.addContent(). Normally a label and a text control are added to provide a new field.
				 * @callback retail.store.orderproducts1.controller.ProductDetails~extHookSetAdditionalPromotionContent
				 * @param {object}
				 *          oForm The UI5 control hosting the promotion fields (sap.ui.layout.form.SimpleForm)
				 * @param {string}
				 *          sPath The path to the current promotion entity
				 * @param {object}
				 *          oPromotion The data of the current promotion entity
				 * @return {void}
				 */
				if (this.extHookSetAdditionalPromotionContent) {
					this.extHookSetAdditionalPromotionContent(oForm, sPath, aPromotions[j]);
				}
			}
		},

		updateCalendarHeader: function(oProduct) {
			// Get the calendar weeks and get the header text according to the first and last displayed date
			var sText = CalendarUtil.getCalendarHeaderText(oProduct);

			// Set the header text for the calendar
			//this.getView().byId("orderProductsTableCalendar").setHeaderText(sText);
			this.getView().byId("orderProductsTableCalendarLabel").setText(sText);
		},

		addCustomData: function() {

			var oCellControl = null;
			var i = 0;

			for (i = 0; i < this._aWeekdays.length; i++) {
				// Get the cell control which contains the current weekday
				oCellControl = this.getCellControlByWeekday(this._aWeekdays[i]);

				// Create custom data for the given weekday, which is needed for coloring and formatting
				CalendarUtil.addCustomDataForWeekday(oCellControl, this._aWeekdays[i], this._oStoreWeekDisplayData);
			}
		},

		getCellControlByWeekday: function(sWeekday) {
			var oCellControl = null;

			/**
			 * @ControllerHook [Get the cell control for a weekday] // EXC_LINTER_014 The hook is called once for every weekday.
			 *                 It should return the control which represents a calendar cell
			 * @callback retail.store.orderproducts1.controller.ProductDetails~extHookGetCellControlByWeekday
			 * @param {string}
			 *          sWeekday The current weekday (DayOne, DayTwo,..., DaySeven)
			 * @return {string} The calendar cell control for this weekday
			 */
			if (this.extHookGetCellControlByWeekday) {
				// Get the cell control from the hook method
				oCellControl = this.extHookGetCellControlByWeekday(sWeekday);
			} else {
				// Standard logic
				oCellControl = sap.ui.core.Fragment.byId("orderProductsFragmentCalendar" + sWeekday, "orderProductsCell" + sWeekday);
			}
			return oCellControl;
		},

		adjustColumnHeaders: function() {

			var oLabel = null;
			var i = 0;

			for (i = 0; i < this._aWeekdays.length; i++) {
				// Get the VBox which contains the current weekday
				oLabel = this.byId("orderProductsColumnLabel" + this._aWeekdays[i]);

				// Adjust the text in the label of the column header
				CalendarUtil.adjustColumnHeader(oLabel, this._aWeekdays[i], this._oStoreWeekDisplayData);
			}
		},

		buildCalendarTableTemplate: function() {

			var i = 0;
			var sFragment = "";
			var oFragment = null;

			// var oColListItem = this.byId("orderProductsCalendarListItem");
			var oColListItem = this._oTemplate;

			// Get the "full" calendar cell fragement
			var sXML = this.getCalendarCellXML();

			// Get the "light" calendar cell fragement (not covering "today")
			var sXMLLight = this.getCalendarCellLightXML();

			// Get current weekday ID
			var dCurrentDate = this.getOwnerComponent().getCurrentDate();
			var iCurrentWeekDayID = dCurrentDate.getDay();
			if (iCurrentWeekDayID === 0) {
				iCurrentWeekDayID = 7;
			}

			// First destroy the calendar cell fragments if there are any
			for (i = 0; i < this._aCalendarCellFragments.length; i++) {
				if (this._aCalendarCellFragments[i]) {
					this._aCalendarCellFragments[i].destroy();
				}
			}

			// For every weekday replace the pattern "DayOne" in template fragment and add it to the cells aggregation
			this._aCalendarCellFragments = [];
			for (i = 0; i < this._aWeekdays.length; i++) {

				if (!this._oStoreWeekDisplayData || this._oStoreWeekDisplayData[this._aWeekdays[i]].WeekDayID === iCurrentWeekDayID) {
					// Column contains "today" --> take the full calendar cell fragment
					sFragment = sXML.replace(/DayOne/g, this._aWeekdays[i]);
				} else {
					// Column does not contain "today" --> take the light calendar cell fragment
					sFragment = sXMLLight.replace(/DayOne/g, this._aWeekdays[i]);
				}
				oFragment = sap.ui.xmlfragment({
					fragmentContent: sFragment,
					id: "orderProductsFragmentCalendar" + this._aWeekdays[i]
				}, this);
				oColListItem.insertCell(oFragment, (i + 1)); // start inserting from second cell (1st cell is calendar week)
				this._aCalendarCellFragments.push(oFragment);
			}
		},

		getCalendarCellXML: function() {

			var sRelativeFragmentPath = "/view/CalendarCell.fragment.xml";
			var sStandardFragmentPath = jQuery.sap.getModulePath("retail.store.orderproducts1") + sRelativeFragmentPath;

			// Load the default template for the calendar cells (with "DayOne", which will be replaced later)
			var sFragmentPath = sStandardFragmentPath;

			/**
			 * @ControllerHook [Provide the path to a modified calendar cell fragment] // EXC_LINTER_014 The hook is called once
			 *                 when the product details view is initialized. This calendar cell needs to cover all cases
			 *                 including "today"
			 * @callback retail.store.orderproducts1.controller.ProductDetails~extHookProvideCalendarCellFragmentPath
			 * @return {string} The path of the calendar cell fragment.
			 */
			if (this.extHookProvideCalendarCellFragmentPath) {
				sFragmentPath = this.extHookProvideCalendarCellFragmentPath();
			}

			// Get the XML via the fragment path (via hook or standard)
			return this.getXMLByFragmentPath(sFragmentPath, sStandardFragmentPath);
		},

		getCalendarCellLightXML: function() {

			var sRelativeFragmentPath = "/view/CalendarCell.fragment.xml";
			var sStandardFragmentPath = jQuery.sap.getModulePath("retail.store.orderproducts1") + sRelativeFragmentPath;

			// Load the default template for the calendar cells (with "DayOne", which will be replaced later)
			var sFragmentPath = sStandardFragmentPath;

			/**
			 * @ControllerHook [Provide the path to a modified calendar cell light fragment] // EXC_LINTER_014 The hook is
			 *                 called once when the product details view is initialized. The calendar cell "light" does not have
			 *                 to consider the "today" case. It is only used for the weekdays not covering "today"
			 * @callback retail.store.orderproducts1.controller.ProductDetails~extHookProvideCalendarCellLightFragmentPath
			 * @return {string} The path of the calendar cell light fragment.
			 */
			if (this.extHookProvideCalendarCellLightFragmentPath) {
				sFragmentPath = this.extHookProvideCalendarCellLightFragmentPath();
			}

			// Get the XML via the fragment path (via hook or standard)
			return this.getXMLByFragmentPath(sFragmentPath, sStandardFragmentPath);
		},

		getXMLByFragmentPath: function(sFragmentPath, sStandardFragmentPath) {
			// Get the template XML fragment
			var oData = jQuery.sap.syncGet(sFragmentPath);

			if (!oData.success && sFragmentPath !== sStandardFragmentPath) {
				// Use the standard XML fragment
				oData = jQuery.sap.syncGet(sStandardFragmentPath);
			}

			return oData.data;
		},

		onCalendarLegendPress: function() {
			var oBarcodeScanHandler = this.getBarcodeScanHandler();
			var oDialog = new sap.m.Dialog({
				title: this.getResourceBundle().getText("DETAIL_CALENDAR_LEGEND_TITLE"),
				content: sap.ui.xmlfragment("retail.store.orderproducts1.view.CalendarLegend"),
				contentWidth: "500px",
				beginButton: new sap.m.Button({
					text: this.getResourceBundle().getText("DETAIL_CALENDAR_LEGEND_BUTTON_CLOSE"),
					press: function() {
						oDialog.close();
					}
				}),
				afterClose: function() {
					oDialog.destroy();
					oBarcodeScanHandler.unlockBarcodeScanHandling();
				}
			}).addStyleClass("sapUiContentPadding");

			// To get access to the global model
			this.getView().addDependent(oDialog);
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), oDialog);

			oBarcodeScanHandler.lockBarcodeScanHandling();
			oDialog.open();
		},

		handleDeviceSpecificFeatures: function() {
			// Make sure that the content of the input field for order quantities is preselected when focused
			var oInput = this.getView().byId("orderProductsInputOrderQuantity");
			var fnFocusIn = jQuery.proxy(function() {
				// Select the text in the input field
				this.selectTextInInput(oInput);

				// Remember that the input field is currently focussed
				this._bInputFocussed = true;
			}, this);
			var fnFocusOut = jQuery.proxy(function() {
				// The input field is not focussed anymore
				this._bInputFocussed = false;
			}, this);
			oInput.addEventDelegate({
				onfocusin: fnFocusIn,
				onfocusout: fnFocusOut
			}, oInput);

			/**
			 * @ControllerHook [Replace the standard swipe behavior on the detail screen] // EXC_LINTER_014 The hook is called
			 *                 once when the product details view is initialized. If it is implemented, the standard swipe
			 *                 behavior on this view can be overwritten. By default, swiping changes the currently displayed
			 *                 product (analog to up/down button).
			 * @callback retail.store.orderproducts1.controller.ProductDetails~extHookReplaceSwipeProductDetail
			 */
			if (this.extHookReplaceSwipeProductDetail) {
				this.extHookReplaceSwipeProductDetail();
			} else {
				// Standard handling: swiping corresponds to up/down button
				if (sap.ui.Device.support.touch) {
					// Enable paging via swipe complete view
					this.byId("orderProductsDetailsPage").addEventDelegate({
						onswipeleft: function() {
							this.onDownPress();
						},
						onswiperight: function() {
							this.onUpPress();
						}
					}, this);
				}
			}

			// Attach an event handler for the "orientation change" event
			sap.ui.Device.orientation.attachHandler(this.onOrientationChange, this);
		},

		selectTextInInput: function(oInput) {
			if (!oInput) {
				return;
			}

			// Call the select method, which works for some device/browser combinations
			jQuery.sap.delayedCall(0, oInput.getFocusDomRef(), "select");

			// Additionally call the selectText method, which works for some other device/browser combinations
			if (oInput.selectText) {
				jQuery.sap.delayedCall(100, oInput, "selectText", [0, oInput.getValue().length]);
			} else {
				jQuery.sap.delayedCall(100, jQuery(oInput.getFocusDomRef()), "selectText", [0, oInput.getValue().length]);
			}
		},

		onOrientationChange: function(mParams) {
			// Refresh the value of the proposed order quantity (line break depending on device)
			this.byId("orderProductsTextOriginalQuantity").getBinding("text").refresh(true);
		}

	});

});