/*
 * Copyright (C) 2009-2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define([
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"sap/ndc/BarcodeScanner",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"retail/store/orderproducts1/controller/BaseController",
	"retail/store/orderproducts1/view/StoreSelectDialog",
	"sap/retail/store/lib/reuses1/util/Formatter",
	"sap/retail/store/lib/reuses1/util/TextUtil",
	"retail/store/orderproducts1/utils/DataChangeUtil",
	"retail/store/orderproducts1/utils/FacetFilterUtil",
	"retail/store/orderproducts1/utils/InteroperabilityHelper",
	"retail/store/orderproducts1/utils/ScanUtil",
	"retail/store/orderproducts1/model/formatter"
], function(MessageBox, MessageToast, BarcodeScanner, JSONModel,
	Filter, FilterOperator, BaseController, StoreSelectDialog, Reuses1Formatter, Reuses1TextUtil, DataChangeUtil,
	FacetFilterUtil, InteroperabilityHelper, ScanUtil, formatter) {
	"use strict";

	return BaseController.extend("retail.store.orderproducts1.controller.ProductList", {

		formatter: formatter,

		onBeforeRendering: function() {
			// Disable the autofocus of the NavContainer
			this.getView().getParent().setAutoFocus(false);
		},

		/**
		 * Called when the ProductList controller is instantiated.
		 * @public
		 */
		onInit: function() {
			// Initialize the class variables for this controller
			this._mDialogs = {};
			this._oComponent = null;
			this._oCrossAppNav = null;
			this._bSwitchToProductList = false;
			this._bStoreEntityReadComplete = false;
			this._bProcessScannedGTINAfterRead = false;
			this._sCurrentlyScannedGTIN = "";
			this._oCurrentlyScannedStoreProduct = null;
			this._oActionSheet = null;
			this._oBookmarkButton = null;
			this._oJamShareButton = null;
			this._sProductLiveChange = "";
			this._bErrorWarningPopupOpen = false;
			this._bStoreAssignmentChanged = false; // The store assignment was changed; display a message toast
			this._mInteropProperties = {}; // Properties which are relevant for interoperability (different oData service versions)
			this._mSettings = {}; // Static settings which can be set via hook
			this._fnProcessAfterListUpdate = null;
			this._iGrowingThreshold = 10; // Growing threshold for product table, can be overridden via hook
			this._oFilterValuesDeferred = null; // deferred object to check if the filter values for the current store are available
			this._oInitialProductPackageDeferred = null; // deferred object which is created/resolved when a new initial package of products is read
			this._bPageChangePending = false; // indicator if there is currently a page change via up/down button

			// Init handling for this view
			this.init();
		},

		init: function() {
			// Get UI5 component
			this._oComponent = this.getOwnerComponent();

			// Set routing handler
			this.getRouter().attachRoutePatternMatched(null, this.onRoutePatternMatched, this);

			var oDataModel = this._oComponent.getModel();

			var oSettingsModel = new sap.ui.model.json.JSONModel({
				ExceptionsEnabled: false,
				RangeOfCoverageEnabled: false
			});
			this.getView().setModel(oSettingsModel, "Settings");

			// Perform some device specific handling (orientation change, resize, auto-selection, ...)
			this.handleDeviceSpecificFeatures();

			// Initialize the interoperability helper
			InteroperabilityHelper.init(oDataModel);

			// Initialize the data manager utility
			this.getDataManager().registerProductRefresh(jQuery.proxy(this.refreshProductDataInList, this));
			this.getDataManager().setProductTable(this.getProductTable());

			// Get some interoperability properties depending on the underlying oData service version
			this.setInteroperabilityProperties();

			// Get Cross Application Navigation service by unified shell
			this._oCrossAppNav = sap.ushell && sap.ushell.Container && sap.ushell.Container.getService("CrossApplicationNavigation");

			// Create binding for view and product table
			this.setBindingForView();

			// Add settings button in unified shell for changing the store assignment
			this.createAssignStoreButton();

			// Special adjustment for facet filter (e.g. display "none" instead of "all" in case of attribute filter list)
			this.adjustFacetFilter();

			// Create the action sheet for the "action" button (down right)
			this.createActionSheet();

			// Create static custom settings which can be set via hooks
			this.createCustomSettings();
		},

		onRoutePatternMatched: function(oEvent) {
			var sName = oEvent.getParameter("name");
			if (sName === "app_start" || sName === "toProductList") {
				this.handleProductListRoute(oEvent);
			}
		},

		handleAppStart: function() {
			// Directly access the assigned store via initial key read
			this.setCurrentStore("");
			this._bSwitchToProductList = true;
		},

		handleProductListRoute: function(oEvent) {
			// Register the barcode scan handler
			this.getBarcodeScanHandler().registerScanHandling(jQuery.proxy(this.onGTINScanComplete, this));

			if (this._bStoreAssignmentChanged) {
				// Store assignment was changed via dialog -> display success message
				MessageToast.show(Reuses1TextUtil.getReuseText("SUCCESS_MSG_USER_ASSIGNMENT"));
				this._bStoreAssignmentChanged = false;
			}

			var sName = oEvent.getParameter("name");
			switch (sName) {
				case "app_start":
					// App start: either display the products of the assigned store or navigate to store selection list
					this.handleAppStart();
					break;
				case "toProductList":
					// Set the store to this view
					this.setCurrentStore(decodeURIComponent(oEvent.getParameter("arguments").store));

					// No scan mode
					this.resetScanMode();

					// Do the handling for return from details to list
					this.handleReturnFromProductDetails();

					break;
			}
		},

		handleScannedGTIN: function(sStoreID, sGTIN, bForceScanning) {

			if (sGTIN === this._sCurrentlyScannedGTIN && !bForceScanning) {
				// This situation happens after a navigation from the Product Details view
				this.handleReturnFromProductDetails();
				return;
			}

			// Remember the new scanned GTIN
			this._sCurrentlyScannedGTIN = sGTIN;

			// Get the last scanned store product from the data buffer class
			// (it was set there when the validity of the GTIN was checked)
			var oStoreProduct = this._oComponent.getLastScannedStoreProduct();
			if (oStoreProduct) {
				// If a store product is given, use this for the scan process and clear it in the buffer class
				this._oCurrentlyScannedStoreProduct = oStoreProduct;
				this._oComponent.setLastScannedStoreProduct(null);

				// Continue processing
				this.handleScannedGTINContinueWithProduct(sStoreID, sGTIN);
			} else {
				// Read the store product for the scanned GTIN
				this.getDataManager().readSingleProductByBarcode(sStoreID, sGTIN, jQuery.proxy(function(oData) {
					// Success callback
					this._oCurrentlyScannedStoreProduct = oData;
					this.handleScannedGTINContinueWithProduct(sStoreID, sGTIN);
				}, this), jQuery.proxy(function() {
					// Error callback
					this._oCurrentlyScannedStoreProduct = null;
					this.handleScannedGTINContinueWithProduct(sStoreID, sGTIN);
				}, this));
			}
		},

		handleScannedGTINContinueWithProduct: function(sStoreID, sGTIN) {
			var oContext = this.getView().getBindingContext();
			if (!oContext || oContext.getProperty("StoreID") !== sStoreID) {
				// Remember that the GTIN needs to be processed after the new store was read successfully
				this._bProcessScannedGTINAfterRead = true;

				// Set the store to this view
				this.setCurrentStore(sStoreID);
			} else {
				// Process the scanned GTIN directly
				this.processScannedGTIN(sStoreID, sGTIN);
			}
		},

		handleReturnFromProductDetails: function() {
			var oController = this.getProductDetailsController();
			if (oController && oController._bDetailViewActive) {
				// Reset the indicator for active detail view
				oController._bDetailViewActive = false;

				if (oController._sCurrentlyScannedGTIN) {
					// A product was scanned on the details view and the corresponding list was read. Set this view to scan mode as
					// well. The facet filter was already adjusted by the details view
					this._sCurrentlyScannedGTIN = oController._sCurrentlyScannedGTIN;
					this._oCurrentlyScannedStoreProduct = this._oComponent.getLastScannedStoreProduct();

					// Reset the scanned GTIN in product details since it was evaluated now in the product list
					oController._sCurrentlyScannedGTIN = "";
				}

				// Display the product on the list
				var oProduct = oController.getModel("storeProduct").getData();
				if (oProduct) {
					var sStoreID = oProduct.StoreID;
					var sProductID = oProduct.ProductID;
					// Set current store product to processed and submit pending changes if necessary
					if (sStoreID && sProductID) {
						this.getDataManager().setProductProcessed(sProductID);
						DataChangeUtil.submitProductChanges(sStoreID, sProductID);

						// Set the old store product processed in the table (do it manually for performance reasons)
						var oItem = this.getItemByProductID(sProductID);
						if (oItem) {
							this.setItemProcessed(oItem);
						}
					}
					this.displayPackageForProductID(oProduct.ProductID);
				}
			}
		},

		displayPackageForProductID: function(sProductID) {
			// Try to select the product in the currently displayed table
			if (this.selectItemByProductID(sProductID)) {
				// Product is already in the table: scroll to it
				this.scrollToSelectedItem();

				// Update the data in the table from the data manager
				var oModelJSON = this.getProductTable().getModel();
				var oData = oModelJSON.getData();
				var iIndexJSON = this.getProductIndexByID(sProductID, oData.Products);
				if (iIndexJSON > -1) {
					// Update the document with the data from the data manager
					var oProductNew = this.getDataManager().getSingleProduct(null, sProductID);
					oData.Products[iIndexJSON] = this.getRefreshedProduct(oData.Products[iIndexJSON], oProductNew);

					// Set the modified data to the model again in order to update the product table on the UI
					oModelJSON.setData(oData);
				}
			} else {
				// Product is not in the table
				var iIndex = this.getDataManager().getIndexOfProduct(sProductID);
				if (iIndex > -1) {
					// Set new package of products
					this._iCurrentSkip = Math.floor(iIndex / this.getGrowingThreshold()) * this.getGrowingThreshold();
					this.setProductDataFromBuffer();
					this.processAfterListUpdate(jQuery.proxy(function() {
						// Select the given product and scroll to it
						this.selectItemByProductID(sProductID);
						this.scrollToSelectedItem();
					}, this));

					// Read next package of products if necessary
					this.readAdjacentPackagesIfNecessary(sProductID);
				}
			}
		},

		readAdjacentPackagesIfNecessary: function(sProductID) {
			var iIndex = this.getDataManager().getIndexOfProduct(sProductID);
			var iCheckIndex = iIndex + this.getGrowingThreshold();
			if (!this.getDataManager().getSingleProduct(iCheckIndex)) {
				// Read next package in background
				this.getDataManager().readProductsNextPackage(true, jQuery.proxy(this.enableDisableUpDown, this));
			}

			iCheckIndex = iIndex - this.getGrowingThreshold();
			if (iCheckIndex > -1 && !this.getDataManager().getSingleProduct(iCheckIndex)) {
				// Read previous package in background
				this.getDataManager().readProductsPrevPackage(true, jQuery.proxy(this.enableDisableUpDown, this));
			}
		},

		scrollToSelectedItem: function() {
			var oSelElement = null;

			// Get the currently selected item in the product table
			var oSelItem = this.getProductTable().getSelectedItem();
			if (oSelItem) {
				oSelElement = jQuery.sap.domById(oSelItem.getId());
			}

			if (oSelElement) {
				var oSelElementRect = oSelElement.getBoundingClientRect();

				var oFilterElement = jQuery.sap.domById(this.getFacetFilter().getId());
				var oFilterRect = oFilterElement.getBoundingClientRect();

				var iPos = oSelElementRect.top - oFilterRect.top;
				this.byId("orderProductsListPage").scrollTo(iPos, 0);
			}
		},

		setCurrentStore: function(sStoreID) {
			var oContext = this.getView().getBindingContext();
			if (oContext && oContext.getProperty("StoreID") === sStoreID) {
				// In this case the store was already set and the data loaded
				return;
			}

			// Remember that the store entity was not read yet
			this._bStoreEntityReadComplete = false;

			// Create a jQuery deferred object which will be resolved when the filter values of the store are available
			this._oFilterValuesDeferred = jQuery.Deferred();

			var oStoreRequestDeferred = jQuery.Deferred();
			this.getDataManager().readSingleStore(sStoreID, jQuery.proxy(function(oData) {
				// Success callback

				// Insert the user default filter values (personalization) into the facet filter for display of filter text
				FacetFilterUtil.addUserDefaultValuesToFilterValues(oData);
				this.getView().getModel().setData(oData);
				this.onStoreRequestCompleted();
				oStoreRequestDeferred.resolve();
			}, this), jQuery.proxy(function(oError) {
				// Error callback
				if (!sStoreID) {
					// Initial start: open store selection dialog
					this.openStoreSelection();
				} else {
					// Show error popup
					var sErrorDetail = Reuses1TextUtil.getMessageForErrorResponses([oError]);
					MessageBox.error(sErrorDetail);
				}
			}, this));

			this.getDataManager().readFiltersForStore(sStoreID, jQuery.proxy(function(oData) {
				// Wait for the store request to come back (if necessary) and add the available filter values
				oStoreRequestDeferred.done(jQuery.proxy(function() {
					var oModel = this.getView().getModel();
					var oStoreData = oModel.getData();
					FacetFilterUtil.mergeFilterValuesToUserDefaultValues(oStoreData, oData);
					oModel.setData(oStoreData);
					FacetFilterUtil.selectUserDefaultValuesInFilter(this.getFacetFilter());

					// The filter values are available now
					this._oFilterValuesDeferred.resolve();
				}, this));
			}, this), jQuery.proxy(function(oError) {
				return;
			}, this));
		},

		onStoreRequestCompleted: function() {
			var oContext = this.getView().getBindingContext();
			if (!oContext) {
				// Do nothing
				return;
			}

			// Remember that the store entity was read
			this._bStoreEntityReadComplete = true;

			// Set the growing threshold of the product table (with hook method for overriding standard value)
			this.setGrowingThreshold();

			var oSettings = oContext.getProperty("Settings");
			if (oSettings && oSettings.StoreID) {
				var oStoreSettings = {
					StoreID: oSettings.StoreID,
					ExceptionsEnabled: !!oSettings.ExceptionsEnabled,
					RangeOfCoverageEnabled: !!oSettings.RangeOfCoverageEnabled
				};
				this.getView().getModel("Settings").setData(oStoreSettings); //provide access to settings in view.xml
				this.adjustScreenLayoutResponsively(true); //adjust Media Range depending on Excepction Column visibility
				this.getOwnerComponent().setStoreSettings(oStoreSettings);
			}

			// Save the store week display configuration in data buffer
			var oData = oContext.getProperty("WeekDisplay");
			if (oData && oData.StoreID) {
				this._oComponent.setStoreWeekDisplayData(oData);
			}

			if (!this._bProcessScannedGTINAfterRead) {
				this.setInitialStoreProductDataWithFilter();
			} else {
				// A scanned GTIN needs to be processed after the store was read completely
				this._oFilterValuesDeferred.done(jQuery.proxy(function() {
					this.processScannedGTIN(oContext.getProperty("StoreID"), this._sCurrentlyScannedGTIN);
					this._bProcessScannedGTINAfterRead = false;
				}, this));
			}

			// Update the share buttons
			this.updateActionSheetButtons();

			// App start: read store with empty key and after that call the route for the implicitly determined store
			if (this._bSwitchToProductList) {
				this._bSwitchToProductList = false;
				this.getRouter().navTo("toProductList", {
					store: encodeURIComponent(oContext.getProperty("StoreID"))
				}, true); // Make sure that the current navigation entry in the browser history is replaced by the new navigation
			}
		},

		onGTINScanComplete: function(sGTIN) {
			// Check if the order quantity is changed in the same user action and cancel the processing if necessary
			if (this.checkOrderQuantityChange()) {
				return;
			}

			// Check if the given barcode is numerical
			if (!ScanUtil.isBarcodeNumeric(sGTIN)) {
				MessageBox.error(this.getResourceBundle().getText("ERROR_MSG_NOT_NUMERIC"));
				return;
			}

			// Get the current store id
			var oContext = this.getView().getBindingContext();
			if (!oContext || !sGTIN) {
				return;
			}
			var sStoreID = oContext.getProperty("StoreID");

			// Check if the product is already in the displayed list
			var oScannedProduct = this.getDataManager().findProductByBarcode(sGTIN);

			if (oScannedProduct) {
				// Product is already in buffer: display it in the table and focus on it
				this.displayPackageForProductID(oScannedProduct.ProductID);
			} else {
				var that = this;
				// Product is not yet in buffer: read the scanned store product and set it in the data buffer class
				this.getDataManager().readSingleProductByBarcode(sStoreID, sGTIN, jQuery.proxy(function(oData) {
					// Success callback
					oScannedProduct = oData;
					this._oComponent.setLastScannedStoreProduct(oScannedProduct);

					// Get previously selected store product
					var sPreviouslySelectedProduct = this._oComponent.getSelectedStoreProduct();

					if (sPreviouslySelectedProduct && sPreviouslySelectedProduct !== oScannedProduct.ProductID) {
						// A new store product is selected; set the old one to processed in the buffer
						this.getDataManager().setProductProcessed(sPreviouslySelectedProduct);

						// Submit the changes for the old store product
						DataChangeUtil.submitProductChanges(sStoreID, sPreviouslySelectedProduct);
					}

					// Make sure that the filter values of the store are available before moving on
					this._oFilterValuesDeferred.done(jQuery.proxy(function() {
						if (sGTIN !== this._sCurrentlyScannedGTIN) {
							this.handleScannedGTIN(sStoreID, sGTIN);
						} else {
							// The product was already scanned: force the scanning again
							this.handleScannedGTIN(sStoreID, sGTIN, true);
						}
					}, this));
				}, this), function() {
					// Error callback
					that._oComponent.setLastScannedStoreProduct(null);

					// No valid GTIN was scanned --> display an error popup
					MessageBox.error(that.getResourceBundle().getText("ERROR_MSG_SCAN_FAILED"));
				});
			}
		},

		setInitialStoreProductDataWithFilter: function() {
			// Set initial facet filter values from personalization and get filter object (if not disabled)
			var oFilter = null;
			if (!this._mSettings.DisableInitialFilterFromPers) {
				// Use the filter personalization from the oData Service
				oFilter = FacetFilterUtil.buildFilterObjectWithPersOData(this.getFacetFilter());
			}
			// Set the correct binding for the product table
			this.setProductListBindingWithFilter(oFilter, "");
		},

		setProductListBindingWithFilter: function(oFilter, sSearch) {
			// First remove all selections in the product table
			var oProductTable = this.getProductTable();
			oProductTable.removeSelections(true);

			// Check if a product was scanned or not
			var sScannedProductID = "";
			if (this._oCurrentlyScannedStoreProduct) {
				sScannedProductID = this._oCurrentlyScannedStoreProduct.ProductID;
			}

			// Create a new deferred object which will be resolved once the initial product data request is back
			this._oInitialProductPackageDeferred = jQuery.Deferred();
			this._bPageChangePending = false;

			var mOptions = {};
			mOptions.ProductDataExpand = this._mSettings.EnableProductExpand;
			if (mOptions.ProductDataExpand) {
				mOptions.ExpandWithStockProjection = this.getView().getModel("Settings").getProperty("/RangeOfCoverageEnabled");
			}
			mOptions.DisablePaging = FacetFilterUtil.isPagingForbiddenForFilter(this.getFacetFilter());

			var sStoreID = this.getView().getBindingContext().getProperty("StoreID");
			this.getDataManager().readProductsInitial(sStoreID, sScannedProductID, oFilter, sSearch, mOptions, jQuery.proxy(
				function(oData) {
					// Read next/prev package in background
					this.getDataManager().readProductsNextPackage(true, jQuery.proxy(this.enableDisableUpDown, this));
					this.getDataManager().readProductsPrevPackage(true, jQuery.proxy(this.enableDisableUpDown, this));

					// Set product data to table; the skip index might be greater than 0 also after the initial read (after scanning)
					this._iCurrentSkip = this.getDataManager().getCurrentMinIndex();
					this.setProductDataFromBuffer();

					// Resolve the deferred object: initial product package is not available
					this._oInitialProductPackageDeferred.resolve();
				}, this), jQuery.proxy(function(oError) {
				// Error callback: show an error popup with the detailed error message
				var sErrorDetail = Reuses1TextUtil.getMessageForErrorResponses([oError]);
				MessageBox.error(sErrorDetail);

				// Remove data from product table
				this._iCurrentSkip = 0;
				oProductTable.getModel().setData({
					Products: null
				});

				// Disable paging buttons and set default page title
				this.enableDisableUpDown();
				this.byId("orderProductsTitle").setText(this.getResourceBundle().getText("FULLSCREEN_TITLE"));

				// Reject the deferred object: initial product package could not be read
				this._oInitialProductPackageDeferred.reject();

			}, this));
		},

		setProductDataFromBuffer: function() {
			var iSkip = this._iCurrentSkip;
			var aProducts = this.getDataManager().getProducts(iSkip, this.getGrowingThreshold());

			var oModelJSON = this.getProductTable().getModel();
			oModelJSON.setData({
				Products: aProducts
			});

			// Adjust title
			var sTitle = "";
			var iCurrentCount = this.getDataManager().getCurrentCount();
			if (iCurrentCount && iCurrentCount > 0 && aProducts.length > 0) {
				sTitle = this.getResourceBundle().getText("FULLSCREEN_HEADER_TITLE_WITH_NUMBERS", [iSkip + 1, iSkip + aProducts.length,
					iCurrentCount
				]);
			} else {
				sTitle = this.getResourceBundle().getText("FULLSCREEN_TITLE");
			}
			this.byId("orderProductsTitle").setText(sTitle);

			// Enable/disable the paging buttons
			this.enableDisableUpDown();
		},

		onUpPress: function() {
			// If there is already a pending page change request, do nothing (maybe the user clicked the button several times)
			if (this._bPageChangePending) {
				return;
			}

			// Check if the order quantity is changed in the same user action and cancel the processing if necessary
			if (this.checkOrderQuantityChange()) {
				return;
			}

			// Now we have a pending page change request: this flag is reset within setTargetPackageOfProducts; if the initial product
			// request fails, the flag will stay true which is not a problem. It will later be reset if a initial product request is sent.
			this._bPageChangePending = true;

			// Make sure that the initial product package is loaded already
			this._oInitialProductPackageDeferred.done(jQuery.proxy(function() {
				if (this._iCurrentSkip < this.getGrowingThreshold()) {
					return;
				}

				// Remove selected items in the product table
				this.getProductTable().removeSelections(true);

				// Go to the previous page
				this.setTargetPackageOfProducts("Up");
			}, this));
		},

		onDownPress: function() {
			// If there is already a pending page change request, do nothing (maybe the user clicked the button several times)
			if (this._bPageChangePending) {
				return;
			}

			// Check if the order quantity is changed in the same user action and cancel the processing if necessary
			if (this.checkOrderQuantityChange()) {
				return;
			}

			// Now we have a pending page change request
			this._bPageChangePending = true;

			// Make sure that the initial product package is loaded already
			this._oInitialProductPackageDeferred.done(jQuery.proxy(function() {
				if (this._iCurrentSkip + this.getGrowingThreshold() >= this.getDataManager().getCurrentCount()) {
					return;
				}

				// Remove selected items in the product table and scroll to top
				this.getProductTable().removeSelections(true);
				this.byId("orderProductsListPage").scrollTo(0, 0);

				// Go to the next page
				this.setTargetPackageOfProducts("Down");
			}, this));
		},

		setTargetPackageOfProducts: function(sDirection, bRecursive) {
			var fnReadTargetPackage, fnGetTargetPackageBackgroundPending, fnGetTargetPackageBackgroundPromise;
			var iNewSkip, iNewCheckSkip;

			if (sDirection === "Up") {
				fnReadTargetPackage = jQuery.proxy(this.getDataManager().readProductsPrevPackage, this.getDataManager());
				iNewSkip = this._iCurrentSkip - this.getGrowingThreshold();
				iNewCheckSkip = iNewSkip - this.getGrowingThreshold();
			} else if (sDirection === "Down") {
				fnReadTargetPackage = jQuery.proxy(this.getDataManager().readProductsNextPackage, this.getDataManager());
				iNewSkip = this._iCurrentSkip + this.getGrowingThreshold();
				iNewCheckSkip = iNewSkip + this.getGrowingThreshold();
			}

			// Check if target package of products was already read
			if (this.getDataManager().getSingleProduct(iNewSkip)) {
				// Data is already available: check if the (over-)next package needs to be read
				if (!this.getDataManager().getSingleProduct(iNewCheckSkip)) {
					// Read the next package of products in advance
					fnReadTargetPackage(true, jQuery.proxy(this.enableDisableUpDown, this));
				}

				// Set product data to table on UI
				this._iCurrentSkip = iNewSkip;
				this.setProductDataFromBuffer();

				// Allow further page changes
				this._bPageChangePending = false;
			} else {
				// Data is not available yet: theck if there is a pending background request for reading the target page
				if (sDirection === "Up") {
					fnGetTargetPackageBackgroundPending = jQuery.proxy(this.getDataManager().getPrevPackageBackgroundPending,
						this.getDataManager());
					fnGetTargetPackageBackgroundPromise = jQuery.proxy(this.getDataManager().getPrevPackageBackgroundPromise,
						this.getDataManager());
				} else if (sDirection === "Down") {
					fnGetTargetPackageBackgroundPending = jQuery.proxy(this.getDataManager().getNextPackageBackgroundPending,
						this.getDataManager());
					fnGetTargetPackageBackgroundPromise = jQuery.proxy(this.getDataManager().getNextPackageBackgroundPromise,
						this.getDataManager());
				}

				if (!bRecursive && fnGetTargetPackageBackgroundPending() && fnGetTargetPackageBackgroundPromise()) {
					// There is a pending background request for reading the target package -> wait until this request is finished and then try again to set the target package (do it only once).
					// We also have to make sure that the background request is 'converted' to a foreground request (busy dialog, ...) by calling onRequestSent/onRequestCompleted
					this.getDataManager().onRequestSent();
					fnGetTargetPackageBackgroundPromise().done(jQuery.proxy(function() {
						this.getDataManager().onRequestCompleted();
						this.setTargetPackageOfProducts(sDirection, true);
					}, this));
				} else {
					// There is no pending background request -> read the target page now
					fnReadTargetPackage(false, jQuery.proxy(function() {
						// Set product data to table and read next package in background
						this._iCurrentSkip = iNewSkip;
						fnReadTargetPackage(true, jQuery.proxy(this.enableDisableUpDown, this));
						this.setProductDataFromBuffer();

						// Allow further page changes
						this._bPageChangePending = false;
					}, this), jQuery.proxy(function(oError) {
						// Error callback: show an error popup with the detailed error message
						var sErrorDetail = Reuses1TextUtil.getMessageForErrorResponses([oError]);
						MessageBox.error(sErrorDetail);

						// Allow further page changes
						this._bPageChangePending = false;
					}, this));
				}
			}
		},

		enableDisableUpDown: function() {
			var iSkip = this._iCurrentSkip;
			if (iSkip < this.getGrowingThreshold()) {
				this.byId("orderProductsButtonUp").setEnabled(false);
			} else {
				this.byId("orderProductsButtonUp").setEnabled(true);
			}

			if (iSkip + this.getGrowingThreshold() >= this.getDataManager().getCurrentCount()) {
				this.byId("orderProductsButtonDown").setEnabled(false);
			} else {
				this.byId("orderProductsButtonDown").setEnabled(true);
			}
		},

		checkOrderQuantityChange: function() {
			var bCancelProcessing = false;
			var aCells = [];
			var aItemsFlexBox = [];
			var oInput = null;
			var oOtherInput = null;
			var fQuantity = 0;
			var oProduct = null;
			var j;
			var iLengthCells;

			if (this._sProductLiveChange) {
				var oItem = this.getItemByProductID(this._sProductLiveChange);
				if (oItem) {
					// Get current order quantity in model
					fQuantity = oItem.getBindingContext().getProperty("OrderQuantity");

					aCells = oItem.getCells();
					iLengthCells = aCells.length;
					for (j = 0; j < iLengthCells; j++) { // Loop over item cells
						if (aCells[j].getId().indexOf("orderProductsVBoxOrderQuantity") > -1 || aCells[j].getId().indexOf(
								"orderProductsHBoxOrderQuantityTablet") > -1) {
							aItemsFlexBox = aCells[j].getItems();
							if (aItemsFlexBox && aItemsFlexBox[0] && (aItemsFlexBox[0].getId().indexOf("orderProductsInputOrderQuantity") > -1 ||
									aItemsFlexBox[0].getId().indexOf("orderProductsInputOrderQuantityTablet") > -1)) {
								// Check if the order quantity is changed in the same user action
								oInput = aItemsFlexBox[0];
								if (Number(oInput.getValue()) !== Reuses1Formatter.roundQuantityMaxTwoDigits(fQuantity)) {
									// Simulate the "order quantity changed" event
									this._sProductLiveChange = "";

									oProduct = oItem.getBindingContext().getObject();
									oOtherInput = this.getInputFromOtherLayout(oInput);
									fQuantity = oInput.getValue();
									if (DataChangeUtil.changeOrderQuantity(oProduct, fQuantity, oInput, oOtherInput, jQuery.proxy(
											function() {
												// Callback function before update: set the changed order quantity on the UI and set the item to processed
												this.setChangedOrderQuantity(oItem, fQuantity);
												this.setItemProcessed(oItem);
											}, this), null, null, null)) {
										// An error/warning popup is displayed -> Cancel current processing
										bCancelProcessing = true;
									}

									break;
								}
							}
						}
					}
				}
			}

			if (this._bErrorWarningPopupOpen) {
				// In case a warning/error popup is displayed (maybe order quantity changed in the same step) --> Cancel current
				// processing
				bCancelProcessing = true;
			}

			return bCancelProcessing;
		},

		onOrderQuantityChange: function(oEvent) {
			// Check if there is still a pending live change; it might be, that a pending change was already submitted by checkOrderQuantityChange
			if (this._sProductLiveChange) {
				this._sProductLiveChange = "";
			} else {
				return;
			}

			var oInput = oEvent.getSource();
			var oOtherInput = this.getInputFromOtherLayout(oInput);
			var oItem = oInput.getParent().getParent();
			var oProduct = oItem.getBindingContext().getObject();
			var fQuantity = oEvent.getParameter("newValue");

			// Submit the changed order quantity to the buffer and the backend
			if (DataChangeUtil.changeOrderQuantity(oProduct, fQuantity, oInput, oOtherInput, jQuery.proxy(
					function() {
						// Callback function before update: set the changed order quantity on the UI and set the item to processed
						this.setChangedOrderQuantity(oItem, fQuantity);
						this.setItemProcessed(oItem);
					}, this), jQuery.proxy(function() {
					// Callback function for popup close
					this._bErrorWarningPopupOpen = false;
				}, this), null, null)) {
				// A warning/error popup is opened --> up/down and back function should not be executed now
				this._bErrorWarningPopupOpen = true;
			}
		},

		onOrderQuantityLiveChange: function(oEvent) {
			var oInput = oEvent.getSource();
			var oOtherInput = this.getInputFromOtherLayout(oInput);

			// Set the current value in the smartphone/tablet input field as well
			if (oOtherInput && oOtherInput.setValue) {
				oOtherInput.setValue(oInput.getValue());
			}

			var oContext = oInput.getBindingContext();
			var fQuantityModel = oContext.getProperty("OrderQuantity");
			if (Number(oInput.getValue()) !== Reuses1Formatter.roundQuantityMaxTwoDigits(fQuantityModel)) {
				this._sProductLiveChange = oContext.getProperty("ProductID");
			} else {
				this._sProductLiveChange = "";
			}
		},

		getInputFromOtherLayout: function(oInput) {
			var oOtherCell = null;
			var oOtherInput = null;

			var oItem = oInput.getParent().getParent();
			var aCells = oItem.getCells();

			var sInputID = oInput.getId();
			if (sInputID.indexOf("orderProductsInputOrderQuantityTablet") > -1) {
				// Get the VBox of the smartphone input field
				oOtherCell = aCells.filter(function(oCell) {
					return (oCell.getId().indexOf("orderProductsVBoxOrderQuantity") > -1);
				})[0];

				// Get the smartphone input field
				if (oOtherCell && oOtherCell.getItems) {
					oOtherInput = oOtherCell.getItems()[0];
				}

			} else if (sInputID.indexOf("orderProductsInputOrderQuantity") > -1) {
				// Get the HBox of the tablet input field
				oOtherCell = aCells.filter(function(oCell) {
					return (oCell.getId().indexOf("orderProductsHBoxOrderQuantityTablet") > -1);
				})[0];

				// Get the tablet input field
				if (oOtherCell && oOtherCell.getItems) {
					oOtherInput = oOtherCell.getItems()[0];
				}
			}

			return oOtherInput;
		},

		onAddButtonPress: function(oEvent) {

			var oProductTable = this.getProductTable();
			var oItem = oProductTable.getSelectedItem();

			if (!oItem) {
				// No line selected, keep the data as it is
				MessageToast.show(this.getResourceBundle().getText("INFO_MSG_ITEM_SELECT"));
				return;
			}

			var oProduct = oItem.getBindingContext().getObject();
			var fChangedOrderQuantity = DataChangeUtil.increaseOrderQuantity(oProduct);
			if (fChangedOrderQuantity !== null) {
				this.setChangedOrderQuantity(oItem, fChangedOrderQuantity);
			}
		},

		onLessButtonPress: function(oEvent) {

			var oProductTable = this.getProductTable();
			var oItem = oProductTable.getSelectedItem();

			if (!oItem) {
				// No line selected, keep the data as it is
				MessageToast.show(this.getResourceBundle().getText("INFO_MSG_ITEM_SELECT"));
				return;
			}

			var oProduct = oItem.getBindingContext().getObject();
			var fChangedOrderQuantity = DataChangeUtil.decreaseOrderQuantity(oProduct);
			if (fChangedOrderQuantity !== null) {
				this.setChangedOrderQuantity(oItem, fChangedOrderQuantity);
			}
		},

		setChangedOrderQuantity: function(oItem, fChangedOrderQuantity, bNoDataUpdate) {
			var aItems = [];
			var sText = "";

			var oProduct = oItem.getBindingContext().getObject();
			if (!bNoDataUpdate) {
				oProduct.OrderQuantity = fChangedOrderQuantity;
				oProduct.IsChangedOrderQuantityPosted = null;
			}

			// Get all cells of the respective list item
			var aCells = oItem.getCells();
			for (var i = 0; i < aCells.length; i++) {
				if (aCells[i].getId().indexOf("orderProductsVBoxOrderQuantity") > -1 || aCells[i].getId().indexOf(
						"orderProductsHBoxOrderQuantityTablet") > -1) {
					// Set the new order quantity in the input fields of the table cell
					aItems = aCells[i].getItems();
					if (aItems && aItems[0] && (aItems[0].getId().indexOf("orderProductsInputOrderQuantity") > -1 || aItems[0].getId().indexOf(
							"orderProductsInputOrderQuantityTablet") > -1)) {
						aItems[0].setValue(Reuses1Formatter.roundQuantityMaxTwoDigits(oProduct.OrderQuantity));
						aItems[0].setValueState(formatter.formatInputValueState(oProduct.IsChangedOrderQuantityPosted, oProduct.isOrderQuantityChanged));
					}
				}

				if (aCells[i].getId().indexOf("orderProductsVBoxProductQuantities") > -1) {
					// Set the new order quantity in the respective text field within the "quantities" column
					aItems = aCells[i].getItems && aCells[i].getItems();
					if (aItems && aItems[0]) {
						aItems = aItems[0].getItems();
						if (aItems && aItems[1] && aItems[1].getId().indexOf("orderProductsColumnQuantityTextOrderQuantity") > -1) {
							sText = formatter.formatOrderQuantityConversion(oProduct.OrderQuantity, oProduct.SalesToOrderQuantityNmrtr,
								oProduct.SalesToOrderQuantityDnmntr);
							aItems[1].setText(sText);
						}
					}
				}

				if (aCells[i].getId().indexOf("orderProductsHBoxOrderQuantityTablet") > -1) {
					// Set the new order quantity in the respective text field for the tablet layout column
					aItems = aCells[i].getItems();
					if (aItems && aItems[2]) {
						aItems = aItems[2].getItems();
						if (aItems && aItems[0] && aItems[0].getId().indexOf("orderProductsTextQuantityUnitTablet") > -1) {
							sText = formatter.formatOrderUOMWithConversion(oProduct.SalesToOrderQuantityNmrtr, oProduct.SalesToOrderQuantityDnmntr,
								oProduct.OrderQuantity,
								oProduct.OrderQuantityUnitCode, oProduct.SalesQuantityUnitCode, oProduct.OrderQuantityUnitName);
							aItems[0].setText(sText);
						}
					}
				}
			}
		},

		setGrowingThreshold: function() {
			var iGrowingThreshold = 20; // Default

			if (sap.ui.Device.system.phone) {
				iGrowingThreshold = 5;
			}

			/**
			 * @ControllerHook [Provide growing threshold for product table] // EXC_LINTER_014 The hook is called when the data
			 *                 of the current store was read. The products are always displayed in packages with the size of the
			 *                 growing threshold.
			 * @callback retail.store.orderproducts1.controller.ProductList~extHookGetGrowingThreshold
			 * @return {number} The threshold for the product table
			 */
			if (this.extHookGetGrowingThreshold) {
				var iGrowingThresholdHook = this.extHookGetGrowingThreshold();

				// Check if a positive number is returned and then take the new value
				if (typeof iGrowingThresholdHook === "number" && iGrowingThresholdHook > 0) {
					iGrowingThreshold = iGrowingThresholdHook;
				}
			}

			// Set the growing threshold in the data manager as well
			this.getDataManager().setGrowingThreshold(iGrowingThreshold);

			// Save the current growing threshold
			this._iGrowingThreshold = iGrowingThreshold;

			// Set the number of product pages to be preloaded
			var iNumberOfPreloadPages = 1; // Default
			if (sap.ui.Device.system.phone) {
				iNumberOfPreloadPages = 5; // Default for phone
			}
	
			/**
			 * @ControllerHook [Provide number of preload pages for product table]
			 * The hook is called when the data of the current store was read. It can be used to adjust the number of product pages to be preloaded.
			 * @callback retail.store.orderproducts1.controller.ProductList~extHookGetNumberOfPreloadPages
			 * @return {number} The number of product pages to be preloaded. It must be greater than or equal to 1.
			 */
			if (this.extHookGetNumberOfPreloadPages) {
				var iNumberOfPreloadPagesHook = this.extHookGetNumberOfPreloadPages();
	
				// Check if a positive number is returned and then take the new value
				if (typeof iNumberOfPreloadPagesHook === "number" && iNumberOfPreloadPagesHook >= 1) {
					iNumberOfPreloadPages = Math.ceil(iNumberOfPreloadPagesHook);
				}
			}
	
			// Set the number of preload pages in the data manager where the preloading is done
			this.getDataManager().setNumberOfPreloadPages(iNumberOfPreloadPages);
		},

		getGrowingThreshold: function() {
			return this._iGrowingThreshold;
		},

		refreshProductDataInList: function(aProductIDs) {
			var iIndex = -1;
			var oProductNew = null;

			var oModelJSON = this.getProductTable().getModel();
			var oData = oModelJSON.getData();

			for (var i = 0; i < aProductIDs.length; i++) {
				iIndex = this.getProductIndexByID(aProductIDs[i], oData.Products);

				if (iIndex > -1) {
					// Update the document with the data from the data manager
					oProductNew = this.getDataManager().getSingleProduct(null, aProductIDs[i]);
					oData.Products[iIndex] = this.getRefreshedProduct(oData.Products[iIndex], oProductNew);
				}
			}

			// Set the modified data to the model again in order to update the product table on the UI
			oModelJSON.setData(oData);

			// In some cases (e.g. after several failed updates) the setData does not update the order quantity in the list anymore.
			// That's why the order quantity (and the value state of the input fields) is updated manually again to be sure we have a consistent state.
			for (i = 0; i < aProductIDs.length; i++) {
				var oItem = this.getItemByProductID(aProductIDs[i]);
				if (oItem) {
					// The data does not need to be updated again - only the UI controls
					this.setChangedOrderQuantity(oItem, null, true);
				}
			}
		},

		getProductTable: function() {
			return this.byId("orderProductsTableProducts");
		},

		getFacetFilter: function() {
			return this.byId("orderProductsFacetFilter");
		},

		getSearchField: function() {
			return this.byId("orderProductsSearchFieldProducts");
		},

		getRefreshedProduct: function(oProductOld, oProductNew) {
			var oProduct = oProductNew;
			return oProduct;
		},

		setBindingForView: function() {
			// Create new JSON model for view
			var oView = this.getView();
			var oModel = new sap.ui.model.json.JSONModel();
			oModel.setDefaultBindingMode(sap.ui.model.BindingMode.OneWay);
			oView.setModel(oModel);

			// Set the binding context of the view in order to stay compatible with previous version (direct oData binding)
			oView.bindElement("/");

			// Get product table and item template
			var oTable = this.getProductTable();
			var oTemplate = oTable.getItems()[0].clone();

			// Create new JSON model for displaying product data in product table
			oModel = new sap.ui.model.json.JSONModel();
			oModel.setDefaultBindingMode(sap.ui.model.BindingMode.OneWay);
			oTable.setModel(oModel);

			// Create binding info for item aggregation of product table
			var oBindingInfo = {
				path: "/Products",
				template: oTemplate
			};

			/**
			 * @ControllerHook [Adjust the binding info of the product table] The hook is called once when the app is started.
			 *                 It can be used to modify the binding info for the item aggregation of the product table
			 * @callback retail.store.orderproducts1.controller.ProductList~extHookAdjustBindingInfo
			 * @param {object}
			 *          oBindingInfo The binding info, which is later passed to the product table via bindAggregation
			 * @return {void}
			 */
			if (this.extHookAdjustBindingInfo) {
				this.extHookAdjustBindingInfo(oBindingInfo);
			}

			// Bind item aggregation of product table
			oTable.bindAggregation("items", oBindingInfo);
		},

		getProductIndexByID: function(sProductID, aProducts) {
			var iIndex = -1;
			if (aProducts && aProducts.length > 0) {
				var iLength = aProducts.length;
				for (var i = 0; i < iLength; i++) {
					if (aProducts[i].ProductID && aProducts[i].ProductID === sProductID) {
						iIndex = i;
						break;
					}
				}
			}
			return iIndex;
		},

		resetScanMode: function() {
			this._sCurrentlyScannedGTIN = "";
			this._oCurrentlyScannedStoreProduct = null;
		},

		switchScanModeToStandardMode: function() {
			// Reset scan variables
			this.resetScanMode();

			// Apply current filter object
			this.applyCurrentFilterToProductList();

			// Navigate to the "standard" mode
			this.getRouter().navTo("toProductList", {
				store: encodeURIComponent(this.getView().getBindingContext().getProperty("StoreID"))
			}, true);
		},

		applyCurrentFilterToProductList: function() {
			// Build current filter object
			var oFilter = FacetFilterUtil.buildFilterObject(this.getFacetFilter());

			// Clear the search field and update the product list binding
			this.getSearchField().setValue("");
			this.setProductListBindingWithFilter(oFilter, "");
		},

		createAssignStoreButton: function() {
			var oButton = new sap.m.Button({
				text: this.getResourceBundle().getText("ASSIGN_STORE_BUTTON"),
				tooltip: this.getResourceBundle().getText("ASSIGN_STORE_BUTTON"),
				press: jQuery.proxy(function() {
					// Open store selection dialog
					this.openStoreSelection();
				}, this)
			});
			this.getView().addDependent(oButton);
			sap.ushell.services.AppConfiguration.addApplicationSettingsButtons([oButton]);
		},

		createActionSheet: function() {
			// Create an action sheet which will be opened to the top
			this._oActionSheet = new sap.m.ActionSheet({
				placement: "Top"
			});
			this.getView().addDependent(this._oActionSheet);

			// Add bookmark button for creating tiles
			this._oBookmarkButton = new sap.ushell.ui.footerbar.AddBookmarkButton({ id: "orderProductsAddBookmarkButton" });
			this._oActionSheet.addButton(this._oBookmarkButton);

			// Add a Jam Share button
			this._oJamShareButton = new sap.ushell.ui.footerbar.JamShareButton({ id: "orderProductsJamShareButton" });
			this._oActionSheet.addButton(this._oJamShareButton);

			// Add a navigation button to the product lookup app
			var oNavButton = new sap.m.Button({
				id: "orderProductsLookupProductsButton",
				text: this.getResourceBundle().getText("LIST_BUTTON_LOOKUP_PRODUCTS"),
				icon: "sap-icon://Fiori3/F0535",
				press: jQuery.proxy(this.onNavToProductLookup, this)
			});
			this._oActionSheet.addButton(oNavButton);

			// Add a navigation button to the adjust stock app
			oNavButton = new sap.m.Button({
				id: "orderProductsAdjustStockButton",
				text: this.getResourceBundle().getText("DETAIL_BUTTON_ADJUST_STOCK"),
				icon: "sap-icon://Fiori4/F0586",
				press: jQuery.proxy(this.onNavToAdjustStock, this)
			});
			this._oActionSheet.addButton(oNavButton);

			/**
			 * @ControllerHook [Add buttons to the action sheet] // EXC_LINTER_014 The hook is called once when the app is
			 *                 started.
			 * @callback retail.store.orderproducts1.controller.ProductList~extHookAddActionSheetButtons
			 * @param {object}
			 *          oActionSheet The action sheet control (sap.m.ActionSheet), which can be enhanced with new buttons.
			 * @return {void}
			 */
			if (this.extHookAddActionSheetButtons) {
				this.extHookAddActionSheetButtons(this._oActionSheet);
			}
		},

		updateActionSheetButtons: function() {

			var oContext = this.getView().getBindingContext();
			var sStoreName = oContext.getProperty("StoreName");
			var sStoreId = oContext.getProperty("StoreID");

			// Update the bookmark button
			var oAppData = (this._oBookmarkButton.getAppData() || {});
			jQuery.extend(oAppData, {
				title: this.getResourceBundle().getText("FULLSCREEN_TITLE"),
				subtitle: sStoreName + " (" + sStoreId + ")"
			});

			this._oBookmarkButton.setAppData(oAppData);

			// Update the Jam Share Button
			var oDisplay = new sap.m.ObjectListItem({
				title: sStoreName,
				attributes: [new sap.m.ObjectAttribute({
					text: sStoreId
				})]
			});

			var oJamShareSettings = (this._oJamShareButton.getJamData() || {});
			jQuery.extend(oJamShareSettings, {
				object: {
					id: document.URL,
					display: oDisplay,
					share: sStoreName + " (" + sStoreId + ")"
				}
			});

			this._oJamShareButton.setJamData(oJamShareSettings);
		},

		onNavToProductLookup: function() {
			var oContext = this.getView().getBindingContext();
			if (oContext && this._oCrossAppNav) {
				this._oCrossAppNav.toExternal({
					target: {
						semanticObject: "Article",
						action: "lookup"
					},
					params: {
						SiteID: oContext.getProperty("StoreID")
					}
				}, this._oComponent);
			}
		},

		onNavToAdjustStock: function() {
			var oContext = this.getView().getBindingContext();
			if (oContext && this._oCrossAppNav) {
				this._oCrossAppNav.toExternal({
					target: {
						semanticObject: "Article",
						action: "correctStock"
					}
				}, this._oComponent);
			}
		},

		processScannedGTIN: function(sStoreID, sGTIN) {
			if (!sStoreID || !sGTIN) {
				return;
			}

			if (this._oCurrentlyScannedStoreProduct) {
				// Reset the facet filter control and the search field
				FacetFilterUtil.resetFacetFilter(this.getFacetFilter());
				this.getSearchField().setValue("");

				// Build the filter according to the scanned store product
				var oFilter = ScanUtil.buildFilterObjectWithStoreProduct(this.getFacetFilter(), this._oCurrentlyScannedStoreProduct,
					sGTIN);

				// Set the new filter and select the scanned product after read is finished
				this.setProductListBindingWithFilter(oFilter, "");
				this.processAfterListUpdate(jQuery.proxy(this.selectScannedProductAfterDataReceived, this));
			} else {
				MessageBox.error(this.getResourceBundle().getText("ERROR_MSG_SCAN_FAILED"));
			}
		},

		selectScannedProductAfterDataReceived: function() {
			if (this._oCurrentlyScannedStoreProduct) {
				this.selectItemByProductID(this._oCurrentlyScannedStoreProduct.ProductID);
				this.scrollToSelectedItem();
			}
		},

		selectItemByProductID: function(sProductID) {
			var oItem = this.getItemByProductID(sProductID);
			if (oItem) {
				this.getProductTable().setSelectedItem(oItem);
				this.selectListItem(oItem);
				return true; // The given ProductID could be found and selected in the list
			} else {
				return false; // The given ProductID could not be found in the list
			}
		},

		getItemByProductID: function(sProductID) {
			var oItem = null;
			var aItems = this.getProductTable().getItems();
			var iLength = aItems.length;
			for (var i = 0; i < iLength; i++) {
				if (aItems[i].getBindingContext().getProperty("ProductID") === sProductID) {
					oItem = aItems[i];
					break;
				}
			}
			return oItem;
		},

		adjustFacetFilter: function() {
			var oFacetFilter = this.getFacetFilter();
			var that = this;

			// Override the function to create the "All" checkbox
			oFacetFilter._createSelectAllCheckboxBar = function(oList) { // TODO--This is a private method
				// First call the standard implementation
				var oBar = sap.m.FacetFilter.prototype._createSelectAllCheckboxBar.call(this, oList);

				var sKey = oList.getKey();
				if (sKey === "Attributes") {
					// In case we are on the attributes list: display "No Filter" instead of "All"
					var oCheckbox = sap.ui.getCore().byId(oList.getAssociation("allcheckbox"));
					var sText = that.getResourceBundle().getText("LIST_FACET_FILTER_NONE");
					if (oCheckbox) {
						oCheckbox.setText(sText);
					}
				}
				return oBar;
			};

			// Override the function to open the facet filter dialog
			oFacetFilter.openFilterDialog = function() {
				that._oFilterValuesDeferred.done(jQuery.proxy(function() {
					// Call the standard implementation
					sap.m.FacetFilter.prototype.openFilterDialog.call(this);
				}, this));
			};

			// Override the function to close the facet filter dialog
			oFacetFilter._closeDialog = function() {
				// First call the standard implementation
				sap.m.FacetFilter.prototype._closeDialog.call(this);

				// Reset scan mode and apply filter to product list
				that.switchScanModeToStandardMode();

				// Set the currently selected filter to personalization
				that.applyFacetFilterForPersonalization();
			};
		},

		onFacetFilterReset: function(oEvent) {
			// Reset the facet filter control
			FacetFilterUtil.resetFacetFilter(this.getFacetFilter());

			// No scan mode anymore
			this.switchScanModeToStandardMode();

			// Set the currently selected filter to personalization
			this.applyFacetFilterForPersonalization();
		},

		onScanButtonPress: function(oEvent) {
			/**
			 * @ControllerHook [React on scan button] // EXC_LINTER_014 The hook is called when the scan button is pressed in
			 *                 the app. A laser scanner can be triggered here. When the GTIN is determined, the function
			 *                 onGTINScanComplete of this controller should be called with the scanned GTIN.
			 * @callback retail.store.orderproducts1.controller.ProductList~extHookOnScanButtonPress
			 * @param {object}
			 *          oEvent The event object of the button press event
			 * @return {void}
			 */
			if (this.extHookOnScanButtonPress) {
				this.extHookOnScanButtonPress(oEvent);
			} else {
				// Call the standard barcode scanner which either uses the camera scanner or shows a popup
				this.standardScanButtonHandling();
			}
		},

		standardScanButtonHandling: function() {
			BarcodeScanner.scan(jQuery.proxy(function(oResult) {
				// Barcode scan successful
				if (oResult.text && !oResult.cancelled) {
					this.getBarcodeScanHandler().handleBarcodeScan(oResult.text);
				}
			}, this), null, jQuery.proxy(function(oParams) {
				// Live change event of input field
				ScanUtil.handleInputLiveChange(oParams.newValue, jQuery.proxy(function(sGTIN) {
					// Continue if automatic GTIN processing is enabled (smartphone/tablet)
					this.getBarcodeScanHandler().handleBarcodeScan(sGTIN);
				}, this));
			}, this));
		},

		openStoreSelection: function() {
			var sCurrentStoreID = "";

			var oDialog = this._mDialogs.StoreSelection;
			if (!oDialog) {
				var sFragmentName = null;

				/**
				 * @ControllerHook [Get fragment name for store selection] The hook is called when the store selection dialog is
				 *                 opened for the first time. It can be used to change the layout of the store selection dialog
				 *                 via an own list item template.
				 * @callback retail.store.orderproducts1.controller.ProductList~extHookGetStoreSelectionFragmentName
				 * @return {string} The fully specified name (i.e. path) of the list item fragment
				 */
				if (this.extHookGetStoreSelectionFragmentName) {
					sFragmentName = this.extHookGetStoreSelectionFragmentName();
				}

				// Create the store selection dialog
				oDialog = new StoreSelectDialog({
					dataManager: this.getDataManager(),
					propertySiteId: "StoreID",
					propertyDescription: "StoreName",
					propertyAddress: "FormattedAddress",
					listItemFragmentName: sFragmentName, // If null, default template is used
					confirm: jQuery.proxy(this.confirmStoreSelection, this),
					cancel: jQuery.proxy(this.cancelStoreSelection, this)
				});
				this._mDialogs.StoreSelection = oDialog;
				this.getView().addDependent(oDialog);
				jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), oDialog);
			}

			var oContext = this.getView().getBindingContext();
			if (oContext) {
				sCurrentStoreID = oContext.getProperty("StoreID");
			}

			if (sCurrentStoreID) {
				// Set the currently assigned store to the selection dialog
				oDialog.setAssignedSiteId(sCurrentStoreID);
			}

			// Lock barcode scan handling and open the store selection dialog
			this.getBarcodeScanHandler().lockBarcodeScanHandling();
			oDialog.open();
		},

		applyFacetFilterForPersonalization: function() {
			// Use the oData service personalization: first get StoreID and selected filter values (as object)
			var sStoreID = this.getView().getBindingContext().getProperty("StoreID");
			var oSelectedFilters = FacetFilterUtil.getSelectedFilterValues(this.getFacetFilter());

			// Save the selected filter values in the oData service personalization
			this.getDataManager().saveSelectedFilterValues(sStoreID, oSelectedFilters);
		},

		adjustScannedProductFilterAfterClose: function(oFacetFilter, oScannedProductItem) {
			if (!ScanUtil.isMerchandiseCategorySelectionEnabled(this._oCurrentlyScannedStoreProduct, this._sCurrentlyScannedGTIN)) {
				return;
			}

			// Check if the user selected an additional or another merchandise category than that of the scanned product
			var oMerchCatList = oFacetFilter.getLists().filter(function(oList) {
				return (oList.getKey() === "MerchandiseCategory");
			})[0];

			if (oMerchCatList) {
				var aSelectedItems = oMerchCatList.getItems().filter(function(oItem) {
					return oItem.getSelected();
				});

				if (aSelectedItems.length !== 1 || aSelectedItems[0].getKey() !== this._oCurrentlyScannedStoreProduct.MerchandiseCategory) {
					oScannedProductItem.setSelected(false);
				}
			}
		},

		confirmStoreSelection: function(oEvent) {
			// Unlock barcode scan handling
			this.getBarcodeScanHandler().unlockBarcodeScanHandling();

			// Get the store ID
			var oSelectedItem = oEvent.getParameter("selectedItem");
			var oContext = oSelectedItem.getBindingContext("ReuseModelDialog");
			var sStoreID = oContext.getProperty("StoreID");

			if (!sStoreID) {
				return;
			}

			var oRouter = this.getRouter();
			var fnNavToStore = function() {
				oRouter.navTo("toProductList", {
					store: encodeURIComponent(sStoreID)
				}, true);
			};

			// Update the assigned store for the user
			this.getDataManager().updateStoreAssignment(sStoreID, jQuery.proxy(function() {
				// Success callback
				this._bStoreAssignmentChanged = true;
				fnNavToStore();
			}, this), function() {
				// Error callback
				jQuery.sap.log.warning("User could not be assigned to store");
				fnNavToStore();
			});
		},

		cancelStoreSelection: function() {
			// Just unlock barcode scan handling
			this.getBarcodeScanHandler().unlockBarcodeScanHandling();
		},

		getProductDetailsController: function() {
			var oController = null;

			var aPages = this.getView().getParent().getPages();
			var oListView = jQuery.grep(aPages, function(oItem) {
				if (oItem.getViewName && oItem.getViewName() === "retail.store.orderproducts1.view.ProductDetails") {
					return true;
				} else {
					return false;
				}
			})[0];

			if (oListView) {
				oController = oListView.getController();
			}

			return oController;
		},

		onListUpdateFinished: function() {
			this.ensureQuantityAlignment();
			if (this._fnProcessAfterListUpdate) {
				this._fnProcessAfterListUpdate();
			}
		},

		processAfterListUpdate: function(fnCallback) {
			this._fnProcessAfterListUpdate = jQuery.proxy(function() {
				this._fnProcessAfterListUpdate = null;
				fnCallback();
			});
		},

		ensureQuantityAlignment: function() {
			var aElements = jQuery(".sapRTSTOrdProdQuantityForAlignment");
			this.ensureEqualWidthForElements(aElements);
		},

		ensureEqualWidthForElements: function(aElements) {
			var iWidthMax = 0;
			for (var i = 0, iLength = aElements.length; i < iLength; i++) {
				// Get the with of every element
				var oRect = aElements[i].getBoundingClientRect();
				var iWidth = oRect && oRect.width || 0;

				// If there is an element with differing width, all elements need to be adjusted
				if (i > 0 && iWidth !== iWidthMax) {
					var bAdjustmentNeeded = true;
				}

				// Calculate the maximum width of all elements
				iWidthMax = Math.max(iWidth, iWidthMax);
			}

			if (bAdjustmentNeeded) {
				// Set the maximum element width to the UI5 control representation of every element
				var oCore = sap.ui.getCore();
				for (i = 0; i < iLength; i++) {
					var oControl = oCore.byId(aElements[i].id);
					if (oControl && oControl.setWidth) {
						oControl.setWidth(iWidthMax + "px");
					}
				}
			}
		},

		onNavBack: function() {
			// Check if the order quantity is changed in the same user action and cancel the processing if necessary
			if (this.checkOrderQuantityChange()) {
				return;
			}

			window.history.go(-1);
		},

		onSearch: function(oEvent) {
			// Get the entered search string
			var sSearchPattern = oEvent.getParameter("query");

			// Get the current facet filter
			var oFilter = FacetFilterUtil.buildFilterObject(this.getFacetFilter());

			// Reset scan variables
			this.resetScanMode();

			// Apply the search string to backend call and additionally set the filter (if set by the user)
			this.setProductListBindingWithFilter(oFilter, sSearchPattern);
			this.processAfterListUpdate(jQuery.proxy(this.setAllProductsVisible, this));

			// Navigate to the "standard" mode (if necessary)
			this.getRouter().navTo("toProductList", {
				store: encodeURIComponent(this.getView().getBindingContext().getProperty("StoreID"))
			}, true);
		},

		setAllProductsVisible: function() {
			// After a backend search, all items on the UI must be visible
			var aItems = this.getProductTable().getItems();
			var iLength = aItems.length;
			for (var i = 0; i < iLength; i++) {
				aItems[i].setVisible(true);
			}
		},

		onActionButtonPress: function(oEvent) {
			if (!this._oActionSheet.isOpen()) {
				this._oActionSheet.openBy(oEvent.getSource());
			} else {
				this._oActionSheet.close();
			}
		},

		setInteroperabilityProperties: function() {
			// Get interop properties from helper class and save it in this controller
			this._mInteropProperties = InteroperabilityHelper.getInteropProperties();
		},

		handleDeviceSpecificFeatures: function() {
			// Make sure that the content of the input field for order quantities is preselected when focused
			// -> do it for the "smartphone" layout
			var oInput = this.getView().byId("orderProductsInputOrderQuantity");
			var that = this;
			var fnFocusIn = function(oEvent) {
				// Select the corresponding line in the table
				var oItem = this.getParent().getParent();
				if (oItem && that.getProductTable().getSelectedItem() !== oItem) {
					that.getProductTable().setSelectedItem(oItem);
					that.selectListItem(oItem);
				}

				// Call the select method, which works for some device/browser combinations
				jQuery.sap.delayedCall(0, this.getFocusDomRef(), "select");

				// Additionally call the selectText method, which works for some other device/browser combinations
				if (oInput.selectText) {
					jQuery.sap.delayedCall(100, this, "selectText", [0, this.getValue().length]);
				} else {
					jQuery.sap.delayedCall(100, jQuery(this.getFocusDomRef()), "selectText", [0, this.getValue().length]);
				}
			};
			oInput.addEventDelegate({
				onfocusin: fnFocusIn
			}, oInput);

			// -> do it also for the new "tablet"/"desktop" layout
			oInput = this.getView().byId("orderProductsInputOrderQuantityTablet");
			oInput.addEventDelegate({
				onfocusin: fnFocusIn
			}, oInput);

			// Set model for layout status: visiblity of controls depending on horizontal space (desktop/tablet/smartphone)
			var oModel = new sap.ui.model.json.JSONModel({
				IsLargeLayout: false,
				IsSmallLayout: true
			});
			this.getView().setModel(oModel, "layoutStatus");

			// Adjust columns for "phone" case
			this.adjustColumnsForPhone();

			// Attach an event handler for the "orientation change" event
			sap.ui.Device.orientation.attachHandler(this.onOrientationChange, this);

			// Attach an event handler for the "resize" event
			sap.ui.Device.resize.attachHandler(this.onResize, this);

			// Adjust the screen layout dependent on phone/table/desktop and orientation
			this.adjustScreenLayoutResponsively();
		},

		onProductLinkPress: function(oEvent) {
			var oSource = oEvent.getSource();
			var oLinkDom = jQuery.sap.domById(oSource.getId());
			if (oLinkDom && oLinkDom.blur) {
				// Remove the focus from the link to make sure that the scrollToSelectedItem function works properly after navigating back to list
				oLinkDom.blur();
			}
			var oColListItem = oSource.getParent().getParent();
			this.handleNavigationToListItem(oColListItem);
		},

		handleNavigationToListItem: function(oListItem) {
			if (!oListItem) {
				return;
			}

			// Check if the order quantity is changed in the same user action and cancel the processing if necessary
			if (this.checkOrderQuantityChange()) {
				return;
			}

			var oContext = oListItem.getBindingContext();

			// Select the corresponding item in the table
			this.getProductTable().setSelectedItem(oListItem);
			this.selectListItem(oListItem);

			var sStoreID = oContext.getProperty("StoreID");
			var sProductID = oContext.getProperty("ProductID");

			// Submit the product if there are changes and there is no pending request
			DataChangeUtil.submitProductChanges(sStoreID, sProductID);

			// Navigate to details view
			this.getRouter().navTo("toProductDetails", {
				store: encodeURIComponent(sStoreID),
				product: encodeURIComponent(sProductID)
			});
		},

		onSelectionChange: function(oEvent) {
			if (oEvent.getParameter("selected")) {
				this.selectListItem(oEvent.getParameter("listItem"));
			}
		},

		onItemPress: function(oEvent) {
			this.selectListItem(oEvent.getSource());
		},

		selectListItem: function(oItem) {
			// Check that an item is given
			if (!oItem) {
				return;
			}

			var sProductID = oItem.getBindingContext().getProperty("ProductID");

			if (!this._sProductLiveChange) {
				// Get previously selected store product
				var sPreviouslySelectedProduct = this._oComponent.getSelectedStoreProduct();
				var oProduct = this.getDataManager().getSingleProduct(null, sPreviouslySelectedProduct);
				var oContext = oItem.getBindingContext();
				if (sPreviouslySelectedProduct && oProduct && sPreviouslySelectedProduct !== oContext.getProperty("ProductID")) {
					// A new store product is selected; set the old one to processed in the buffer
					this.getDataManager().setProductProcessed(sPreviouslySelectedProduct);

					// Set the old store product processed in the table (do it manually for performance reasons)
					var oPrevItem = this.getItemByProductID(sPreviouslySelectedProduct);
					if (oPrevItem) {
						this.setItemProcessed(oPrevItem);
					}

					// Submit the changes for the old store product
					DataChangeUtil.submitProductChanges(oContext.getProperty("StoreID"), sPreviouslySelectedProduct);
				}
			}

			// Remember the store product, which is currently selected
			this._oComponent.setSelectedStoreProduct(sProductID);
		},

		setItemProcessed: function(oItem) {
			// Set unread indicator and change the data in the underlying model
			oItem.setUnread(false);
			var oProduct = oItem.getBindingContext().getObject();
			oProduct.IsOrderQuantityProcessed = true;
		},

		onOrientationChange: function(mParams) {
			// Maybe the view layout has to be changed when orientation is switched
			this.adjustScreenLayoutResponsively();
		},

		onResize: function(mParams) {
			// Maybe the view layout has to be changed when screen size reaches/leaves a given threshold
			this.adjustScreenLayoutResponsively();
		},

		adjustScreenLayoutResponsively: function() {

			if (sap.ui.Device.system.phone) {
				// The switch between "smartphone" and "tablet" layout is not relevant for phones
				return;
			}

			if (this._RangeSetRedefinable) {
				var bExceptionsEnabled = this.getView().getModel("Settings").getProperty("/ExceptionsEnabled");
				if (bExceptionsEnabled) {
					this._currentRangeSetName = "OP-Desktop-Exceptions";
					this._RangeSetRedefinable = false;
				} else {
					this._RangeSetRedefinable = bExceptionsEnabled; //false or undefined
				}
			} else if (this._RangeSetRedefinable === undefined) {
				this._currentRangeSetName = "OP-Desktop";
				this._RangeSetRedefinable = true;
			}

			// define custom range set
			if (!sap.ui.Device.media.hasRangeSet(this._currentRangeSetName)) {
				//			var iRangeBorder = 1250;
				var iRangeBorder = bExceptionsEnabled ? 1250 : 1024;
				sap.ui.Device.media.initRangeSet(this._currentRangeSetName, [iRangeBorder], "px", ["small", "large"], true);
			}

			var oModel = this.getView().getModel("layoutStatus");

			if (sap.ui.Device.media.getCurrentRange(this._currentRangeSetName).name !== "large") {
				// Switch to "smartphone" layout when number of effective pixels is lower than defined border
				if (oModel.getProperty("/IsLargeLayout")) {
					oModel.setProperty("/IsLargeLayout", false);
					oModel.setProperty("/IsSmallLayout", true);
				}
			} else {
				// Switch to "tablet/desktop" layout when number of effective pixels is higher than defined border
				if (oModel.getProperty("/IsSmallLayout")) {
					oModel.setProperty("/IsLargeLayout", true);
					oModel.setProperty("/IsSmallLayout", false);
				}
			}
		},

		adjustColumnsForPhone: function() {
			if (!sap.ui.Device.system.phone) {
				// The switch between "smartphone" and "tablet" layout is not relevant for phones
				return;
			}

			// For phones: remove the columns which are intended for tablet/desktop use cases; this saves some performance
			var aColumnIDsTablet = [
				"orderProductsColumnOrderQuantityReviewUrgencyTablet",
				"orderProductsColumnOrderTablet",
				"orderProductsColumnOnPromotion",
				"orderProductsColumnCurrentQuantity",
				"orderProductsColumnExpectedQuantity",
				"orderProductsColumnAllocatedQuantity"
			];
			var aCellIDsTablet = [
				"orderProductsTextUrgencyLabelTablet",
				"orderProductsHBoxOrderQuantityTablet",
				"orderProductsVBoxOnPromotionStatusTablet",
				"orderProductsTextCurrentQuantity",
				"orderProductsTextExpectedQuantity",
				"orderProductsTextPushQuantity"
			];
			var oProductTable = this.getProductTable();
			var oTemplate = oProductTable.getItems()[0];
			var aCellsToRemove = oTemplate.getCells().filter(function(oCell) {
				var sId = oCell.getId();
				for (var i = 0, iLength = aCellIDsTablet.length; i < iLength; i++) {
					if (sId.indexOf(aCellIDsTablet[i]) > -1) {
						return true;
					}
				}
				return false;
			});

			// Remove the cells
			for (var i = 0, iLength = aCellsToRemove.length; i < iLength; i++) {
				oTemplate.removeCell(aCellsToRemove[i]);
				aCellsToRemove[i].destroy();
			}

			// Remove the columns
			var oColumn = null;
			for (i = 0, iLength = aColumnIDsTablet.length; i < iLength; i++) {
				oColumn = this.byId(aColumnIDsTablet[i]);
				oProductTable.removeColumn(oColumn);
				oColumn.destroy();
			}
		},

		createCustomSettings: function() {
			// Default settings
			this._mSettings.DisableInitialFilterFromPers = false;
			this._mSettings.EnableProductExpand = false;

			/**
			 * @ControllerHook [Disable initial filtering] The hook is called once when the app is started. Return true, if the
			 *                 initial filtering (with last selected filter) should be disabled when the app is started
			 * @callback retail.store.orderproducts1.controller.ProductList~extHookGetDisableInitialFiltering
			 * @return {boolean} The indicator, if the initial filtering should be disabled
			 */
			if (this.extHookGetDisableInitialFiltering) {
				this._mSettings.DisableInitialFilterFromPers = this.extHookGetDisableInitialFiltering();
			}

			/**
			 * @ControllerHook [Enable direct product detail read] The hook is called once when the app is started. Return true,
			 *                 if the app should directly read all product data in the list screen. This leads to longer
			 *                 response times for the product table, but the up/down buttons in the detail screen will work
			 *                 faster because no data has to be read again.
			 * @callback retail.store.orderproducts1.controller.ProductList~extHookGetEnableProductExpand
			 * @return {boolean} The indicator, if the direct product detail read should be enabled
			 */
			if (this.extHookGetEnableProductExpand) {
				this._mSettings.EnableProductExpand = this.extHookGetEnableProductExpand();
			}
		}

	});
});