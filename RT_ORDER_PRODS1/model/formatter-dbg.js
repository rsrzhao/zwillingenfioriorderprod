/*
 * Copyright (C) 2009-2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define([
	"sap/ui/core/format/DateFormat",
	"sap/ui/core/format/NumberFormat",
	"sap/retail/store/lib/reuses1/util/Formatter"
], function(DateFormat, NumberFormat, Reuses1Formatter) {
	"use strict";

	var oFormatter = {

		_oResourceBundle: null,

		init: function(oComponent, oResourceBundle) {
			this._oComponent = oComponent;
			this._oResourceBundle = oResourceBundle;
		},

		_formatQuantityWithDate: function(fQuantityInOrderUoM, sOrderUoM, iDays, sDeliveryDate, sSalesUoM, fSalesUoMPerOrderUoMNumerator,
			fSalesUoMPerOrderUoMDenominator) {

			var sFormattedQuantity = "";

			if (fQuantityInOrderUoM >= 0 && fSalesUoMPerOrderUoMNumerator && fSalesUoMPerOrderUoMDenominator && sSalesUoM && sOrderUoM) {

				if ((fQuantityInOrderUoM * 1) === 0) { // Convert potential string into number by "* 1"
					sFormattedQuantity = Reuses1Formatter.formatQuantityMaxOneDigitWithUnit("0", sSalesUoM);
					return sFormattedQuantity;
				}

				var fQuantityInSalesUoM = oFormatter._convertQuantityFromOrderUoMToSalesUoM(fQuantityInOrderUoM,
					fSalesUoMPerOrderUoMNumerator, fSalesUoMPerOrderUoMDenominator);
				var sQuantityInOrderUoM = Reuses1Formatter.formatQuantityMaxOneDigitWithUnit(fQuantityInOrderUoM,
					sOrderUoM);
				var sQuantityInSalesUoM = Reuses1Formatter.formatQuantityMaxOneDigitWithUnit(fQuantityInSalesUoM,
					sSalesUoM);

				if (iDays < 5) {
					// Just display the number of days until delivery
					if (sSalesUoM !== sOrderUoM) {
						if (iDays === 1) {
							// Pattern: 1 KT (24 PC) in 1 day
							sFormattedQuantity = oFormatter._oResourceBundle.getText("DETAIL_QTY_VALUE_W_SALESUOM_WO_DATE_1_DAY", [
								sQuantityInOrderUoM, sQuantityInSalesUoM
							]);
						} else {
							// Pattern: 1 KT (24 PC) in 2 days
							sFormattedQuantity = oFormatter._oResourceBundle.getText("DETAIL_QTY_VALUE_W_SALESUOM_WO_DATE", [sQuantityInOrderUoM,
								sQuantityInSalesUoM, iDays
							]);
						}
					} else {
						if (iDays === 1) {
							// Pattern: 1 KT in 1 day
							sFormattedQuantity = oFormatter._oResourceBundle.getText("DETAIL_QTY_VALUE_WO_SALESUOM_WO_DATE_1_DAY", [
								sQuantityInOrderUoM
							]);
						} else {
							// Pattern: 1 KT in 2 days
							sFormattedQuantity = oFormatter._oResourceBundle.getText("DETAIL_QTY_VALUE_WO_SALESUOM_WO_DATE", [
								sQuantityInOrderUoM, iDays
							]);
						}
					}
				} else if (sDeliveryDate) {
					// Additionally display the delivery date
					var oDateFormatWeekday = DateFormat.getDateInstance({
						pattern: "EEE",
						UTC: true // Pure dates must always be treated as UTC timestamps with time 00:00
					});
					var sDateFormatted = oFormatter.formatDate(sDeliveryDate, "short");
					var sWeekday = oDateFormatWeekday.format(sDeliveryDate, true);

					if (sSalesUoM !== sOrderUoM) {
						// Pattern: 1 KT (24 PC) in 6 days (Mon, 28.04.14)
						sFormattedQuantity = oFormatter._oResourceBundle.getText("DETAIL_QTY_VALUE_W_SALESUOM_W_DATE", [sQuantityInOrderUoM,
							sQuantityInSalesUoM, iDays, sWeekday, sDateFormatted
						]);
					} else {
						// Pattern: 1 KT in 6 days (Mon, 28.04.14)
						sFormattedQuantity = oFormatter._oResourceBundle.getText("DETAIL_QTY_VALUE_WO_SALESUOM_W_DATE", [sQuantityInOrderUoM,
							iDays, sWeekday, sDateFormatted
						]);
					}
				}
			}

			return sFormattedQuantity;
		},

		_convertQuantityFromOrderUoMToSalesUoM: function(fQuantityInOrderUoM, fSalesUoMPerOrderUoMNumerator, fSalesUoMPerOrderUoMDenominator) {
			return fQuantityInOrderUoM * fSalesUoMPerOrderUoMNumerator / fSalesUoMPerOrderUoMDenominator;
		},

		_convertQuantityFromSalesUoMToOrderUoM: function(fQuantityInSalesUoM, fSalesUoMPerOrderUoMNumerator, fSalesUoMPerOrderUoMDenominator) {
			return fQuantityInSalesUoM * fSalesUoMPerOrderUoMDenominator / fSalesUoMPerOrderUoMNumerator;
		},

		_getSalesUoMToOrderUoMConversionFactor: function(fSalesUoMPerOrderUoMNumerator, fSalesUoMPerOrderUoMDenominator) {
			return fSalesUoMPerOrderUoMNumerator / fSalesUoMPerOrderUoMDenominator;
		},

		isPastTodayFuture: function(sDate) {
			var sPastTodayFuture = "";
			if (sDate) {
				// Make sure to have a JS date
				var dDate = oFormatter.getJSDate(sDate);

				// Get today's date
				var dToday = oFormatter._oComponent.getCurrentDate();

				// Compare the given date with today's date
				var sCompare = oFormatter.dateIsEarlierEqualLater(dDate, dToday, true, false);
				if (sCompare === "Earlier") {
					sPastTodayFuture = "P";
				} else if (sCompare === "Equal") {
					sPastTodayFuture = "T";
				} else if (sCompare === "Later") {
					sPastTodayFuture = "F";
				}
			}
			return sPastTodayFuture;
		},

		dateIsEarlierEqualLater: function(dDateOne, dDateTwo, bDateOneUseUTC, bDateTwoUseUTC) {
			var iDateOne = bDateOneUseUTC ? dDateOne.getUTCDate() : dDateOne.getDate();
			var iMonthOne = bDateOneUseUTC ? dDateOne.getUTCMonth() : dDateOne.getMonth();
			var iYearOne = bDateOneUseUTC ? dDateOne.getUTCFullYear() : dDateOne.getFullYear();

			var iDateTwo = bDateTwoUseUTC ? dDateTwo.getUTCDate() : dDateTwo.getDate();
			var iMonthTwo = bDateTwoUseUTC ? dDateTwo.getUTCMonth() : dDateTwo.getMonth();
			var iYearTwo = bDateTwoUseUTC ? dDateTwo.getUTCFullYear() : dDateTwo.getFullYear();

			var sResult = "";
			if (iYearOne < iYearTwo) {
				sResult = "Earlier";
			} else if (iYearOne > iYearTwo) {
				sResult = "Later";
			} else {
				// Same year
				if (iMonthOne < iMonthTwo) {
					sResult = "Earlier";
				} else if (iMonthOne > iMonthTwo) {
					sResult = "Later";
				} else {
					// Same month
					if (iDateOne < iDateTwo) {
						sResult = "Earlier";
					} else if (iDateOne > iDateTwo) {
						sResult = "Later";
					} else {
						// Dates are equal
						sResult = "Equal";
					}
				}
			}
			return sResult;
		},

		getJSDate: function(sDate) {
			var dDate;
			if (typeof sDate === "object") {
				// A javascript date is already given
				dDate = sDate;
			} else {
				// Create a javascript date for given date
				dDate = new Date(sDate.match(/\d+/)[0] * 1);
			}
			return dDate;
		},

		formatIsEndOfCoverage: function(sDate, sEndOfCoverage) {
			var bIsEndOfCoverage = false;
			if (sDate && sEndOfCoverage) {
				var dDate = oFormatter.getJSDate(sDate);
				var dEndOfCoverage = oFormatter.getJSDate(sEndOfCoverage);
				if (dDate.getUTCDate() === dEndOfCoverage.getUTCDate() &&
					dDate.getUTCMonth() === dEndOfCoverage.getUTCMonth() &&
					dDate.getUTCFullYear() === dEndOfCoverage.getUTCFullYear()) {
					bIsEndOfCoverage = true;
				}
			}
			return bIsEndOfCoverage;
		},

		formatPostalCodeCity: function(sPostalCode, sCity) {
			return sPostalCode + " " + sCity;
		},

		formatPushQuantity: function(fTotalPushQuantityInSalesUoM, fNextPushQuantityInSalesUoM, sOrderUoM, iDays, sDeliveryDate, sSalesUoM,
			fSalesUoMPerOrderUoMNumerator, fSalesUoMPerOrderUoMDenominator) {
			var sPushQuantity = "";
			var fTotalPushQuantityInOrderUoM = oFormatter._convertQuantityFromSalesUoMToOrderUoM(
				fTotalPushQuantityInSalesUoM, fSalesUoMPerOrderUoMNumerator,
				fSalesUoMPerOrderUoMDenominator);

			var fNextPushQuantityInOrderUoM = oFormatter._convertQuantityFromSalesUoMToOrderUoM(
				fNextPushQuantityInSalesUoM, fSalesUoMPerOrderUoMNumerator,
				fSalesUoMPerOrderUoMDenominator);

			var sQuantityWithDate = oFormatter._formatQuantityWithDate(fNextPushQuantityInOrderUoM, sOrderUoM,
				iDays, sDeliveryDate, sSalesUoM, fSalesUoMPerOrderUoMNumerator,
				fSalesUoMPerOrderUoMDenominator);
			if (sQuantityWithDate && fTotalPushQuantityInSalesUoM && fNextPushQuantityInSalesUoM && fTotalPushQuantityInSalesUoM !==
				fNextPushQuantityInSalesUoM) {
				var sQuantityOrderUoM = Reuses1Formatter.formatQuantityMaxOneDigitWithUnit(fTotalPushQuantityInOrderUoM,
					sOrderUoM);
				var sQuantitySalesUoM = Reuses1Formatter.formatQuantityMaxOneDigitWithUnit(fTotalPushQuantityInSalesUoM,
					sSalesUoM);
				if (sOrderUoM !== sSalesUoM) {
					sPushQuantity = sQuantityWithDate + ", " + oFormatter._oResourceBundle.getText("DETAIL_TOTAL_QTY_VALUE_W_SALESUOM", [
						sQuantityOrderUoM, sQuantitySalesUoM
					]);
				} else {
					sPushQuantity = sQuantityWithDate + ", " + oFormatter._oResourceBundle.getText("DETAIL_TOTAL_QTY_VALUE_WO_SALESUOM", [
						sQuantityOrderUoM
					]);
				}
			} else {
				sPushQuantity = sQuantityWithDate;
			}
			return sPushQuantity;
		},

		formatOrderQuantity: function(fOrderQuantity, sOrderUoM, iDays, sDeliveryDate, sSalesUoM, fSalesUoMPerOrderUoMNumerator,
			fSalesUoMPerOrderUoMDenominator) {
			return oFormatter
				._formatQuantityWithDate(fOrderQuantity, sOrderUoM, iDays, sDeliveryDate, sSalesUoM, fSalesUoMPerOrderUoMNumerator,
					fSalesUoMPerOrderUoMDenominator);
		},

		formatShelfLife: function(iShelfLife) {
			var sShelfLife = "";
			if (iShelfLife && iShelfLife > 0) {
				if (iShelfLife === 1) {
					sShelfLife = oFormatter._oResourceBundle.getText("DETAIL_EXPIRATION_VALUE_1_DAY");
				} else {
					sShelfLife = oFormatter._oResourceBundle.getText("DETAIL_EXPIRATION_VALUE", [iShelfLife]);
				}
			}
			return sShelfLife;
		},

		formatShelfLifeVisible: function(iShelfLife) {
			var bVisible = false;
			if (iShelfLife && iShelfLife > 0) {
				bVisible = true;
			}
			return bVisible;
		},

		formatPrevOrderQuantity: function(fOrderQuantity, sOrderUoM, fSalesUoMPerOrderUoMNumerator, fSalesUoMPerOrderUoMDenominator) {
			var sPrevQuantity = "";
			if (fSalesUoMPerOrderUoMNumerator && fSalesUoMPerOrderUoMDenominator && sOrderUoM && fOrderQuantity) {
				var sFormattedQuantity = Reuses1Formatter.formatQuantityMaxOneDigitWithUnit(fOrderQuantity, sOrderUoM);
				var sLabelText = oFormatter._oResourceBundle.getText("DETAIL_PREV_ORDER_QUANTITY");

				if (sLabelText.length + sFormattedQuantity.length > 16 && sap.ui.Device.media.getCurrentRange(sap.ui.Device.media.RANGESETS.SAP_STANDARD)
					.name === "Phone") {
					// Display the previous order quantity below the label
					sPrevQuantity = sLabelText + "\n" + sFormattedQuantity;
				} else {
					// Display the previous order quantity in one line with the label
					sPrevQuantity = sLabelText + " " + sFormattedQuantity;
				}
			}

			return sPrevQuantity;
		},

		formatSalesUoMPerOrderUoM: function(fSalesUoMPerOrderUoMNumerator, fSalesUoMPerOrderUoMDenominator, sSalesUoM, sOrderUoM) {
			var sOrderUoMConversion = "";

			if (fSalesUoMPerOrderUoMNumerator && fSalesUoMPerOrderUoMDenominator && sSalesUoM && sOrderUoM) {
				var fSalesUoMToOrderUoMConversionFactor = oFormatter._getSalesUoMToOrderUoMConversionFactor(
					fSalesUoMPerOrderUoMNumerator, fSalesUoMPerOrderUoMDenominator);
				var sSalesUoMPerOrderUoM = Reuses1Formatter.formatQuantityMaxOneDigit(fSalesUoMToOrderUoMConversionFactor);

				if (sSalesUoM === sOrderUoM) {
					// Just display the Order UoM
					sOrderUoMConversion = sOrderUoM;
				} else {
					// Display the Order UoM with conversion to Sales UoM in brackets
					sOrderUoMConversion = oFormatter._oResourceBundle.getText("DETAIL_SALES_PER_ORDER", [sOrderUoM, sSalesUoMPerOrderUoM,
						sSalesUoM
					]);
				}
			}

			return sOrderUoMConversion;
		},

		formatPromoIconVisibility: function(bIsOnPromotionToday) {
			// This formatter is needed in order to hide the icon as long as bIsOnPromotion is undefined
			var bVisible = false;
			if (bIsOnPromotionToday) {
				bVisible = true;
			}
			return bVisible;
		},

		formatUnreadItem: function(bIsOrderQuantityProcessed) {
			return !bIsOrderQuantityProcessed;
		},

		formatReviewUrgencyVisible: function(bExceptionsEnabled, bIsSmallLayout, sOrderQuantityReviewUrgency) {
			var bReviewUrgencyVisible = bExceptionsEnabled && bIsSmallLayout; // not visible on phones 

			if (bReviewUrgencyVisible && !((sOrderQuantityReviewUrgency === "1") || (sOrderQuantityReviewUrgency === "2"))) {
				bReviewUrgencyVisible = false;
			}

			return bReviewUrgencyVisible;
		},

		formatReviewUrgencyText: function(sOrderQuantityReviewUrgency) {
			var sReviewUrgencyText = "";

			if (sOrderQuantityReviewUrgency === "1") {
				sReviewUrgencyText = oFormatter._oResourceBundle.getText("REVIEW_URGENCY_TEXT_MUST");
			} else if (sOrderQuantityReviewUrgency === "2") {
				sReviewUrgencyText = oFormatter._oResourceBundle.getText("REVIEW_URGENCY_TEXT_SHOULD");
			}

			return sReviewUrgencyText;
		},

		formatReviewUrgencyTextTablet: function(sOrderQuantityReviewUrgency) {
			var sReviewUrgencyText = "";

			if (sOrderQuantityReviewUrgency === "1" || sOrderQuantityReviewUrgency === "2") {
				sReviewUrgencyText = oFormatter.formatReviewUrgencyText(sOrderQuantityReviewUrgency);
			} else {
				sReviewUrgencyText = oFormatter._oResourceBundle.getText("REVIEW_URGENCY_TEXT_NONE");
			}

			return sReviewUrgencyText;
		},

		formatExceptionPriorityText: function(sExceptionPriority) {
			var sExceptionPriorityText = "";

			if (sExceptionPriority === "1") {
				sExceptionPriorityText = oFormatter._oResourceBundle.getText("EXCEPTION_PRIORITY_HIGH_TEXT");
			} else if (sExceptionPriority === "2") {
				sExceptionPriorityText = oFormatter._oResourceBundle.getText("EXCEPTION_PRIORITY_MEDIUM_TEXT");
			}

			return sExceptionPriorityText;
		},

		formatExceptionPriorityAsStatus: function(sExceptionPriority) {

			if (sExceptionPriority === "1") {
				return "Error";
			} else if (sExceptionPriority === "2") {
				return "Warning";
			} else {
				return "None";
			}
		},

		formatExceptionsDetailsPanelVisible: function(aExceptions) {
			if (aExceptions && aExceptions.length > 0) {
				return true;
			} else {
				return false;
			}
		},

		formatOnPromotionTextSingleLineVisible: function(bOnPromotionThisWeek, bOnPromotionNextWeek) {
			if (bOnPromotionThisWeek && bOnPromotionNextWeek) {
				return false;
			} else {
				return true;
			}
		},

		formatOnPromotionTextTwoLinesVisible: function(bOnPromotionThisWeek, bOnPromotionNextWeek) {
			if (bOnPromotionThisWeek && bOnPromotionNextWeek) {
				return true;
			} else {
				return false;
			}
		},

		formatOnPromotionText: function(bOnPromotionThisWeek, bOnPromotionNextWeek) {
			var sOnPromotionText = "";

			if ((bOnPromotionThisWeek === false) && (bOnPromotionNextWeek === false)) {
				sOnPromotionText = oFormatter._oResourceBundle.getText("PROMOTION_TEXT_NONE");
			} else if ((bOnPromotionThisWeek === true) && (bOnPromotionNextWeek === true)) {
				sOnPromotionText = oFormatter._oResourceBundle.getText("PROMOTION_TEXT_BOTH_WEEKS");
			} else if ((bOnPromotionThisWeek === true) && (bOnPromotionNextWeek === false)) {
				sOnPromotionText = oFormatter._oResourceBundle.getText("PROMOTION_TEXT_THIS_WEEK");
			} else if ((bOnPromotionThisWeek === false) && (bOnPromotionNextWeek === true)) {
				sOnPromotionText = oFormatter._oResourceBundle.getText("PROMOTION_TEXT_NEXT_WEEK");
			}

			return sOnPromotionText;
		},

		formatOnPromotionTextState: function(bOnPromotionThisWeek, bOnPromotionNextWeek) {
			var sOnPromotionTextState = "";

			if ((bOnPromotionThisWeek === true) || (bOnPromotionNextWeek === true)) {
				sOnPromotionTextState = "Success";
			} else {
				sOnPromotionTextState = "None";
			}

			return sOnPromotionTextState;
		},

		formatReviewUrgencyTextState: function(iReviewUrgency) {
			var sReviewUrgencyTextState = "None";

			if (iReviewUrgency == 1) {
				sReviewUrgencyTextState = "Error";
			}

			if (iReviewUrgency == 2) {
				sReviewUrgencyTextState = "Warning";
			}

			return sReviewUrgencyTextState;
		},

		formatOnPromotionTextTablet: function(bOnPromotionThisWeek, bOnPromotionNextWeek) {
			var sOnPromotionText = "";

			if ((bOnPromotionThisWeek === false) && (bOnPromotionNextWeek === false)) {
				sOnPromotionText = oFormatter._oResourceBundle.getText("PROMOTION_TEXT_NONE_TABLET");
			} else if ((bOnPromotionThisWeek === true) && (bOnPromotionNextWeek === true)) {
				sOnPromotionText = oFormatter._oResourceBundle.getText("PROMOTION_TEXT_THIS_WEEK_TABLET") + "\n" + oFormatter._oResourceBundle.getText(
					"PROMOTION_TEXT_NEXT_WEEK_TABLET");
			} else if ((bOnPromotionThisWeek === true) && (bOnPromotionNextWeek === false)) {
				sOnPromotionText = oFormatter._oResourceBundle.getText("PROMOTION_TEXT_THIS_WEEK_TABLET");
			} else if ((bOnPromotionThisWeek === false) && (bOnPromotionNextWeek === true)) {
				sOnPromotionText = oFormatter._oResourceBundle.getText("PROMOTION_TEXT_NEXT_WEEK_TABLET");
			}

			return sOnPromotionText;
		},

		formatOnPromotionTextCurrentWeekTablet: function(bOnPromotionThisWeek) {
			return oFormatter._oResourceBundle.getText("PROMOTION_TEXT_THIS_WEEK_TABLET");
		},

		formatOnPromotionTextNextWeekTablet: function(bOnPromotionThisWeek) {
			return oFormatter._oResourceBundle.getText("PROMOTION_TEXT_NEXT_WEEK_TABLET");
		},

		formatUOMConversion: function(fSalesUoMPerOrderUoMNumerator, fSalesUoMPerOrderUoMDenominator, sOrderUoMName) {
			var sUOMConversion = "";
			if (fSalesUoMPerOrderUoMNumerator && fSalesUoMPerOrderUoMDenominator && sOrderUoMName) {
				var fSalesUoMToOrderUoMConversionFactor = oFormatter._getSalesUoMToOrderUoMConversionFactor(
					fSalesUoMPerOrderUoMNumerator, fSalesUoMPerOrderUoMDenominator);
				var sSalesUoMPerOrderUoM = Reuses1Formatter.formatQuantityMaxOneDigit(fSalesUoMToOrderUoMConversionFactor);
				sUOMConversion = oFormatter._oResourceBundle.getText("LIST_UOM_CONVERSION", [sSalesUoMPerOrderUoM, sOrderUoMName]);
			}
			return sUOMConversion;
		},

		formatDate: function(sDate, sStyle) {
			var sDateFormatted = "";
			if (sDate) {
				// Get instance of date format class with given style
				var oDateFormat = DateFormat.getDateInstance({
					style: sStyle,
					UTC: true // Pure dates must always be treated as UTC timestamps with time 00:00
				});

				// Format the date according to user settings
				sDateFormatted = oDateFormat.format(sDate);
			}
			return sDateFormatted;
		},

		formatCalendarTitle: function(sSalesQuantityUnit) {
			var sUnit = " ";
			if (sSalesQuantityUnit) {
				sUnit = sSalesQuantityUnit;
			}
			return oFormatter._oResourceBundle.getText("DETAIL_CALENDAR_TITLE", [sUnit]);
		},

		formatCalendarWeek: function(sFirstDate, sLastDate) {
			var sCalendarWeek = "";
			if (sFirstDate && sLastDate) {
				// Get instance of date format class with "dd" pattern
				var oDateFormat = DateFormat.getDateInstance({
					pattern: "dd",
					UTC: true // Pure dates must always be treated as UTC timestamps with time 00:00
				});

				var sFirstDay = oDateFormat.format(sFirstDate);
				var sLastDay = oDateFormat.format(sLastDate);
				sCalendarWeek = oFormatter._oResourceBundle.getText("DETAIL_DATE_OF_DAY", [sFirstDay]) + "\n\u2013\n" +
					oFormatter._oResourceBundle.getText("DETAIL_DATE_OF_DAY", [sLastDay]);
			}
			return sCalendarWeek;
		},

		formatSalesGoodsReceiptQuantity: function(sDate, fSalesQuantity, fGoodsReceiptQuantity) {
			var fQuantity = 0;
			if (oFormatter.isPastTodayFuture(sDate) === "P") {
				fQuantity = fSalesQuantity;
			} else {
				fQuantity = fGoodsReceiptQuantity;
			}

			return oFormatter.formatQuantityInCalendar(fQuantity);
		},

		formatQuantityInCalendar: function(fQuantity) {
			var sFormattedQuantity = "";
			if (fQuantity > 0) {
				sFormattedQuantity = Reuses1Formatter.formatQuantityMaxOneDigit(fQuantity);
			} else {
				sFormattedQuantity = "\u2013"; // unicode for "en dash"
			}
			return sFormattedQuantity;
		},

		formatPromotionNameWithID: function(sPromotionID, sPromotionName) {
			var sName = "";
			if (sPromotionID && sPromotionName) {
				sName = sPromotionName + " (" + sPromotionID + ")";
			} else if (sPromotionID) {
				sName = sPromotionID;
			}
			return sName;
		},

		formatFilterTextWithKey: function(sText, sKey, sFilterID) {
			var sFormattedText = "";
			if (sText && sKey && (sFilterID !== "Attributes")) {
				// Display text with key in brackets
				sFormattedText = sText + " (" + sKey + ")";
			} else if (sText) {
				// Display only text
				sFormattedText = sText;
			} else if (sKey) {
				// Display only key
				sFormattedText = sKey;
			}
			return sFormattedText;
		},

		formatInputValueState: function(bIsChangedOrderQuantityPosted, bIsOrderQuantityChanged) {
			var sValueState = "None";

			if (bIsChangedOrderQuantityPosted !== undefined && bIsChangedOrderQuantityPosted !== null && bIsOrderQuantityChanged !== undefined &&
				bIsOrderQuantityChanged !== null) {
				if (bIsOrderQuantityChanged && bIsChangedOrderQuantityPosted) {
					// Order Quantity was changed manually and successfully posted
					sValueState = "Success";
				} else if (bIsOrderQuantityChanged && !bIsChangedOrderQuantityPosted) {
					// Order Quantity was changed manually, but not posted due to an error situation
					sValueState = "Error";
				}
			}

			return sValueState;
		},

		formatOrderUOMWithConversion: function(fSalesUoMPerOrderUoMNumerator, fSalesUoMPerOrderUoMDenominator, fOrderQuantity,
			sOrderQuantityUnitCode, sSalesQuantityUnitCode, sOrderQuantityUnitName) {
			var sOrderUOMWithConversion = "";
			if (fSalesUoMPerOrderUoMNumerator && fSalesUoMPerOrderUoMDenominator && sOrderQuantityUnitCode && sSalesQuantityUnitCode &&
				sOrderQuantityUnitName) {
				if (sOrderQuantityUnitCode === sSalesQuantityUnitCode) {
					sOrderUOMWithConversion = sOrderQuantityUnitName;
				} else {
					var sOrderQuantityConversionAndUnit = oFormatter.formatOrderQuantityConversionAndUnit(fOrderQuantity,
						fSalesUoMPerOrderUoMNumerator,
						fSalesUoMPerOrderUoMDenominator, sSalesQuantityUnitCode);
					sOrderUOMWithConversion = sOrderQuantityUnitName + " (" + sOrderQuantityConversionAndUnit + ")";
				}
			}
			return sOrderUOMWithConversion;
		},

		formatOrderQuantityConversionAndUnit: function(fQuantityInOrderUoM, fSalesUoMPerOrderUoMNumerator, fSalesUoMPerOrderUoMDenominator,
			sSalesUoM) {
			var sOrderQuantityWithUnit = "";
			var sFormattedQuantity = oFormatter.formatOrderQuantityConversion(fQuantityInOrderUoM,
				fSalesUoMPerOrderUoMNumerator, fSalesUoMPerOrderUoMDenominator);

			if (sFormattedQuantity) {
				sOrderQuantityWithUnit = sFormattedQuantity + " " + sSalesUoM;
			}
			return sOrderQuantityWithUnit;
		},

		formatPrice: function(sPrice, sCurrency) {
			var sFormattedPrice = "";
			if (sPrice && sCurrency) {
				var oFormatCurrency = NumberFormat.getCurrencyInstance({
					showMeasure: false
				});
				sFormattedPrice = oFormatCurrency.format(sPrice, sCurrency);
			}
			return sFormattedPrice;
		},

		formatPriceWithCurrency: function(sPrice, sCurrency) {
			var sFormattedPrice = "";
			if (sPrice && sCurrency) {
				var oFormatCurrency = NumberFormat.getCurrencyInstance({
					currencyCode: false
				});
				sFormattedPrice = oFormatCurrency.format(sPrice, sCurrency);
			}
			return sFormattedPrice;
		},

		formatPriceWithCurrencyForPromotion: function(sPrice, sCurrency, fSalesPriceQuantity, sSalesQuantityUnitCode) {
			var sFormattedPrice = "";

			if (sCurrency) {
				var sPriceWithCurr = oFormatter.formatPriceWithCurrency(sPrice, sCurrency);
				if (fSalesPriceQuantity && sSalesQuantityUnitCode) {
					var sQuantityWithUnit = Reuses1Formatter.formatQuantityMaxOneDigitWithUnit(fSalesPriceQuantity,
						sSalesQuantityUnitCode);
					sFormattedPrice = sPriceWithCurr + " / " + sQuantityWithUnit;
				} else {
					sFormattedPrice = sPriceWithCurr;
				}
			}
			return sFormattedPrice;
		},

		formatOrderQuantityConversion: function(fQuantityInOrderUoM, fSalesUoMPerOrderUoMNumerator, fSalesUoMPerOrderUoMDenominator) {
			var sFormattedQuantity = "";
			if (fSalesUoMPerOrderUoMNumerator && fSalesUoMPerOrderUoMDenominator) {
				var fQuantityInSalesUoM = oFormatter._convertQuantityFromOrderUoMToSalesUoM(fQuantityInOrderUoM,
					fSalesUoMPerOrderUoMNumerator, fSalesUoMPerOrderUoMDenominator);
				sFormattedQuantity = Reuses1Formatter.formatQuantityMaxOneDigit(fQuantityInSalesUoM);
			}
			return sFormattedQuantity;
		},

		formatMerchandiseCategory: function(sMerchCat, sMerchCatName) {
			var sText = "";
			if (sMerchCatName) {
				sText = sMerchCatName;
			} else if (sMerchCat) {
				sText = sMerchCat;
			}
			return sText;
		},

		formatPricingDetailsFormVisible: function(aPromotions) {
			if (aPromotions && aPromotions.length > 0) {
				return true;
			} else {
				return false;
			}
		},

		formatRangeOfCoverage: function(iRangeOfCoverageInDays, sEndOfCoverage) {
			var sText = "";
			if (typeof iRangeOfCoverageInDays === "number") {
				if (iRangeOfCoverageInDays < 5) {
					// Just display the number of days
					if (iRangeOfCoverageInDays === 1) {
						sText = oFormatter._oResourceBundle.getText("DETAIL_RANGE_OF_COVERAGE_WO_DATE_1_DAY");
					} else {
						sText = oFormatter._oResourceBundle.getText("DETAIL_RANGE_OF_COVERAGE_WO_DATE", [iRangeOfCoverageInDays]);
					}
				} else if (sEndOfCoverage) {
					// Additionally display the end of coverage date
					var oDateFormatWeekday = DateFormat.getDateInstance({
						pattern: "EEE",
						UTC: true // Pure dates must always be treated as UTC timestamps with time 00:00
					});
					var sDateFormatted = oFormatter.formatDate(sEndOfCoverage, "short");
					var sWeekday = oDateFormatWeekday.format(sEndOfCoverage);
					sText = oFormatter._oResourceBundle.getText("DETAIL_RANGE_OF_COVERAGE_W_DATE", [iRangeOfCoverageInDays, sWeekday, sDateFormatted]);
				}
			}
			return sText;
		}

	};

	return oFormatter;

});