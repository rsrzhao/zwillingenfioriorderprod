/*
 * Copyright (C) 2009-2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define([
	"sap/ui/base/Object",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"sap/ui/core/BusyIndicator",
	"sap/retail/store/lib/reuses1/util/TextUtil",
	"retail/store/orderproducts1/utils/DataChangeUtil",
	"retail/store/orderproducts1/utils/ScanUtil"
], function(Object, MessageBox, MessageToast, BusyIndicator, Reuses1TextUtil, DataChangeUtil, ScanUtil) {
	"use strict";

	var DataManager = Object.extend("retail.store.orderproducts1.model.DataManager", {

		constructor: function(oComponent, oDataModel, oResourceBundle, oBarcodeScanHandler) {
			// Initialize the class variables for the data manager
			this._oComponent = null;
			this._oDataModel = null;
			this._oResourceBundle = null;
			this._oBarcodeScanHandler = null;
			this._oProductTable = null;
			this._aProducts = [];
			this._aProductRefreshCallbacks = [];
			this._iGrowingThreshold = 10;
			this._iNumberOfPreloadPages = 1;
			this._iInlineCount = 0;
			this._bNextPackageBackgroundPending = false;
			this._bPrevPackageBackgroundPending = false;
			this._oNextPackageBackgroundDeferred = null;
			this._oPrevPackageBackgroundDeferred = null;
			this._mPendingProductRequests = {};
			this._mPendingProductDetailBGRequests = {};

			this._sStoreID = "";
			this._oFilter = null;
			this._sSearch = "";
			this._mOptions = null;

			this._aFailedOperations = [];
			this._sCurrentTimeOutID = "";
			this._iBatchLimit = 10;
			this._iDelayTime = 5000;
			this._bPromptedOfflineMessage = false;

			this._oComponent = oComponent;
			this._oDataModel = oDataModel;
			this._oResourceBundle = oResourceBundle;
			this._oBarcodeScanHandler = oBarcodeScanHandler;
		},

		setProductTable: function(oProductTable) {
			this._oProductTable = oProductTable;
		},

		registerProductRefresh: function(fnCallback) {
			if (typeof fnCallback === "function") {
				this._aProductRefreshCallbacks.push(fnCallback);
			}
		},

		triggerProductRefresh: function(aProductIDs) {
			jQuery.each(this._aProductRefreshCallbacks, function(iIndex, fnCallback) {
				fnCallback(aProductIDs);
			});
		},

		onRequestSent: function(bNoBusyDialog, bNoScanBlocking, bOnlyProdTableBusy) {
			// Busy dialog handling
			if (!bNoBusyDialog) {
				if (bOnlyProdTableBusy) {
					this._oProductTable.setBusy(true);
				} else {
					BusyIndicator.show();
				}
			}

			if (!bNoScanBlocking) {
				this._oBarcodeScanHandler.lockBarcodeScanHandling();
			}
		},

		onRequestCompleted: function(bNoBusyDialog, bNoScanBlocking, bOnlyProdTableBusy) {
			// Busy dialog handling
			if (!bNoBusyDialog) {
				if (bOnlyProdTableBusy) {
					this._oProductTable.setBusy(false);
				} else {
					BusyIndicator.hide();
				}
			}

			if (!bNoScanBlocking) {
				this._oBarcodeScanHandler.unlockBarcodeScanHandling();
			}
		},

		readSingleStore: function(sStoreID, fnSuccess, fnError) {
			var oModel = this._oDataModel;
			var sPath = "";
			if (sStoreID) {
				sPath = "/Stores('" + encodeURIComponent(sStoreID) + "')";
			} else {
				sPath = "/Stores('')";
			}

			var sExpand = "";
			var mUrlParameters = {};
			sExpand = this.addExpandOption(sExpand, "WeekDisplay");
			sExpand = this.addExpandOption(sExpand, "Filters/UserDefaultFilterValues");
			sExpand = this.addExpandOption(sExpand, "Settings");

			if (sExpand) {
				mUrlParameters["$expand"] = sExpand;
			}

			// Set ID for the deferred Group
			//oModel.setDeferredGroups(["singleStore"]);

			oModel.metadataLoaded().then(function() {
				this.onRequestSent();
				oModel.read(sPath, {
					//groupId: "singleStore",
					success: jQuery.proxy(function(oData, oResponse) {
						this.onRequestCompleted();
						if (typeof fnSuccess === "function") {
							fnSuccess(oData, oResponse);
						}
					}, this),
					error: jQuery.proxy(function(oError) {
						this.onRequestCompleted();
						if (typeof fnError === "function") {
							fnError(oError);
						}
					}, this),
					urlParameters: mUrlParameters
				});
				/*oModel.submitChanges({
					groupId: "singleStore"
				});*/
			}.bind(this));

		},

		readFiltersForStore: function(sStoreID, fnSuccess, fnError) {
			var oModel = this._oDataModel;
			var sPath = "";
			if (sStoreID) {
				sPath = "/Stores('" + encodeURIComponent(sStoreID) + "')/Filters";
			} else {
				sPath = "/Stores('')/Filters";
			}

			// Set ID for the deferred Group
			oModel.setDeferredGroups(["filtersForStore"]);

			oModel.read(sPath, {
				groupId: "filtersForStore",
				urlParameters: {
					"$expand": "FilterValues"
				},
				success: jQuery.proxy(function(oData, oResponse) {
					this.onRequestCompleted(true, true);
					if (typeof fnSuccess === "function") {
						fnSuccess(oData, oResponse);
					}
				}, this),
				error: jQuery.proxy(function(oError) {
					this.onRequestCompleted(true, true);
					if (typeof fnError === "function") {
						fnError(oError);
					}
				}, this)
			});

			this.onRequestSent(true, true);
			oModel.submitChanges({
				groupId: "filtersForStore"
			});
		},

		readStoreWeekDisplayData: function(sStoreID, fnSuccess, fnError) {
			var sPath = "/StoreWeekDisplays('" + encodeURIComponent(sStoreID) + "')";

			this.onRequestSent();
			this._oDataModel.read(sPath, {
				success: jQuery.proxy(function(oData, oResponse) {
					this.onRequestCompleted();
					if (typeof fnSuccess === "function") {
						fnSuccess(oData, oResponse);
					}
				}, this),
				error: jQuery.proxy(function(oError) {
					this.onRequestCompleted();
					if (typeof fnError === "function") {
						fnError(oError);
					}
				}, this)
			});
		},

		readStoreSettings: function(sStoreID, fnSuccess, fnError) {
			var sPath = "/Stores('" + encodeURIComponent(sStoreID) + "')/Settings";

			this.onRequestSent(true, true);
			this._oDataModel.read(sPath, {
				success: jQuery.proxy(function(oData, oResponse) {
					this.onRequestCompleted(true, true);
					if (typeof fnSuccess === "function") {
						fnSuccess(oData, oResponse);
					}
				}, this),
				error: jQuery.proxy(function(oError) {
					this.onRequestCompleted(true, true);
					if (typeof fnError === "function") {
						fnError(oError);
					}
				}, this)
			});
		},

		readStores: function(mAdditionalParameters, fnSuccess, fnError) {
			// Set URL parameters
			var mUrlParameters = {
				"$inlinecount": "allpages"
			};
			this.enrichURLParameters(mUrlParameters, mAdditionalParameters);

			var sPath = "/Stores";
			this.onRequestSent();
			this._oDataModel.read(sPath, {
				success: jQuery.proxy(function(oData, oResponse) {
					this.onRequestCompleted();
					if (typeof fnSuccess === "function") {
						fnSuccess(oData, oResponse);
					}
				}, this),
				error: jQuery.proxy(function(oError) {
					this.onRequestCompleted();
					if (typeof fnError === "function") {
						fnError(oError);
					}
					//MessageBox.error(oError.message);
				}, this),
				urlParameters: mUrlParameters
			});
		},

		updateStoreAssignment: function(sStoreID, fnSuccess, fnError) {
			var sPath = "/Users('')";

			// Assign the given store to the user
			var oData = {};
			oData.AssignedStoreID = sStoreID;

			this.onRequestSent();
			this._oDataModel.update(sPath, oData, {
				success: jQuery.proxy(function(oResponse) {
					this.onRequestCompleted();
					if (typeof fnSuccess === "function") {
						fnSuccess();
					}
				}, this),
				error: jQuery.proxy(function(oError) {
					this.onRequestCompleted();
					if (typeof fnError === "function") {
						fnError(oError);
					}
				}, this)
			});
		},

		readProductsInitial: function(sStoreID, sScannedProductID, oFilter, sSearch, mOptions, fnSuccess, fnError) {
			this._sStoreID = sStoreID;
			this._sScannedProductID = sScannedProductID;
			this._oFilter = oFilter;
			this._sSearch = sSearch;
			this._mOptions = mOptions;

			if (!this._mOptions.SuppressBufferUpdate) {
				// First clear the buffer
				this.resetProducts();
			}

			// Existing pending store product requests must be invalidated so that their result is not processed anymore
			this.invalidateAllPendingProductRequests();

			var mParameters = {
				skip: 0,
				top: mOptions.DisablePaging ? this.getMaxItemsForDisabledPaging() : this._iGrowingThreshold
			};
			if (this._sSearch) {
				mParameters.search = this._sSearch;
			}
			if (this._mOptions.ProductDataExpand) {
				mParameters.expand = "RetailPromotions,Weeks,Exceptions";
				if (this._mOptions.ExpandWithStockProjection) {
					mParameters.expand = this.addExpandOption(mParameters.expand, "StockProjection");
				}
			}

			this.readProducts(this._sStoreID, this._sScannedProductID, this._oFilter, mParameters, mOptions, jQuery.proxy(function(oData,
				oResponse) {
				if (oData.__count && oData.__count > 0) {
					this._iInlineCount = mOptions.DisablePaging ? Math.min(oData.__count, this.getMaxItemsForDisabledPaging()) : oData.__count;
				} else {
					this._iInlineCount = 0;
				}

				if (typeof fnSuccess === "function") {
					fnSuccess(oData, oResponse);
				}
			}, this), jQuery.proxy(function(oError) {
				this._iInlineCount = 0;
				if (typeof fnError === "function") {
					fnError(oError);
				}
			}, this));
		},

		readProductsNextPackage: function(bInBackground, fnSuccess, fnError) {
			if (this._aProducts.length >= this._iInlineCount) {
				return;
			}

			if (bInBackground) {
				if (this._bNextPackageBackgroundPending) {
					// There is already a pending background request for reading the next package: do nothing
					return;
				} else {
					this._bNextPackageBackgroundPending = true;
					this._oNextPackageBackgroundDeferred = jQuery.Deferred();
				}
			}

			var mParameters = {
				skip: this._aProducts.length,
				top: this._iGrowingThreshold * this._iNumberOfPreloadPages
			};
			if (this._sSearch) {
				mParameters.search = this._sSearch;
			}
			if (this._mOptions.ProductDataExpand) {
				mParameters.expand = "RetailPromotions,Weeks";
				mParameters.expand = mParameters.expand + ",Exceptions"; //Request them independently from Settings/Enabled, which is not requested after deep link
			}

			var sScannedProductID = "";

			var mOptions = {};
			mOptions.InBackground = bInBackground;

			this.readProducts(this._sStoreID, sScannedProductID, this._oFilter, mParameters, mOptions, jQuery.proxy(function(oData, oResponse) {
				// Reset pending indicator and execute callback function
				if (typeof fnSuccess === "function") {
					fnSuccess(oData, oResponse);
				}
				if (bInBackground) {
					this._bNextPackageBackgroundPending = false;
					this._oNextPackageBackgroundDeferred.resolve();
				}
				if (this._bPromptedOfflineMessage) {
					MessageToast.show(this._oResourceBundle.getText("INFO_MSG_CONNECTION_RESTORED"));
					this._bPromptedOfflineMessage = false;
				}
			}, this), jQuery.proxy(function(oError) {
				// Reset pending indicator and execute callback function
				if (typeof fnError === "function") {
					fnError(oError);
				}
				if (bInBackground) {
					this._bNextPackageBackgroundPending = false;
					this._oNextPackageBackgroundDeferred.resolve();
				}
				if (this.checkRequestFailedBecauseOffline(oError)) {
					// Display a message toast to inform the user that currently there is no connection
					MessageToast.show(this._oResourceBundle.getText("INFO_MSG_NO_CONNECTION"));
					this._bPromptedOfflineMessage = true;
				}
			}, this));
		},

		readProductsPrevPackage: function(bInBackground, fnSuccess, fnError) {
			var iMinIndex = this.getCurrentMinIndex();
			if (iMinIndex <= 0) {
				return;
			}

			if (bInBackground) {
				if (this._bPrevPackageBackgroundPending) {
					// There is already a pending background request for reading the previous package: do nothing
					return;
				} else {
					this._bPrevPackageBackgroundPending = true;
					this._oPrevPackageBackgroundDeferred = jQuery.Deferred();
				}
			}

			var mParameters = {
				skip: Math.max(iMinIndex - this._iGrowingThreshold * this._iNumberOfPreloadPages, 0),
				top: Math.min(this._iGrowingThreshold * this._iNumberOfPreloadPages, iMinIndex)
			};
			if (this._sSearch) {
				jQuery.extend(mParameters, {
					search: this._sSearch
				});
			}
			if (this._mOptions.ProductDataExpand) {
				var sExpand = "RetailPromotions,Weeks";
				sExpand = sExpand + ",Exceptions"; //Request them independently from Settings/Enabled, which is not requested after deep link
				jQuery.extend(mParameters, {
					expand: sExpand
				});
			}

			var mOptions = {};
			mOptions.InBackground = bInBackground;

			this.readProducts(this._sStoreID, "", this._oFilter, mParameters, mOptions, jQuery.proxy(function(oData, oResponse) {
				// Reset pending indicator and execute callback function
				if (typeof fnSuccess === "function") {
					fnSuccess(oData, oResponse);
				}
				if (bInBackground) {
					this._bPrevPackageBackgroundPending = false;
					this._oPrevPackageBackgroundDeferred.resolve();
				}
				if (this._bPromptedOfflineMessage) {
					MessageToast.show(this._oResourceBundle.getText("INFO_MSG_CONNECTION_RESTORED"));
					this._bPromptedOfflineMessage = false;
				}
			}, this), jQuery.proxy(function(oError) {
				// Reset pending indicator and execute callback function
				if (typeof fnError === "function") {
					fnError(oError);
				}
				if (bInBackground) {
					this._bPrevPackageBackgroundPending = false;
					this._oPrevPackageBackgroundDeferred.resolve();
				}
				if (this.checkRequestFailedBecauseOffline(oError)) {
					// Display a message toast to inform the user that currently there is no connection
					MessageToast.show(this._oResourceBundle.getText("INFO_MSG_NO_CONNECTION"));
					this._bPromptedOfflineMessage = true;
				}
			}, this));
		},

		readProducts: function(sStoreID, sScannedProductID, oFilter, mParameters, mOptions, fnSuccess, fnError) {
			// Set URL parameters
			var mUrlParameters = {
				"$inlinecount": "allpages"
			};
			this.enrichURLParameters(mUrlParameters, mParameters);

			// Set path depending on whether a product was scanned or not
			var sPath = "";
			if (sScannedProductID) {
				sPath = "/StoreProducts(StoreID='" + encodeURIComponent(sStoreID) + "',ProductID='" + encodeURIComponent(sScannedProductID) + "')/ProductsOfSamePage";
			} else {
				sPath = "/Stores('" + encodeURIComponent(sStoreID) + "')/Products";
			}

			var iNextNumber = this.getNextFreeNumberForProductRequest();
			this._mPendingProductRequests[iNextNumber] = {
				pending: true,
				noBusyDialog: mOptions.InBackground,
				noScanBlocking: mOptions.InBackground,
				invalidated: false
			};

			var bOnlyProdTableBusy = false;
			if (!mOptions.InBackground) {
				bOnlyProdTableBusy = true;
			}
			this.onRequestSent(mOptions.InBackground, mOptions.InBackground, bOnlyProdTableBusy);
			this._oDataModel.read(sPath, {
				success: jQuery.proxy(function(oData, oResponse) {
					// Evaluate the response only if the request has not been invalidated in the meantime (e.g. by setting a new filter)
					if (!(this._mPendingProductRequests[iNextNumber] && this._mPendingProductRequests[iNextNumber].invalidated)) {
						// Merge the new products into the buffer
						if (!mOptions.SuppressBufferUpdate) {
							var aResults = jQuery.map(oData.results, function(oItem) {
								return jQuery.extend({}, oItem);
							});
							this.appendNewProducts(aResults);
						}

						this.onRequestCompleted(mOptions.InBackground, mOptions.InBackground, bOnlyProdTableBusy);
						if (typeof fnSuccess === "function") {
							fnSuccess(oData, oResponse);
						}
					}
					if (this._mPendingProductRequests[iNextNumber]) {
						delete this._mPendingProductRequests[iNextNumber];
					}
				}, this),
				error: jQuery.proxy(function(oError) {
					// Evaluate the response only if the request has not been invalidated in the meantime (e.g. by setting a new filter)
					if (!(this._mPendingProductRequests[iNextNumber] && this._mPendingProductRequests[iNextNumber].invalidated)) {
						this.onRequestCompleted(mOptions.InBackground, mOptions.InBackground, bOnlyProdTableBusy);
						if (typeof fnError === "function") {
							fnError(oError);
						}
					}
					if (this._mPendingProductRequests[iNextNumber]) {
						delete this._mPendingProductRequests[iNextNumber];
					}
				}, this),
				filters: oFilter ? [oFilter] : null,
				urlParameters: mUrlParameters
			});
		},

		readSingleProduct: function(sStoreID, sProductID, fnSuccess, fnError) {
			var mUrlParameters = {
				"$expand": "RetailPromotions,Weeks,Exceptions" //Request them independently from Settings/Enabled, which is not requested after deep link
			};

			var sPath = "/StoreProducts(StoreID='" + encodeURIComponent(sStoreID) + "',ProductID='" + encodeURIComponent(sProductID) + "')";
			this.onRequestSent();
			this._oDataModel.read(sPath, {
				success: jQuery.proxy(function(oData, oResponse) {
					// Merge the product into the buffer
					var oProduct = jQuery.extend({}, oData);
					this.refreshSingleProduct(oProduct);

					this.onRequestCompleted();
					if (typeof fnSuccess === "function") {
						fnSuccess(oData, oResponse);
					}
				}, this),
				error: jQuery.proxy(function(oError) {
					this.onRequestCompleted();
					if (typeof fnError === "function") {
						fnError(oError);
					}
				}, this),
				urlParameters: mUrlParameters
			});
		},

		readProductDetails: function(sStoreID, sProductID, bInBackground, fnSuccess, fnError) {
			var sPath = "";
			var oModel = this._oDataModel;

			// Set ID for the deferred Group
			oModel.setDeferredGroups(["productDetails"]);

			sPath = "/StoreProducts(StoreID='" + encodeURIComponent(sStoreID) + "',ProductID='" + encodeURIComponent(sProductID) + "')/RetailPromotions";
			oModel.read(sPath, {
				groupId: "productDetails"
			});

			sPath = "/StoreProducts(StoreID='" + encodeURIComponent(sStoreID) + "',ProductID='" + encodeURIComponent(sProductID) + "')/Weeks";
			oModel.read(sPath, {
				groupId: "productDetails"
			});

			sPath = "/StoreProducts(StoreID='" + encodeURIComponent(sStoreID) + "',ProductID='" + encodeURIComponent(sProductID) + "')/Exceptions";
			oModel.read(sPath, {
				groupId: "productDetails"
			});

			if (bInBackground) {
				this._mPendingProductDetailBGRequests[sProductID] = jQuery.Deferred();
			}

			this.onRequestSent(bInBackground, bInBackground);
			oModel.submitChanges({
				groupId: "productDetails",
				success: jQuery.proxy(function(oData, oResponse, aErrorResponses) {
					// Batch request successful
					this.onRequestCompleted(bInBackground, bInBackground);
					var oResponsePromotions = oData.__batchResponses[0];
					var oResponseWeeks = oData.__batchResponses[1];
					var oResponseExceptions = oData.__batchResponses[2];

					if (oResponsePromotions.statusCode === "200" && oResponsePromotions.data !== undefined && oResponseWeeks.statusCode === "200" &&
						oResponseWeeks.data !== undefined && oResponseExceptions.statusCode === "200" && oResponseExceptions.data !== undefined) {
						// Read request was successful --> update data in buffer
						var iIndex = this.getIndexOfProduct(sProductID);
						if (iIndex > -1) {
							this._aProducts[iIndex].RetailPromotions = oResponsePromotions.data;
							this._aProducts[iIndex].Weeks = oResponseWeeks.data;
							this._aProducts[iIndex].Exceptions = oResponseExceptions.data;
						}

						if (typeof fnSuccess === "function") {
							fnSuccess();
						}
					} else {
						if (typeof fnError === "function" && aErrorResponses.length > 0) {
							fnError(aErrorResponses[0]);
						}
					}

					if (bInBackground) {
						this._mPendingProductDetailBGRequests[sProductID].resolve();
						delete this._mPendingProductDetailBGRequests[sProductID];
					}

					if (this._bPromptedOfflineMessage) {
						MessageToast.show(this._oResourceBundle.getText("INFO_MSG_CONNECTION_RESTORED"));
						this._bPromptedOfflineMessage = false;
					}
				}, this),
				error: jQuery.proxy(function(oError) {
					// Batch request failed
					this.onRequestCompleted(bInBackground, bInBackground);

					if (this.checkRequestFailedBecauseOffline(oError)) {
						// Show message toast that there is currently no connection; the user can continue with the currently available data
						MessageToast.show(this._oResourceBundle.getText("INFO_MSG_NO_CONNECTION"));
						this._bPromptedOfflineMessage = true;
					}

					if (typeof fnError === "function") {
						fnError(oError);
					}

					if (bInBackground) {
						this._mPendingProductDetailBGRequests[sProductID].resolve();
						delete this._mPendingProductDetailBGRequests[sProductID];
					}
				}, this)
			});

		},

		readSingleProductByBarcode: function(sStoreID, sBarcode, fnSuccess, fnError) {
			var mUrlParameters = {
				"$expand": "RetailPromotions,Weeks,Exceptions"
			};

			var sPath = "/Stores('" + encodeURIComponent(sStoreID) + "')/Products";
			this.onRequestSent();
			this._oDataModel.read(sPath, {
				success: jQuery.proxy(function(oData, oResponse) {
					this.onRequestCompleted();

					// Data was read successfully; check if only one entry is returned and call success callback (otherwise error)
					if (oData.results.length >= 1 && typeof fnSuccess === "function") {
						fnSuccess(oData.results[0]);
					} else if (typeof fnError === "function") {
						fnError();
					}
				}, this),
				error: jQuery.proxy(function(oError) {
					this.onRequestCompleted();
					this._oComponent.setLastScannedStoreProduct(null);
					MessageBox.error(oError.message);
					//if (typeof fnError === "function") {
					//	fnError(oError);
					//}
				}, this),
				filters: [new sap.ui.model.Filter("GlobalTradeItemNumber", "EQ", sBarcode)],
				urlParameters: mUrlParameters
			});
		},

		readStockProjection: function(sStoreID, sProductID, fnSuccess, fnError) {
			var sPath = "/StoreProducts(StoreID='" + encodeURIComponent(sStoreID) + "',ProductID='" + encodeURIComponent(sProductID) + "')/StockProjection";
			this.onRequestSent(true, true);
			this._oDataModel.read(sPath, {
				success: jQuery.proxy(function(oData) {
					this.onRequestCompleted(true, true);

					// Set the stock projection data also to the existing product data
					this.mergeProductStockProjection(sProductID, oData);
					if (typeof fnSuccess === "function") {
						fnSuccess(oData);
					}
				}, this),
				error: jQuery.proxy(function(oError) {
					this.onRequestCompleted(true, true);
					if (typeof fnError === "function") {
						fnError(oError);
					}
				}, this)
			});
		},

		submitProductChanges: function(sStoreID, sProductID, fnSuccess, fnError) {
			var fnSaveSuccess = function() {
				if (typeof fnSuccess === "function") {
					fnSuccess();
				}
			};

			// Get product from buffer
			var iIndex = this.getIndexOfProduct(sProductID);
			if (iIndex === -1) {
				fnSaveSuccess();
				return;
			}

			// Check if the processing state or the order quantity were changed and that that there is no pending request
			if (!this._aProducts[iIndex]._bOrderQuantityChanged && !this._aProducts[iIndex]._bProcessingStateChanged ||
				this._aProducts[iIndex]._bRequestPending) {
				fnSaveSuccess();
				return;
			}

			// Create data for update
			var oChangedData = {};
			var oOrderQuantity = this._aProducts[iIndex].OrderQuantity;
			if (this._aProducts[iIndex]._bOrderQuantityChanged) {
				if (typeof oOrderQuantity !== "string") {
					oChangedData.OrderQuantity = oOrderQuantity.toString();
				} else {
					oChangedData.OrderQuantity = oOrderQuantity;
				}
			}
			oChangedData.IsOrderQuantityProcessed = this._aProducts[iIndex].IsOrderQuantityProcessed;

			// The backend can handle updates via merge/patch now -> send the order quantity, which enables that feature
			oChangedData.OrderQuantityUnitCode = this._aProducts[iIndex].OrderQuantityUnitCode;

			// Remember that there is a pending request
			this._aProducts[iIndex]._bRequestPending = true;

			var bOrderQuantityChanged = this._aProducts[iIndex]._bOrderQuantityChanged;

			var oModel = this._oDataModel;

			// Set ID for the deferred Group
			oModel.setDeferredGroups(["productChanges"]);

			var sPath = "/StoreProducts(StoreID='" + encodeURIComponent(sStoreID) + "',ProductID='" + encodeURIComponent(sProductID) + "')";
			oModel.update(sPath, oChangedData, {
				eTag: this._aProducts[iIndex].__metadata.etag,
				groupId: "productChanges"
			});

			oModel.read(sPath, {
				groupId: "productChanges",
				urlParameters: {
					$expand: "RetailPromotions,Weeks,Exceptions"
				}
			});

			this.onRequestSent(true, true);

			// Submit the Changes for the given Group
			oModel.submitChanges({
				groupId: "productChanges",
				success: jQuery.proxy(function(oData, oResponse) {
					var iIndexUpdate = this.getIndexOfProduct(sProductID);
					if (iIndexUpdate > -1) {
						this._aProducts[iIndexUpdate]._bRequestPending = false;
					}

					this.onRequestCompleted(true, true);
					var oResponseUpdate = oData.__batchResponses[0];
					var oResponseRead = oData.__batchResponses[1];

					if (oResponseRead.statusCode === "200" && oResponseRead.data !== undefined) {
						// Read request was successful --> update data on UI if necessary
						if (bOrderQuantityChanged || typeof oResponseRead.data.OrderQuantity !== "undefined" && Number(oResponseRead.data.OrderQuantity) !== Number(oOrderQuantity)) {
							// It might be the case that the change was successful, but the quantity was rounded or it was changed in the meantime in another session
							var oProduct = jQuery.extend({}, oResponseRead.data);
							this.refreshSingleProduct(oProduct);
							this.triggerProductRefresh([sProductID]);
							if (oResponseRead && oResponseRead.headers && oResponseRead.headers.etag) {
								this._aProducts[iIndexUpdate].__metadata.etag = oResponseRead.headers.etag;
							}
						} else {
							// Remember that the processing state is not changed anymore
							if (iIndexUpdate > -1) {
								this._aProducts[iIndexUpdate]._bProcessingStateChanged = false;
							}
							if (oResponseUpdate.__changeResponses && oResponseUpdate.__changeResponses[0].headers &&
								oResponseUpdate.__changeResponses[0].headers.etag) {
								this._aProducts[iIndexUpdate].__metadata.etag = oResponseUpdate.__changeResponses[0].headers.etag;
							} else {
								oProduct = jQuery.extend({}, oResponseRead.data);
								this.refreshSingleProduct(oProduct);
								this.triggerProductRefresh([sProductID]);
								if (oResponseRead && oResponseRead.headers && oResponseRead.headers.etag) {
									this._aProducts[iIndexUpdate].__metadata.etag = oResponseRead.headers.etag;
								}
							}
						}
						if (typeof fnSuccess === "function") {
							fnSuccess(oResponseUpdate, oResponseRead);
						}
					}

				}, this),
				error: jQuery.proxy(function(oError) {
					// Batch request failed
					var iIndexUpdate = this.getIndexOfProduct(sProductID);
					if (iIndexUpdate > -1) {
						this._aProducts[iIndexUpdate]._bRequestPending = false;
					}

					this.onRequestCompleted(true, true);

					if (this.checkRequestFailedBecauseOffline(oError)) {
						// Store the failed update requests in a queue so that it can be re-processed later
						this.storeFailedOperation({
							sStoreID: sStoreID,
							sProductID: sProductID,
							oChangedData: oChangedData,
							sEtag: this._aProducts[iIndexUpdate].__metadata.etag
						});

						// Try to re-process the failed operations periodically
						this.startDelayedProcessing();

						if (!this._bPromptedOfflineMessage) {
							// Display a message toast to inform the user that currently there is no connection
							MessageToast.show(this._oResourceBundle.getText("INFO_MSG_NO_CONNECTION"));
							this._bPromptedOfflineMessage = true;
						}
					} else {
						// Standard handling: call error callback function
						if (typeof fnError === "function") {
							fnError(oError);
						}
					}
				}, this)
			});
		},

		checkRequestFailedBecauseOffline: function(oError) {
			var bOffline = true;
			if (oError && oError.response && oError.response.statusCode > 0 || oError && oError.statusCode > 0) {
				// An actual status code is provided, so the error was not caused by missing network connection (maybe short dump or timeout)
				bOffline = false;
			}
			return bOffline;
		},

		storeFailedOperation: function(oOperation) {
			// Check if operation has already been saved and then update the changed data
			for (var i = 0, iLength = this._aFailedOperations.length; i < iLength; i++) {
				if (this._aFailedOperations[i].sProductID === oOperation.sProductID && this._aFailedOperations[i].sStoreID === oOperation.sStoreID) {
					this._aFailedOperations[i].oChangedData = oOperation.oChangedData;
					return;
				}
			}
			// Otherwise save the operation
			this._aFailedOperations.push(oOperation);
		},

		startDelayedProcessing: function() {
			// Cancel an existing delayed call if there is any
			if (this._sCurrentTimeOutID) {
				jQuery.sap.clearDelayedCall(this._sCurrentTimeOutID);
				this._sCurrentTimeOutID = "";
			}

			// Try to re-process the failed operations after a certain time
			this._sCurrentTimeOutID = jQuery.sap.delayedCall(this._iDelayTime, this, this.processFailedOperations);
		},

		processFailedOperations: function() {
			// Process only n products at once, splice removes the entries from _aFailedOperations
			var iBatchLimit = Math.min(this._aFailedOperations.length, this._iBatchLimit);
			var oModel = this._oDataModel;
			// Set ID for the deferred Group
			oModel.setDeferredGroups(["failedOperations"]);
			var iChangeSetId = 1;

			// Add merge operation for each product
			for (var i = 0; i < iBatchLimit; i++) {
				var sProductID = this._aFailedOperations[i].sProductID;
				var sStoreID = this._aFailedOperations[i].sStoreID;
				var oChangedData = this._aFailedOperations[i].oChangedData;

				var sPath = "/StoreProducts(StoreID='" + encodeURIComponent(sStoreID) + "',ProductID='" + encodeURIComponent(sProductID) + "')";

				oModel.update(sPath, oChangedData, {
					groupId: "failedOperations",
					changeSetId: iChangeSetId,
					eTag: this._aFailedOperations[i].sEtag
				});

				iChangeSetId++;
			}
			// Add get operation for each product
			for (i = 0; i < iBatchLimit; i++) {
				sProductID = this._aFailedOperations[i].sProductID;
				sStoreID = this._aFailedOperations[i].sStoreID;
				oChangedData = this._aFailedOperations[i].oChangedData;

				sPath = "/StoreProducts(StoreID='" + encodeURIComponent(sStoreID) + "',ProductID='" + encodeURIComponent(sProductID) + "')";
				oModel.read(sPath, {
					groupId: "failedOperations",
					urlParameters: {
						$expand: "RetailPromotions,Weeks,Exceptions"
					}
				});
			}

			this.onRequestSent(true, true);

			oModel.submitChanges({
				groupId: "failedOperations",
				success: jQuery.proxy(function(oData, oResponse) {
					this.onRequestCompleted(true, true);

					if (this._bPromptedOfflineMessage) {
						// Display a message toast to inform the user that the connection has been restored
						MessageToast.show(this._oResourceBundle.getText("INFO_MSG_CONNECTION_RESTORED"));
						this._bPromptedOfflineMessage = false;
					}

					var aErrorResponses = [];
					// Search each processed product in _aProducts
					var aSuccProductIDs = [];
					var aUpdatedProducts = [];
					var iOffset = oData.__batchResponses.length / 2;
					for (i = 0; i < iOffset; i++) {
						var oResponseUpdate = oData.__batchResponses[i];
						var oResponseRead = oData.__batchResponses[i + iOffset];
						if (oResponseRead.statusCode === "200" && oResponseRead.data !== undefined) {
							// Check if the updated product is still in the buffer
							var iIndex = this.getIndexOfProduct(oResponseRead.data.ProductID);

							if (iIndex !== -1) {
								// Update the data in the buffer and collect successful products
								var oProduct = jQuery.extend({}, oResponseRead.data);
								this.refreshSingleProduct(oProduct);
								aSuccProductIDs.push(oProduct.ProductID);
								if (oResponseRead && oResponseRead.headers && oResponseRead.headers.etag) {
									this._aProducts[iIndex].__metadata.etag = oResponseRead.headers.etag;
								}
							}

							// Remove the successfully processed operation from the queue
							this.removeFailedOperation(oResponseRead.data.ProductID, oResponseRead.data.StoreID);

							// Collect the updated products
							aUpdatedProducts.push(oResponseRead.data);
						}

						if (oResponseUpdate.response) {
							aErrorResponses.push(oResponseUpdate);
						}
					}

					if (aErrorResponses.length !== 0) {
						var sCompleteErrorMessage = Reuses1TextUtil.getMessageForErrorResponses(aErrorResponses);
						MessageBox.error("At least one error occured", {
							details: sCompleteErrorMessage
						});
					}

					// Update product data on UI if necessary
					if (aSuccProductIDs.length !== 0) {
						this.triggerProductRefresh(aSuccProductIDs);
					}

					// Call a utility hook function which may be used to display further messages to the user
					if (aUpdatedProducts.length !== 0 && this.utilityExtHookAfterDelayedUpdate) {
						/**
						 * @UtilityHook [Post processing after delayed update]
						 * This function is called after some delayed order quantity updates have been processed in the background. These delayed updates
						 * originate from manual updates on the order quantity during an offline period. The app caches these updates and processes them
						 * automaticall as soon as the network connection is available again. As a parameter, the function gets an array of processed store products.
						 * The function can be used to display further details to the user, e.g. which products have been processed exactly.
						 * @callback retail.store.orderproducts1.model.DataManager~utilityExtHookAfterDelayedUpdate
						 */
						this.utilityExtHookAfterDelayedUpdate(aUpdatedProducts);
					}

					if (this._aFailedOperations.length !== 0) {
						// Re-process the remaining update operations
						this.startDelayedProcessing();
					}
				}, this),
				error: jQuery.proxy(function() {
					// Batch request failed
					this.onRequestCompleted(true, true);

					// Try again
					this.startDelayedProcessing();
				}, this)
			});
		},

		removeFailedOperation: function(sProductID, sStoreID) {
			// Remove the operation for the given store product
			for (var i = 0, iLength = this._aFailedOperations.length; i < iLength; i++) {
				if (this._aFailedOperations[i].sProductID === sProductID && this._aFailedOperations[i].sStoreID === sStoreID) {
					this._aFailedOperations.splice(i, 1);
					return;
				}
			}
		},

		setGrowingThreshold: function(iGrowingThreshold) {
			this._iGrowingThreshold = iGrowingThreshold;
		},

		setNumberOfPreloadPages: function(iNumberOfPreloadPages) {
			this._iNumberOfPreloadPages = iNumberOfPreloadPages;
		},

		getCurrentMinIndex: function() {
			var iIndex = -1;
			for (var i = 0, iLength = this._aProducts.length; i < iLength; i++) {
				if (this._aProducts[i] && this._aProducts[i].ProductID) {
					iIndex = i;
					break;
				}
			}
			return iIndex;
		},

		addExpandOption: function(sExpandOld, sExpandAdditional) {
			var sExpand = "";
			if (sExpandOld) {
				sExpand = sExpandOld + "," + sExpandAdditional;
			} else {
				sExpand = sExpandAdditional;
			}
			return sExpand;
		},

		appendNewProducts: function(aProducts) {
			// The list of products does not necessarily start from the beginning (in case of scanning)
			var iIndex = -1;
			for (var i = 0, iLength = aProducts.length; i < iLength; i++) {
				iIndex = Math.max(aProducts[i].EntitySetIndex - 1, 0);
				if (!this._aProducts[iIndex] || !this._aProducts[iIndex].ProductID) {
					// Only update the buffer in case the product is not available yet
					this._aProducts[iIndex] = aProducts[i];
				}
			}
		},

		saveSelectedFilterValues: function(sStoreID, oFilters) {
			var sFunction = "";
			var oModel = this._oDataModel;

			if (!oFilters) {
				// Nothing to do
				return;
			}

			oModel.setDeferredGroups(["filterValues"]);

			// First create a request to delete all existing filter value personalization data
			sFunction = "/DeleteUserDefaultFilterValues";

			oModel.callFunction(sFunction, {
				method: "POST",
				groupId: "filterValues",
				urlParameters: {
					StoreID: sStoreID,
					OrderProductFilterID: "",
					OrderProductFilterValueID: ""
				}
			});

			// Now create a request for every selected filter value
			jQuery.each(oFilters, function(sFilterID, oFilterValues) {
				jQuery.each(oFilterValues, function(sFilterValueID, bSelected) {
					if (bSelected) {

						sFunction = "/UpdateUserDefaultFilterValue";

						oModel.callFunction(sFunction, {
							method: "POST",
							groupId: "filterValues",
							urlParameters: {
								StoreID: sStoreID,
								OrderProductFilterID: sFilterID,
								OrderProductFilterValueID: sFilterValueID
							}
						});
					}
				});
			});

			this.onRequestSent(true, true);
			oModel.submitChanges({
				groupId: "filterValues",
				success: jQuery.proxy(function(oData) {
					this.onRequestCompleted(true, true);
				}, this),
				error: jQuery.proxy(function(oError) {
					// Batch request failed
					this.onRequestCompleted(true, true);
					jQuery.sap.log.error("Saving personalization data failed.");
				}, this)
			});
		},

		enrichURLParameters: function(mUrlParameters, mAdditionalParameters) {
			if (mUrlParameters && mAdditionalParameters) {
				if (mAdditionalParameters.skip) {
					jQuery.extend(mUrlParameters, {
						"$skip": mAdditionalParameters.skip
					});
				}
				if (mAdditionalParameters.top) {
					jQuery.extend(mUrlParameters, {
						"$top": mAdditionalParameters.top
					});
				}
				if (mAdditionalParameters.search) {
					jQuery.extend(mUrlParameters, {
						"search": mAdditionalParameters.search
					});
				}
				if (mAdditionalParameters.expand) {
					jQuery.extend(mUrlParameters, {
						"$expand": mAdditionalParameters.expand
					});
				}
			}
		},

		getCurrentCount: function() {
			return this._iInlineCount;
		},

		getSingleProduct: function(iIndex, sProductID) {
			var iBufferIndex = -1;
			var oProduct = null;

			if (sProductID) {
				// Access with product ID
				iBufferIndex = this.getIndexOfProduct(sProductID);
			} else if (iIndex > -1) {
				// Direct access with index
				iBufferIndex = iIndex;
			}

			if (iBufferIndex > -1 && this._aProducts[iBufferIndex] && this._aProducts[iBufferIndex].ProductID) {
				oProduct = jQuery.extend(true, {}, this._aProducts[iBufferIndex]);
			}

			return oProduct;
		},

		getProducts: function(iSkip, iTop) {
			var aProducts = [];
			for (var i = iSkip; i < iSkip + iTop; i++) {
				if (this._aProducts[i] && this._aProducts[i].ProductID) {
					aProducts.push(jQuery.extend({}, this._aProducts[i]));
				}
			}
			return aProducts;
		},

		getMaxItemsForDisabledPaging: function() {
			// Default value: request a maximum of 500 entries in case paging is disabled
			var iMaxItems = 500;

			/**
			 * @UtilityHook [Get maximum number of items for disabled paging]
			 * In some special cases it is necessary to disable the backend paging mechanism. Then the UI retrieves all matching products from the
			 * backend and performs a pure frontend paging. Specifically, this is the case if the user selects any of the following attribute filters:
			 * - With Zero Order Quantity
			 * - Changed Order Quantity Not Posted
			 * - Processed
			 * - Not Processed
			 * If the backend paging is disabled, the UI still uses a maximum number of items for the backend request,
			 * which is 500 by default. This default quantity can be changed via this utility hook method.
			 * @callback retail.store.orderproducts1.model.DataManager~utilityExtHookGetMaxItemsForDisabledPaging
			 * @return {int} The maximum number of items in case the backend paging is disabled.
			 */
			if (this.utilityExtHookGetMaxItemsForDisabledPaging) {
				var iMaxItemsHook = this.utilityExtHookGetMaxItemsForDisabledPaging();
				if (iMaxItemsHook > 0) {
					iMaxItems = iMaxItemsHook;
				}
			}
			return iMaxItems;
		},

		findProductByBarcode: function(sBarcode) {
			var oProduct = null;
			for (var i = 0, iLength = this._aProducts.length; i < iLength; i++) {
				if (this._aProducts[i] && this._aProducts[i].ProductID && ScanUtil.checkBarcodeMatchForProduct(sBarcode,
						this._aProducts[i])) {
					oProduct = this._aProducts[i];
					break;
				}
			}
			return oProduct;
		},

		getIndexOfProduct: function(sProductID) {
			var iIndex = -1;
			var iLength = this._aProducts.length;
			for (var i = 0; i < iLength; i++) {
				if (this._aProducts[i] && this._aProducts[i].ProductID === sProductID) {
					iIndex = i;
					break;
				}
			}
			return iIndex;
		},

		refreshSingleProduct: function(oProduct) {
			var iIndex = this.getIndexOfProduct(oProduct.ProductID);
			if (iIndex > -1) {
				// Save the stock projection for this product if it's available -> it should not be affected by the refresh
				var oStockProjection = this._aProducts[iIndex].StockProjection;

				// Update the product data
				this._aProducts[iIndex] = oProduct;

				if (oStockProjection) {
					// Restore the stock projection again
					this._aProducts[iIndex].StockProjection = oStockProjection;
				}
			}
		},

		mergeProductStockProjection: function(sProductID, oStockProjection) {
			var iIndex = this.getIndexOfProduct(sProductID);
			if (iIndex > -1) {
				this._aProducts[iIndex].StockProjection = oStockProjection;
			}
		},

		setProductNewOrderQuantity: function(sProductID, fOrderQuantity, bProcessed) {
			var iIndex = this.getIndexOfProduct(sProductID);
			if (iIndex > -1) {
				this._aProducts[iIndex].OrderQuantity = fOrderQuantity;
				this._aProducts[iIndex].IsChangedOrderQuantityPosted = null;

				if (bProcessed) {
					this._aProducts[iIndex].IsOrderQuantityProcessed = true;
				}

				// Remember that the order quantity of this product was changed
				this._aProducts[iIndex]._bOrderQuantityChanged = true;
			}
		},

		setProductProcessed: function(sProductID) {
			var iIndex = this.getIndexOfProduct(sProductID);
			if (iIndex > -1 && !this._aProducts[iIndex].IsOrderQuantityProcessed) {
				this._aProducts[iIndex].IsOrderQuantityProcessed = true;

				// Remember that the processing state of this product was changed
				this._aProducts[iIndex]._bProcessingStateChanged = true;
			}
		},

		resetProducts: function() {
			this._aProducts = [];
		},

		getNextFreeNumberForProductRequest: function() {
			var iMaxNumber = 0;
			for (var sKey in this._mPendingProductRequests) {
				if (this._mPendingProductRequests.hasOwnProperty(sKey) && sKey > iMaxNumber) {
					iMaxNumber = Number(sKey);
				}
			}
			return iMaxNumber + 1;
		},

		invalidateAllPendingProductRequests: function() {
			for (var sKey in this._mPendingProductRequests) {
				if (this._mPendingProductRequests.hasOwnProperty(sKey) && this._mPendingProductRequests[sKey].pending) {
					// Set the 'invalidated' flag which is evaluated later in the response callbacks of readProducts
					this._mPendingProductRequests[sKey].invalidated = true;

					// Call onRequestCompleted now because we fake the request to be completed and it's not called later in the responce callbacks anymore
					this.onRequestCompleted(this._mPendingProductRequests[sKey].noBusyDialog, this._mPendingProductRequests[sKey].noScanBlocking);
				}
			}
			this._bPrevPackageBackgroundPending = false;
			this._bNextPackageBackgroundPending = false;
			this._oNextPackageBackgroundDeferred = null;
			this._oPrevPackageBackgroundDeferred = null;
		},

		getNextPackageBackgroundPending: function() {
			return this._bNextPackageBackgroundPending;
		},

		getPrevPackageBackgroundPending: function() {
			return this._bPrevPackageBackgroundPending;
		},

		getNextPackageBackgroundPromise: function() {
			return this._oNextPackageBackgroundDeferred && this._oNextPackageBackgroundDeferred.promise();
		},

		getPrevPackageBackgroundPromise: function() {
			return this._oPrevPackageBackgroundDeferred && this._oPrevPackageBackgroundDeferred.promise();
		},

		getProductDetailBackgroundPromise: function(sProductID) {
			return this._mPendingProductDetailBGRequests && this._mPendingProductDetailBGRequests[sProductID] &&
				this._mPendingProductDetailBGRequests[sProductID].promise();
		}

	});

	return DataManager;
});